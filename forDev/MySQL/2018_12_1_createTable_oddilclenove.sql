CREATE TABLE `oddilclenove` (
  `iPK` int(11) NOT NULL AUTO_INCREMENT,
  `sJmeno` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `sPrijmeni` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `sText` text COLLATE utf8_czech_ci,
  `dCreate` datetime DEFAULT NULL,
  `dUpdate` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `sRocnik` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `iTrener` tinyint(2) DEFAULT '0',
  `iArchive` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`iPK`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
