CREATE TABLE `oddil_prispevky` (
  `id_user` int(11) NOT NULL,
  `dUpdate` datetime DEFAULT NULL,
  `dPlacenoDo` date DEFAULT NULL,
  `sTypPlatby` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `sPoznamka` text COLLATE utf8_czech_ci,
  `iCastka` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `oddilclenove_iPK` FOREIGN KEY (`id_user`) REFERENCES `oddilclenove` (`iPK`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='musi to byz iPK clena oddilu';
SELECT * FROM d185685_sd.oddil_prispevky;