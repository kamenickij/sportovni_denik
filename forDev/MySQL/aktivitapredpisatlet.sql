-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 31. říj 2018, 17:15
-- Verze serveru: 8.0.11
-- Verze PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `sportovni_denik`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `aktivitapredpisatlet`
--

CREATE TABLE `aktivitapredpisatlet` (
  `iPK` int(11) NOT NULL,
  `datum` date DEFAULT NULL,
  `iNemoc` int(11) DEFAULT NULL,
  `sNemoc` varchar(1000) DEFAULT NULL,
  `iZavod` int(11) DEFAULT NULL,
  `sZavod` varchar(1000) DEFAULT NULL,
  `casZatizeni` varchar(1000) DEFAULT NULL,
  `casRegenerace` varchar(1000) DEFAULT NULL,
  `atlet_id_user` int(11) NOT NULL,
  `trener_id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `aktivitapredpisatlet`
--
ALTER TABLE `aktivitapredpisatlet`
  ADD PRIMARY KEY (`iPK`),
  ADD KEY `fk_aktivitaPredpisAtlet_user1_idx` (`atlet_id_user`),
  ADD KEY `fk_aktivitaPredpisAtlet_user2_idx` (`trener_id_user`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `aktivitapredpisatlet`
--
ALTER TABLE `aktivitapredpisatlet`
  MODIFY `iPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `aktivitapredpisatlet`
--
ALTER TABLE `aktivitapredpisatlet`
  ADD CONSTRAINT `fk_aktivitaPredpisAtlet_user1` FOREIGN KEY (`atlet_id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `fk_aktivitaPredpisAtlet_user2` FOREIGN KEY (`trener_id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
