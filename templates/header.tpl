<?php
   Header("Expires: ".GMDate("D, d M Y H:i:s")." GMT");
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.
?>
<html class="denik">
<head>
<title>Sportovní deník</title>
<meta http-equiv=Content-Type content="text/html; charset=UTF-8" />
{* jquery *}
<script src="js/jquery-3.3.1.min.js"></script>
{* 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> *}

<script src="js/js.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/datatables.min.js"></script>

{* <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css"> *}

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="js/globalScripts.js"></script>

<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="css/datatables.min.css">



<!-- FONT AWESOME 5 -->
<script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0"/>
<!-- IE 6 "fixes" -->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="css/modal_ie.css" media="screen" />
<![endif]-->

</head>
{include file='../templates/modal-global.tpl'}
{include file='../templates/modal-global-info.tpl'}
{include file='../templates/spinner.tpl'}