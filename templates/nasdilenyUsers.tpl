<script>
  jQuery(function ($) {

    $('#btnNastUserTyden').click(function () {
      $('#btnUlozit').click();
     var data = $('#frmUsers').serialize();
     dataString = data.replace(/id_user=/g, '');
     dataString = dataString.replace(/&/g, ',');
     var tyden = $('#tyden-form option:selected').val();
     var id_user_origin = $('#filtr-uzivatel-select').val();
     dataString = 'sAction=modalNasdileni&id_users=' +  dataString + '&tyden=' + tyden + '&id_user_origin=' + id_user_origin; 
     console.log(dataString);
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: dataString,
        success: function (content) {
          console.log(content);
          $('.modal-global-close').click();
        }

      });
      return false;
    });
    
  }); //ready
</script>

<h3>Výpis nasdílených atletů:</h3>
<form id="frmUsers">
<table class="nasdilenyUsers">
  <th>
    NASDÍLET
  </th>
  <th>
    Jméno
  </th>
  <th>
    Příjmení
  </th>
  <th>
    Email
  </th>
  
  {foreach from=$nasdilenyUsers key=k item=i}
      
  <tr class="rt-tr-input" iduser="{$i.id_user}">
    <td class="idUser">
        <input type="checkbox" name="id_user" value="{$i.id_user}" class="sdilet">
    </td>
    <td colspan="">
      {$i.jmeno}
    </td>
    <td colspan="">
      {$i.prijmeni}
    </td>
    <td>
      {$i.email}
    </td>
    
    
  </tr>
  {/foreach}  
</table>
  </form>
<button class="btn-filtr" id="btnNastUserTyden">Nastavit</button>
