<div class="info table1">	
        <form id="editUser">
            
            <input type="hidden" id="sAction" name="sAction" value="adminEditUser">
	<table class="admin-users">
		<thead>
		<tr>
			<th>ID</th>
			<th>KOD</th>
			<th>Login</th>
			<th>Email</th>
			<th>Titul</th>
			<th>Jmeno</th>
			<th>Prijmeni</th>
                        <th>Heslo</th>                        
			<th>Typ</th>
			<th>Popis</th>
			

		</tr>
		</thead>
		<tbody>
{foreach from=$users key=k item=i}
    <input type="hidden" id="iduser" name="iduser" value="{$i.id_user}">
  <tr>
  	<td>{$i.id_user}</td>
  	<td><input type="text" name="kod" value="{$i.kod}" ></td>
  	<td><input type="" name="login" value="{$i.login}" ></td>
  	<td><input type="text" name="email" value="{$i.email}" disabled="disabled" ></td>
  	<td><input type="text" name="titul" value="{$i.titul}"  ></td>
  	<td><input type="text" name="jmeno" value="{$i.jmeno}"  ></td>
  	<td><input type="text" name="prijmeni" value="{$i.prijmeni}"  ></td>
        <td><input type="password" name="heslo" id="heslo" value="" placeholder="Pro změnu zadejte heslo"  >
            <input type="password" name="hesloPotvrzeni" id="hesloPotvrzeni" value="" style="display: none;" placeholder="Heslo znovu" ></td>
  	<td><select name="typ">
					<option value="ATL" {if $i.typ_user == "ATL" } selected="selected" {/if} >ATLET</option>
					<option value="TRN" {if $i.typ_user == "TRN" } selected="selected" {/if} >TRENÉR</option>
					</select></td>
  	<td><textarea name="popis" value="{$i.popis}"></textarea></td>
  	

  </tr>
{/foreach}
</tbody>
</table>
</form>

</div>
<button class="btn-filtr" id="editUserSave">ULOŽIT</button>

<script type="text/javascript">
$(function() {
   $('#heslo').change(function() {
    var heslo = $(this).val();
            if(heslo>'') {
                $('#hesloPotvrzeni').show();
            } else {
                $('#hesloPotvrzeni').hide();
            }
        }); 
        
        $('#editUserSave').click(function(){
            var hesloPotvrzeni = $('#hesloPotvrzeni').val();
            var heslo = $('#heslo').val();
            if (heslo > '' && heslo !== hesloPotvrzeni) {
                alert('Hesla se neshodují.');
                return false;
            }
            var dataString = $('#editUser').serialize();
             $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      infoSuccess(content);
                      $('.modal-global').hide();
                    }
                  });
        });
    
    
});
</script>
