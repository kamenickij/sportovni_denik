<script>
  $(function () {
   $('.select-2').select2();
   
        {if $tydenData[1]['iNemoc'] == 1}
            $('.btn-nemoc-btn.{$denTyden}').click();  
        {/if}
            
        {if $tydenData[1]['iZavod'] == 1}
            $('.btn-zavod-btn.{$denTyden}').click();    
        {/if}
	// zmena MJ po zmene selectu
   $('.train-phase').on('change','.input-ots-id', function(){
     var mj = $(this).closest('div').find('option:selected').attr('sMj');
     //console.log(mj);
     $(this).closest('div').find('.mj').text(mj);
     //ar iPK = $(this).closest('.train-item').attr('idOtu');
     var iPK = $(this).val();
    $(this).closest('div').attr('idotu', iPK);  

     iPK = 'rp_' + iPK;
     $(this).closest('.train-item').find('.rp_prepocet').removeClass();
     $(this).closest('.train-item').find('.p_prepocet').find('span').addClass(iPK).addClass('rp_prepocet');

   });
    

  }); //function
</script>
<!-- tydenni-predpis-item.tpl -->
<input type="hidden" class="apa_ipk" value="{$tydenData[1]['apa_ipk']}" >
<div class="day-row-2 train-set">
  <div class="faze1">
    <h3>FÁZE I</h3>
    <div style="clear:both;"></div>
    <input type="hidden" class="iNemoc" value="0">
    <input type="hidden" class="iZavod" value="0">
    <p class="labelFazeOtu">Celk. čas zatížení</p><input class="fazeOtu casZat" value="{$tydenData[1]['casZatizeni']}" ><p class="fazeCasZat">HOD</p>
    <div style="clear:both; height:5px;"></div>
    <p class="labelFazeOtu">Regenerace/stretch.</p><input class="fazeOtu reg" value="{$tydenData[1]['casRegenerace']}" ><p class="fazeCasZat">HOD</p>
    <!-- id="sFazeMisto" name="sFazeMisto" -->
    <span>{if isset($tydenData[1]['misto'])}{$tydenData[1]['misto']}{/if}</span>
    {if isset($tydenData[1]['idTrener'])}<input type="hidden" class="idTrener" value="{$tydenData[1]['idTrener']}">{/if}
    <div class="train-phase">        
        {if isset($tydenData[1]['aktivity'])}
            {foreach $tydenData[1]['aktivity'] key=id item=aktivita}
                <div class="train-item">
                  <div class="item"> 
                       {if $aktivita['iStav'] < 1} <i class="fas fa-question grey" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 2} <i class="fas fa-check-square green" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 3} <i class="fas fa-window-close red" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 4} <span class='presunutoDoFaze'>1</span> 
                          {elseif $aktivita['iStav'] == 5} <span class='presunutoDoFaze'>2</span> 
                          {elseif $aktivita['iStav'] == 6} <span class='presunutoDoFaze'>3</span> 
                                      {/if}
                      <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                      
                      <input type="hidden" class="idRadku" value="{$id}"> 
                  </div>
                  <div class="item">    
                      <span class="jinaFaze toFaze2" style="display: inline-block;" dofaze="5" title="Přesunout do fáze 2">2</span>
                      <span class="jinaFaze toFaze3" style="display:inline-block;" dofaze="6" title="Přesunout do fáze 3">3</span>
                    </div> 
                  <div class="item">
                      <select class="input-ots-id select-2" disabled="">
                        <option value='0'>Vyberte OTS/STS</option>
                        {foreach from=$ots key=k item=i}
                          <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                          {/foreach}
                        </select>
                    </div>
                    <div class="item">
                        <span class="aktivitaMargin inline-block">{$aktivita['sAktivita']}</span>
                    </div>
                    <div class="item">    
                      <span class="inline-block">{$aktivita['iOpakovani']}</span>
                    </div>  
                    <div class="item">  
                      <span class="nasobeni inline-block">x</span>
                    </div>
                    <div class="item">
                      <span class="inline-block">{$aktivita['vzdalenost']}</span>
                    </div>
                    <div class="item">  
                        <p class="mj inline-block">{$aktivita['ots_smj']}</p>
                    </div>
                    <div class="item">
                        <span title="Nesplnil jsem."><i class="fas fa-exclamation-circle red zamitnoutPolozku aControl"></i></span>
                        <span title="Splnil jsem."><i class="fas fa-check-circle confirmPolozku aControl"></i></span>
                    </div> 
                                          
                                        
                </div>
            {/foreach}
              
        {/if}
      <div class="pridat"></div>
	  <div class="plus-ots">
        <i style="color:green;font-size:20px;cursor:pointer" class="fa fa-plus-square " aria-hidden="true"></i>
      </div>

      
    </div>


  </div>
  <!-- /train-phase -->
  <div class="faze2">
    <h3>FÁZE II</h3>
    <span>{if isset($tydenData[2]['misto'])}{$tydenData[2]['misto']}{/if}</span>
    {if isset($tydenData[2]['idTrener'])}<input type="hidden" class="idTrener" value="{$tydenData[2]['idTrener']}">{/if}
    <div class="train-phase">
        {if isset($tydenData[2]['aktivity'])}
            {foreach $tydenData[2]['aktivity'] key=id item=aktivita}
            <div class="train-item">
                  <div class="item"> 
                       {if $aktivita['iStav'] < 1} <i class="fas fa-question grey" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 2} <i class="fas fa-check-square green" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 3} <i class="fas fa-window-close red" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 4} <span class='presunutoDoFaze'>1</span> 
                          {elseif $aktivita['iStav'] == 5} <span class='presunutoDoFaze'>2</span> 
                          {elseif $aktivita['iStav'] == 6} <span class='presunutoDoFaze'>3</span> 
                                      {/if}
                      <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                      
                      <input type="hidden" class="idRadku" value="{$id}"> 
                  </div>
                  <div class="item">    
                      <span class="jinaFaze toFaze2" style="display: inline-block;" dofaze="1" title="Přesunout do fáze 1">1</span>
                      <span class="jinaFaze toFaze3" style="display:inline-block;" dofaze="6" title="Přesunout do fáze 3">3</span>
                    </div> 
                  <div class="item">
                      <select class="input-ots-id select-2" disabled="">
                        <option value='0'>Vyberte OTS/STS</option>
                        {foreach from=$ots key=k item=i}
                          <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                          {/foreach}
                        </select>
                    </div>
                    <div class="item">
                        <span class="aktivitaMargin inline-block">{$aktivita['sAktivita']}</span>
                    </div>
                    <div class="item">    
                      <span class="inline-block">{$aktivita['iOpakovani']}</span>
                    </div>  
                    <div class="item">  
                      <span class="nasobeni inline-block">x</span>
                    </div>
                    <div class="item">
                      <span class="inline-block">{$aktivita['vzdalenost']}</span>
                    </div>
                    <div class="item">  
                        <p class="mj inline-block">{$aktivita['ots_smj']}</p>
                    </div>
                    <div class="item">
                        <span title="Nesplnil jsem."><i class="fas fa-exclamation-circle red zamitnoutPolozku aControl"></i></span>
                        <span title="Splnil jsem."><i class="fas fa-check-circle confirmPolozku aControl"></i></span>
                    </div> 
                                          
                                        
                </div>
        {/foreach}
        {/if}
      <div class="pridat"></div>
	  <div class="plus-ots">
        <i style="color:green;font-size:20px;cursor:pointer" class="fa fa-plus-square " aria-hidden="true"></i>
      </div>

      
    </div>




  </div>
  <!-- /train-phase -->
  <div class="faze3">
    <h3>FÁZE III</h3>
    <span>{if isset($tydenData[3]['misto'])}{$tydenData[3]['misto']}{/if}</span>
    {if isset($tydenData[3]['idTrener'])}<input type="hidden" class="idTrener" value="{$tydenData[3]['idTrener']}">{/if}
    <div class="train-phase">
        {if isset($tydenData[3]['aktivity'])}
      {if isset($tydenData[3]['aktivity'])}
            {foreach $tydenData[3]['aktivity'] key=id item=aktivita}
            <div class="train-item">
                  <div class="item"> 
                       {if $aktivita['iStav'] < 1} <i class="fas fa-question grey" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 2} <i class="fas fa-check-square green" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 3} <i class="fas fa-window-close red" style="font-size: 18pt;padding-right: 5px; margin-top:3px;"></i>
                          {elseif $aktivita['iStav'] == 4} <span class='presunutoDoFaze'>1</span> 
                          {elseif $aktivita['iStav'] == 5} <span class='presunutoDoFaze'>2</span> 
                          {elseif $aktivita['iStav'] == 6} <span class='presunutoDoFaze'>3</span> 
                                      {/if}
                      <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                      
                      <input type="hidden" class="idRadku" value="{$id}"> 
                  </div>
                  <div class="item">    
                      <span class="jinaFaze toFaze2" style="display: inline-block;" dofaze="4" title="Přesunout do fáze 1">1</span>
                      <span class="jinaFaze toFaze3" style="display:inline-block;" dofaze="5" title="Přesunout do fáze 2">2</span>
                    </div> 
                  <div class="item">
                      <select class="input-ots-id select-2" disabled="">
                        <option value='0'>Vyberte OTS/STS</option>
                        {foreach from=$ots key=k item=i}
                          <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                          {/foreach}
                        </select>
                    </div>
                    <div class="item">
                        <span class="aktivitaMargin inline-block">{$aktivita['sAktivita']}</span>
                    </div>
                    <div class="item">    
                      <span class="inline-block">{$aktivita['iOpakovani']}</span>
                    </div>  
                    <div class="item">  
                      <span class="nasobeni inline-block">x</span>
                    </div>
                    <div class="item">
                      <span class="inline-block">{$aktivita['vzdalenost']}</span>
                    </div>
                    <div class="item">  
                        <p class="mj inline-block">{$aktivita['ots_smj']}</p>
                    </div>
                    <div class="item">
                        <span title="Nesplnil jsem."><i class="fas fa-exclamation-circle red zamitnoutPolozku aControl"></i></span>
                        <span title="Splnil jsem."><i class="fas fa-check-circle confirmPolozku aControl"></i></span>
                    </div> 
                                          
                                        
                </div>
        {/foreach}
        {/if}
        {/if}
      <div class="pridat"></div>
	  <div class="plus-ots">
        <i style="color:green;font-size:20px;cursor:pointer" class="fa fa-plus-square " aria-hidden="true"></i>
      </div>

      
    </div>

    </div>
    <!-- /train-phase -->
  </div>
  <div class="day-row-3">
      <b>Poznámka od trenéra:</b> {if isset($pozn['sPoznamka'])}{$pozn['sPoznamka']}{/if}
      <div><b>Poznámka od atleta:</b></div>
      <textarea style="width:100%" rows="6" class="poznamkaAtleta" placeholder="Krátká poznámka od atleta...">{if isset($pozn['sPoznamkaAtlet'])}{$pozn['sPoznamkaAtlet']}{/if}</textarea>
      
  </div>







