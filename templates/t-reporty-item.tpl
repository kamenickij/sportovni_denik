<script>
  $(function () {
   $('.select-2').select2();
    var otuReg = $('.otuReg').html();
    var casReg = '{$tydenData[1]['casRegenerace']}' ;
   // console.log(otuReg);
   // console.log(casReg);
    if (isNaN(casReg)) casReg=0;
    var celkCasReg = parseInt(otuReg) + parseInt(casReg);
    $('.otuReg').html(celkCasReg);

  }); //function
</script>
<!-- tydenni-predpis-item.tpl -->
 {* pokud je nemoc a nebo zavod *}
{if $tydenData[1]['iNemoc'] == '1'}
    <h3>NEMOC</h3>
    poznámka k nemoci:{$tydenData[1]['sNemoc']}
{elseif $tydenData[1]['iZavod']}   
    <h3>ZÁVOD</h3> 
    počet startů:{$tydenData[1]['sZavod']}
{else}


    <div class="day-row-2 train-set">
    <div class="faze1">
        <div>
            {$tydenData[1]['iNemoc']}
            {$tydenData[1]['iZavod']}
            {$tydenData[1]['sNemoc']}
            {$tydenData[1]['sZavod']}
            {$tydenData[1]['casZatizeni']}
            {$tydenData[1]['casRegenerace']}
        </div>
        <h3>FÁZE I</h3>
        <!-- id="sFazeMisto" name="sFazeMisto" -->
        <span>{if isset($tydenData[1]['misto'])}{$tydenData[1]['misto']}{/if}</span>
        {if isset($tydenData[1]['idTrener'])}<input type="hidden" class="idTrener" value="{$tydenData[1]['idTrener']}">{/if}
        <div class="train-phase">        
        
                {if isset($tydenData[1]['aktivity'])}
                    {foreach $tydenData[1]['aktivity'] key=id item=aktivita}
                            <div class="train-item">
                                <div class="item">
                                    <span><i class="fas  
                                        {if $aktivita['iStav'] < 2}fa-question-circle grey {elseif $aktivita['iStav'] == 2 || $aktivita['iStav'] > 3 }fa-check-circle green {elseif $aktivita['iStav'] == 3} fa-times-circle red {/if} fa-lg"></i></span>
                                        {if $aktivita['iStav'] == 4}<span class='presunutoDoFaze'>1</span>
                                        {elseif $aktivita['iStav'] == 5}<span class='presunutoDoFaze'>2</span>
                                        {elseif $aktivita['iStav'] == 6}<span class='presunutoDoFaze'>3</span>
                                        {/if}
                                <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                                
                                <input type="hidden" class="idRadku" value="{$id}"> 
                            </div>
                            <div class="item">                     
                                <select class="input-ots-id select-2" disabled="">
                                <option value='0'>Vyberte OTS/STS</option>
                                {foreach from=$ots key=k item=i}
                                    <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="item">    
                                <span class="aktivitaMargin inline-block">{$aktivita['sAktivita']}</span>
                            </div>
                            <div class="item">        
                                <span class="inline-block">{$aktivita['iOpakovani']}</span>
                            </div>
                            <div class="item">    
                                <span class="nasobeni inline-block">x</span>
                            </div>
                            <div class="item">    
                                <span class="inline-block">{$aktivita['vzdalenost']}</span>
                            </div>
                            <div class="item">    
                                <p class="mj inline-block">{$aktivita['ots_smj']}</p>  
                            </div>                                         
                        </div>
                    {/foreach}
                {/if}
            
        
        </div>


    </div>
    <!-- /train-phase -->
    <div class="faze2">
        <h3>FÁZE II</h3>
        <span>{if isset($tydenData[2]['misto'])}{$tydenData[2]['misto']}{/if}</span>
        {if isset($tydenData[2]['idTrener'])}<input type="hidden" class="idTrener" value="{$tydenData[2]['idTrener']}">{/if}
        <div class="train-phase">
            {if isset($tydenData[2]['aktivity'])}
                {foreach $tydenData[2]['aktivity'] key=id item=aktivita}
                 <div class="train-item">
                                <div class="item">
                                    <span><i class="fas  
                                        {if $aktivita['iStav'] < 2}fa-question-circle grey {elseif $aktivita['iStav'] == 2 || $aktivita['iStav'] > 3 }fa-check-circle green {elseif $aktivita['iStav'] == 3} fa-times-circle red {/if} fa-lg"></i></span>
                                        {if $aktivita['iStav'] == 4}<span class='presunutoDoFaze'>1</span>
                                        {elseif $aktivita['iStav'] == 5}<span class='presunutoDoFaze'>2</span>
                                        {elseif $aktivita['iStav'] == 6}<span class='presunutoDoFaze'>3</span>
                                        {/if}
                                <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                                
                                <input type="hidden" class="idRadku" value="{$id}"> 
                            </div>
                            <div class="item">                     
                                <select class="input-ots-id select-2" disabled="">
                                <option value='0'>Vyberte OTS/STS</option>
                                {foreach from=$ots key=k item=i}
                                    <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="item">    
                                <span class="aktivitaMargin inline-block">{$aktivita['sAktivita']}</span>
                            </div>
                            <div class="item">        
                                <span class="inline-block">{$aktivita['iOpakovani']}</span>
                            </div>
                            <div class="item">    
                                <span class="nasobeni inline-block">x</span>
                            </div>
                            <div class="item">    
                                <span class="inline-block">{$aktivita['vzdalenost']}</span>
                            </div>
                            <div class="item">    
                                <p class="mj inline-block">{$aktivita['ots_smj']}</p>  
                            </div>                                         
                        </div>
            {/foreach}
            {/if}
        
        </div>




    </div>
    <!-- /train-phase -->
    <div class="faze3">
        <h3>FÁZE III</h3>
        <span>{if isset($tydenData[3]['misto'])}{$tydenData[3]['misto']}{/if}</span>
        {if isset($tydenData[3]['idTrener'])}<input type="hidden" class="idTrener" value="{$tydenData[3]['idTrener']}">{/if}
        <div class="train-phase">
            {if isset($tydenData[3]['aktivity'])}
        {if isset($tydenData[3]['aktivity'])}
                {foreach $tydenData[3]['aktivity'] key=id item=aktivita}
                 <div class="train-item">
                                <div class="item">
                                    <span><i class="fas  
                                        {if $aktivita['iStav'] < 2}fa-question-circle grey {elseif $aktivita['iStav'] == 2 || $aktivita['iStav'] > 3 }fa-check-circle green {elseif $aktivita['iStav'] == 3} fa-times-circle red {/if} fa-lg"></i></span>
                                        {if $aktivita['iStav'] == 4}<span class='presunutoDoFaze'>1</span>
                                        {elseif $aktivita['iStav'] == 5}<span class='presunutoDoFaze'>2</span>
                                        {elseif $aktivita['iStav'] == 6}<span class='presunutoDoFaze'>3</span>
                                        {/if}
                                <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                                
                                <input type="hidden" class="idRadku" value="{$id}"> 
                            </div>
                            <div class="item">                     
                                <select class="input-ots-id select-2" disabled="">
                                <option value='0'>Vyberte OTS/STS</option>
                                {foreach from=$ots key=k item=i}
                                    <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="item">    
                                <span class="aktivitaMargin inline-block">{$aktivita['sAktivita']}</span>
                            </div>
                            <div class="item">        
                                <span class="inline-block">{$aktivita['iOpakovani']}</span>
                            </div>
                            <div class="item">    
                                <span class="nasobeni inline-block">x</span>
                            </div>
                            <div class="item">    
                                <span class="inline-block">{$aktivita['vzdalenost']}</span>
                            </div>
                            <div class="item">    
                                <p class="mj inline-block">{$aktivita['ots_smj']}</p>  
                            </div>                                         
                        </div>
            {/foreach}
            {/if}
            {/if}
        
        </div>

        </div>
        <!-- /train-phase -->
    </div>
    <div class="day-row-3">
        <b>Poznámka od trenéra:</b> {if isset($pozn['sPoznamka'])}{$pozn['sPoznamka']}{/if}
        <div><b>Poznámka od atleta:</b></div>
        {if isset($pozn['sPoznamkaAtlet'])}{$pozn['sPoznamkaAtlet']}{/if}
    </div>
{/if}{* /if pokud je nemoc a nebo zavod *}








