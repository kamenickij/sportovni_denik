<h3>Porovnání s přiřazenou roční tabulkou </h3>
<span class="table1">
  <table class="porovnaniTable"> 
      <thead>
          <tr>
            <th>OTS</th>
            <th>Předpis</th>
            <th>Aktuálně</th>
            <th>Rozdíl</th>
          </tr>   
      </thead>
      <tbody>
  {foreach from=$rp key=ots item=num}
        {* nezobrazuj pokud je to nulova hodnota  *}
       {if $num.rocni != null || $num.rocni != 0 }  
            <tr iPKotu="{if isset($num.iPK)}{$num.iPK}{else}0{/if}">
                <td>{$num.nazev}</td>
                <td class="rp">{if isset($num.rocni)}{$num.rocni}{else}0{/if}</td>
                <td class="ap">{if isset($num.tyden)}{$num.tyden}{else}0{/if}</td>
                <td class="rozdil">{if isset($num.rozdil)}{$num.rozdil}{else}0{/if} </td>
            </tr>
      {/if}
  {/foreach}
      </tbody>
</table>
</span>
