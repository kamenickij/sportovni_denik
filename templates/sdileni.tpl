<script>
$(function() {
  $("#rocni-predpis").click(function(){
    var ajaxModul = $(this).attr("id");
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction="+ajaxModul,
      success: function(content){
        if(content){
          $("#page").html(content);
        }
      }
    });
    return false;
  });

  $("#tydenni-predpis").click(function(){
    var ajaxModul = $(this).attr("id");
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction="+ajaxModul,
      success: function(content){
        if(content){
          $("#page").html(content);
        }
      }
    });
    return false;
  });

  $("#pridatUser").click(function(){
    var idAdd = $("#idAdd").val();
    var emailAdd = $("#emailAdd").val();
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction=pridatUser&idAdd="+idAdd+"&emailAdd="+emailAdd,
      success: function(content){
        if(content){
          $("#page").html(content);
        }
      }
    });
    return false;
    
  });

$(".rocniTabulka").on("change", function(){
    var id_user = $(this).parent().attr("value");
    var iPK = $(this).val();
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction=priraditTabulku&id_user="+id_user+"&iPK="+iPK,
      success: function(content){
        if(content){
          infoSuccess(content);
        }
      }
    });
    return false;
    
  });
  $('#filtruj').click(function(){
    var dataString = $('#sdileni-filtr').serialize();
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction=sdileni-filtr&" + dataString,
      success: function(content){
        if(content){
          console.log(content);
        }
      }
    });
    return false;
    
  });
  // select2
  $('.rocniTabulka').select2();

  $(".fa-ban").click(function () {
    if (confirm("Opravdu chcete zrušit propojení s tímto uživatelem?") == true) {
      var id_user_deconnect = $(this).closest("tr").attr("value");
      $.ajax({
          type: "POST",
          url: "ajax/ajax.php",
          data: "sAction=sdileni-deconnect&id_user_deconnect=" + id_user_deconnect
        })
        .done(function (content) {
          $(".modal-info-container").html(content);
          $(".modal-info").show();
        })
        .fail(function () {
          $(".modal-info-container").html("<p>Došlo k chybě. Uživatel nebyl odebrán.</p>");
          $(".modal-info").show();
        });
  

    } else {
      return false;
    }
  });


});//function


</script>
{$messagePridani}
<h2 class="sekce">Sdílení</h2>
<h3>Nasdílet si nového atleta</h3>
<label for="idAdd">ID</label>
<input type="text" name="idAdd" id="idAdd" max="" />
<label for="emailAdd">Email</label>
<input type="email" name="emailAdd" id="emailAdd" max="" />
<button class="btn-filtr" id="pridatUser">PRIDAT</button>


<div style="clear:both"></div>


<h3>Výpis nasdílených atletů:</h3>
<span class="table1">
<table>
  <thead>
    <tr>
  <th>
    ID
  </th>
  <th>
    Jméno
  </th>
  <th>
    Příjmení
  </th>
  <th>
    Email
  </th>
  <th>
    Roční tabulka
  </th>
  <th>
    Zrušit
  </th>
  </tr>
</thead>
<tbody>
  {foreach from=$rozpisUsers key=k item=i}


  <tr class="rt-tr-input" value="{$i.id_user}">
    <td class="idUser">
      {$i.id_user}
    </td>
    <td colspan="">
      {$i.jmeno}
    </td>
    <td colspan="">
      {$i.prijmeni}
    </td>
    <td>
      {$i.email}
    </td>
    <td value="{$i.id_user}">
      <select class="rocniTabulka" style="width: 250px;">
        <option value="0">
          Žádná tabulka
        </option>
        {foreach from=$rocniTabulky key=k2 item=i2}
        <option value="{$i2.iPK}" {if $i2.iPK == $i.rocni_predpis_iPK }selected{/if} >
          {$i2.nazev}
        </option>
        {/foreach}
      </select>
    </td>
    <td style="text-align: center;">
      <i  style="cursor:pointer; font-size:18pt;" class="fa fa-ban iconHoverRed" aria-hidden="true" title="Zrušit sdílení s tímto uživatelem."></i>
    </td>
  </tr>
  {/foreach}
  </tbody>
</table>
</span>
