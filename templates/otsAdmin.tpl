

        
        <div class="OTU table1">
          <table>
              <thead>
                <tr>
                  <th colspan="">
                    ID
                  </th>
                  <th>Kód</th>
                  <th>Zkratka</th>
                  <th colspan="">
                    Název
                  </th>
                  <th colspan="">
                    MJ
                  </th>
                  <th>
                    SELECT SPEC.
                  </th>
                  {* <th>ULOŽIT</th> *}
                  <th>SMAZAT</th>
                  <th>EDIT</th>
                </tr>
              </thead>
              <tbody>
                {foreach from=$rozpisOts key=k item=i}
        
        
                <tr class="rt-tr-input">
                  <td class="iPK" value="{$i.iPK}">
                      {$i.iPK}
                </td>
                <td >
                  {$i.kod}
                </td>
                <td >
                  {$i.sZkrat}
                </td>
                <td colspan="">
                    {$i.sName}
                </td>
                <td colspan="">
                  {$i.sMj}
                </td >
                <td>
                  <select class="select2 OTUselect{$i.iPK} selectSave" multiple="multiple" style="width: 500px;">
                    {foreach from=$rozpisSpec key=k2 item=i2}
                      <option value="{$i2.iPK}" >{$i2.sName}</option>
                    {/foreach}
                  </select>
                </td>
                {* <td><i class="fas fa-save saveMultiselect" style="font-size: 18pt; cursor: pointer;"></i></td> *}
                <td class="center-text"><i class="fas fa-trash-alt deleteOtu" style="font-size: 18pt; cursor: pointer;"></i></td>
                <td class="center-text"><i class="fas fa-pencil-alt editOtu" style="font-size: 18pt; cursor: pointer;"></i></td>

        
                </tr>
                <script type="text/javascript">
                  $('.select2').select2();                 
                  arr = "{$i.spec}";
                  arr2 = arr.split(",");
                  console.log(arr2);
                  $('.OTUselect{$i.iPK}').val(arr2);
                </script>
                {/foreach}
                </tbody>
        
            </table></div>
            <script type="text/javascript">
              $(document).ready(function(){
                $('.select2').select2();

                $('.OTU').on('click', '.saveMultiselect', function(){
                  var otuIPK = 0;
                  var specValue =[];
                  var dataString = '';
                  otuIPK = $(this).parent().parent().find('.iPK').attr('value');
                  specValue = $(this).parent().parent().find('.select2').val();
                  specValue.toString();
                  dataString = "iPK="+otuIPK+"&specValue="+specValue+"&sAction=adminOtu&sAction2=nastavitSpecOtu";
                  console.log(dataString);
                  $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      console.log(content);
                      $('#hledatOtu').click();
                    }
                  });
                  return false;

                });
                
                
                $('.OTU').on('change', '.selectSave', function(){
                  var otuIPK = 0;
                  var specValue =[];
                  var dataString = '';
                  otuIPK = $(this).closest('tr').find('.iPK').attr('value');
                  specValue = $(this).closest('tr').find('.select2').val();
                  specValue.toString();
                  dataString = "iPK="+otuIPK+"&specValue="+specValue+"&sAction=adminOtu&sAction2=nastavitSpecOtu";
                  console.log(dataString);
                  $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      console.log(content);
                      
                    }
                  });
                  return false;

                });
                $('.OTU').on('click', '.deleteOtu', function(){
                  var otuIPK = 0;
                  
                  otuIPK = $(this).parent().parent().find('.iPK').attr('value');
                  
                  dataString = "sAction=adminDeleteOtu&iPK="+otuIPK;
                  console.log(dataString);
                  $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      alert('Smazáno.');
                      $('#hledatOtu').click();
                    }
                  });
                  return false;

                });

                $('.OTU').on('click', '.editOtu', function(){
                  var otuIPK = 0;
                  
                  otuIPK = $(this).parent().parent().find('.iPK').attr('value');
                  var dataString = "sAction=adminEditOtu&iPK="+otuIPK;
                  $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      $('.modal-global-content').html(content);
                      $('.modal-global').show();
                    }
                  });
                  
                  return false;

                });
              });//ready
            </script>
    
    

