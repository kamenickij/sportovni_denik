
<script>
  $(function () {
    //oznaceni tr bg svetle mddrou
    $(".rt-modal-table").on("click", "tbody tr", function () {
      $('.rt-modal-table').find('.bgsvmodra').removeClass('bgsvmodra');
      $(this).addClass('bgsvmodra');
    });

    $('#submit').click(function () {

      var data = $('#rocni_predpis_form').serialize();
      var test = $('#nazevTable').val();
      if (test == "") {
        alert("Je nezbytné doplnit název tabulky.");
        return;

      } else {
        
        $.ajax({
          type: "POST",
          url: "ajax/ajax.php",
          data: "sAction=rocni_rozpis_save&" + data,
          success: function (content) {
            if(content){
                //$(".modal-content-action").html(content); 
                var message = "Tabulka byla v pořádku uložena."; 
                infoSuccess(message);              
              } else {
                var message = "Tabulka byla v pořádku uložena."; 
                infoFail(message); 
              }
          }
        });
        return false;
      }
    });

    // rocni tabulka odebrani radku
    $(".rt-modal-table").on("click", ".fa-minus-square", function () {
      $(this).closest('tr').remove();
      return false;
    });
    //rocni tabulka pridani dalsiho radku
    $("#plus").click(function () {
          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",            
            data: "sAction=plus",
            success: function (content) {
              if (content) {
                $("#pridatTr").replaceWith(content);
               // $("table").find(".select-2").each(function () {
                  $(".select-2").select2();
               // });
              }
            }
          });

      return false;
    });
    $(".close").click(function () {
      $(".modal").css("display", "none");
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=rocni-predpis",
        success: function (content) {
          if (content) {
            $("#page").html(content);
          }
        }
      });

    });
  
    //select on change
    $('form').on('change', '.aktivita', function () {
       var aktivitaId = $(this).val();
       $(this).closest('td').attr('check', 0);
       var check = 0;
      //check jestli uz tuto aktivitu nema vybranou
      $(this).closest('tbody').find('.aktivita').each(function (){
          var eachVal = $(this).val();
          var checkAttr = $(this).closest('td').attr('check');
          checkAttr = parseInt(checkAttr);
          if (aktivitaId == eachVal && checkAttr == 1) {
            alert('Tuto aktivitu máze již v roční tabulce vybranou. Vyberte prosím jinou.');
            check = 1;
            
            return false;
          
          }
        });
        if (check == 1){
            $(this).val("0").trigger("change");
            return false;
          }
        $(this).closest('td').attr('check', 1);

      $(this).closest('tr').find('input').each(function () {
        //console.log($(this).attr('id'));
        var id = $(this).attr('id');
        if (id.indexOf('_') > 0) {
          id = id.substring(0, id.indexOf('_'));
        }
        $(this).removeAttr('id');
        $(this).removeAttr('name');
        $(this).attr('id', id + '_' + aktivitaId);
        $(this).attr('name', id + '_' + aktivitaId);


        // "this" is the current element in the loop
      });
      //$('div').each(function (index, value) {
      //  console.log('div' + index + ':' + $(this).attr('id'));
      //});
    });
    
    $('.rt-modal-table').on('change','#spec',function(){
        var spec_iPK = $(this).val();
        $.ajax({
        type: "POST",
        url: "ajax/ajax.php",        
        data: "sAction=get-spec-otu&spec_iPK="+spec_iPK,
        success: function (content) {
            $('.rt-tr-input').remove();
           //$('#pridatTr').append(content);
           $( content).insertBefore( "#pridatTr" );
        }
      });
       
        
        return false;
    });
    
    // select2
    $('.select-2').select2();


    



  }); //function



</script>

<span class="close modal-rt-close btn-sub-menu"><i class="fas fa-times"></i></span>

<div class="rt-modal-popis">
</div>
<!-- modal-rt.tpl-->
<div class="rt-modal-table">
    <div class="modal-content-action"></div>
    <form method="post" id="rocni_predpis_form" action="" onsubmit="return false;">
        <input type="text" placeholder="Název tabulky" id="nazevTable" name="nazevTable" style="min-width: 429px;" >
        <div style="clear: both;"></div>
        <label for="spec" style="margin-left: 35px; margin-right: 8px;">Vyberte specializaci:</label><select class="select-2" id="spec" name="spec" style="width: 250px;">
          <option value="0">Žádná</option>
          {foreach from=$rozpisSpec key=k2 item=i2}
                      <option value="{$i2.iPK}" >{$i2.sName}</option>
          {/foreach}
        </select>
  <table style="margin-top: 10px;" class="modal-rt-table-frozen">
    <thead>
      
      <tr>
        
        <th class="modal-rt-frozen-td" style="width:280px;">
          týden
        </th>
        <th>
          41
        </th>
        <th>
          42
        </th>
        <th>
          43
        </th>
        <th>
          44
        </th>
        <th>
          45
        </th>
        <th>
          46
        </th>
        <th>
          47
        </th>
        <th>
          48
        </th>
        <th>
          49
        </th>
        <th>
          50
        </th>
        <th>
          51
        </th>
        <th>
          52
        </th>
        <th>
          1
        </th>
        <th>
          2
        </th>
        <th>
          3
        </th>
        <th>
          4
        </th>
        <th>
          5
        </th>
        <th>
          6
        </th>
        <th>
          7
        </th>
        <th>
          8
        </th>
        <th>
          9
        </th>
        <th>
          10
        </th>
        <th>
          11
        </th>
        <th>
          12
        </th>
        <th>
          13
        </th>
        <th>
          14
        </th>
        <th>
          15
        </th>
        <th>
          16
        </th>
        <th>
          17
        </th>
        <th>
          18
        </th>
        <th>
          19
        </th>
        <th>
          20
        </th>
        <th>
          21
        </th>
        <th>
          22
        </th>
        <th>
          23
        </th>
        <th>
          24
        </th>
        <th>
          25
        </th>
        <th>
          26
        </th>
        <th>
          27
        </th>
        <th>
          28
        </th>
        <th>
          29
        </th>
        <th>
          30
        </th>
        <th>
          31
        </th>
        <th>
          32
        </th>
        <th>
          33
        </th>
        <th>
          34
        </th>
        <th>
          35
        </th>
        <th>
          36
        </th>
        <th>
          37
        </th>
        <th>
          38
        </th>
        <th>
          39
        </th>
        <th>
          40
        </th>
        
      </tr>
      </thead>
      <tbody>
      <tr class="rt-tr-input">
        
        <td check="1"  class="modal-rt-frozen-td" style="background-color:#fff;width:280px;">
            <span class="td-minus"><i  style="color:red;font-size:20px;cursor:pointer" class="fa fa-minus-square" aria-hidden="true"></i></span>
          <select class="aktivita select-2" name="specializace" style="width:250px;" >
                      <option value='0'>Vyberte OTS/STS</option>
                    {foreach from=$ots key=k item=i}
                        <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                        {/foreach}
                  </select>
      </td>
      <td colspan="1" nowrap>
       <span class="rtKey">41</span>
          <input type="text" id="41" name="41"/>
      </td>
      <td colspan="1" nowrap>
      <span class="rtKey">41</span>
          <input type="text" id="42" name="42"/>
      </td>
      <td colspan="1" nowrap>
          <input type="text" id="43" name="43"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="44" name="44"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="45" name="45"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="46" name="46"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="47" name="47"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="48" name="48"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="49" name="49"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="50" name="50" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="51" name="51"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="52" name="52" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="1" name="1" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="2" name="2" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="3" name="3" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="4" name="4" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="5" name="5" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="6" name="6" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="7" name="7" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="8" name="8" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="9" name="9" />
      </td>
      <td colspan="1">
          <input type="text" id="10" name="10" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="11" name="11"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="12" name="12"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="13" name="13"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="14" name="14"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="15" name="15"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="16" name="16"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="17" name="17"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="18" name="18"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="19" name="19"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="20" name="20"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="21" name="21"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="22" name="22"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="23" name="23"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="24" name="24"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="25" name="25"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="26" name="26"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="27" name="27"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="28" name="28"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="29" name="29"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="30" name="30"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="31" name="31"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="32" name="32" />
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="33" name="33"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="34" name="34"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="35" name="35"/>
      </td>
      <td colspan="1">
          <input type="text" id="36" name="36"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="37" name="37"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="38" name="38"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="39" name="39"/>
      </td>
      <td colspan="1">
      <span class="rtKey">41</span>
          <input type="text" id="40" name="40"/>
      </td>
      
      </tr>

      
      <tr id="pridatTr">

      </tr>

      
      </tbody>

    
  </table>
<i id="plus" style="color:green;font-size:28px;cursor:pointer;margin-top:10px;" class="fa fa-plus-square" aria-hidden="true"></i>
        <input type="submit" id="submit" value="ULOŽIT" class="btn-sub-menu modal-rt-ulozit">
</form>
</div>
