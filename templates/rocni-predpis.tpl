<script type="text/javascript">
    /* <!CDATA[ */
$(function() {
  // select2
    $('.select-2').select2();
  //  
  $("#rocni-tabulka").on("click", function(){
    var ajaxModul = $(this).attr("id");
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction="+ajaxModul,
      success: function(content){
        if(content){
          $(".modal-content").html(content);
            $(".modal").css( "display", "block" );
        }
      }
    });
    return false;
    });
//OTU/STU
  $("#seznam-ots").on("click", function(){
    var ajaxModul = $(this).attr("id");
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction="+ajaxModul,
      success: function(content){
        if(content){
          $("#page").html(content);
        }
      }
    });
    return false;
    });

    $(".rt-sdileni").on("click", function(){
      var ajaxModul = $(this).attr("ajax");
      var tableipk = $(this).attr("tableipk");
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction="+ajaxModul+"&tableipk="+tableipk,
        success: function(content){
          if(content){
            $(".modal-content").html(content);
              $(".modal").css( "display", "block" );
          }
        }
      });
      return false;

    });
    
    $(".rt-kopie").on("click", function(){
      var ajaxModul = $(this).attr("ajax");
      var iPK = $(this).attr("iPK");
      if (!confirm('Skutečně chcete zkopírovat tuto roční tabulku?')) {
          return false;
      }
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction="+ajaxModul+"&iPK="+iPK,
        success: function(content){            
          if(content){
             var message = 'Roční tabulka byla zkopírována.';
             infoSuccess(message);
             $('#filtruj').click();
          }
        }
      });
      return false;

    });

    $('#filtruj').on("click" ,function(){
          var name = $("#nazev").val();
          var atlet = $("#filtr-uzivatel-select").val();
    
          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data: "sAction=rocni-predpis&name="+name+"&atlet="+atlet,
            success: function(content){
              if(content){
                $("#page").html(content);
              }
            }
          });
                return false;
        });

    // kliknuti na zobraz tabulku
   /* $("#zobrazit-rt").click(function(){
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=zobrazit-rt",
        success: function(content){
          if(content){
            $(".modal-content").html(content);
            $(".modal").css( "display", "block" );
          }
        }
      });

    $(".modal").css( "display", "block" );
    });*/

  $("#container-rocni-tabulky").on ("click", ".rt-vypis-expand", function(){
    $(this).closest("div").next("div").toggle();
    return false;
  });
  $("#zpet").click(function(){
    $("#denik").click();
    return false;
  });


return false;
});//function

// kliknuti na zobraz tabulku
function getPredpisTable(iPrimaryKey){

 var data = 'sAction=zobrazit-rt&iPrimaryKey='+iPrimaryKey;

        $.ajax({
   type: "POST",
   url: "ajax/ajax.php",
   data: data,
   success: function(content){
     if(content){
       $(".modal-content").html(content);

     }
   }
 });

 $(".modal").css( "display", "block" );
 return false;
};
// klik na smazat tabulku
function delTable(iPK){
 $.ajax({
   type: "POST",
   url: "ajax/ajax.php",
   data: "sAction=smazat-rt&iPK="+iPK,
   success: function(content){
     alert(content);
     $('#filtruj').click();
     }
   });
    return false;
 }

/* ]]> */
</script>
<!--rocni-prdpis.tpl  -->
  <!-- The Modal -->
  <div class="modal">

  <!-- Modal content -->
  <div class="modal-content">

  </div>

  </div>


{* <h2 class="sekce">Deník / Roční předpis</h2> *}
      <div id="denik-menu-btns">
      
        <button id="rocni-tabulka" class="btn-sub-menu"><i class="far fa-calendar-alt" style="font-size:12pt; margin-right:5px;" aria-hidden="true"></i>Nová roční tabulka</button>
        <button id="seznam-ots" class="btn-sub-menu"><i class="fas fa-list-ol" style="font-size:12pt; margin-right:5px;" aria-hidden="true"></i>Seznam OTU / STU</button>
      </div>
      
     
      <div class="filtr">
        <div class="filtr-block">
          <label for="nazev">Název</label>
          <input type="text" name="nazev" id="nazev" max="" value="{$nazevTabulky}" >

        </div>

        <div class="filtr-block">
          <label for="filtr-uzivatel-select"> Uživatel</label>
          <select id="filtr-uzivatel-select" class="select-2" style="width: 250px;">
              <option value="0" {if $atlet == 0} selected {/if}>Všichni uživatelé</option>
          {foreach from=$selectUzivatelu item=i}
            <option value="{$i.id_user}" {if $atlet == $i.id_user} selected {/if}>{$i.id_user}-{$i.jmeno} {$i.prijmeni}-{$i.email}</option>
          {/foreach}
          </select>
        </div>


        <div style="clear:both"></div>

        <div class="filtr-block filtr-filtruj">
          <button id="filtruj" class="btn-filtr"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
        </div>


      </div>
      <div style="clear:both"></div>

      <div id="container-rocni-tabulky">
       {foreach from=$rozpisHeader key=k item=i}
           <div class="rocni-tabulka-vypis">
          <div class="rt-vypis-popis">
          {* <i class="fas fa-expand rt-vypis-expand" aria-hidden="true"></i> *}
          <h5>{$i['nazev']}</h5>
          </div>
        <div class="rt-vypis-detail">

          <button id="zobrazit-rt" class="btn-2" onclick="return getPredpisTable({$i['iPK']}); return false;"><i class="fas fa-eye icon"></i><i class="fas fa-pencil-alt icon"></i>ZOBRAZIT/UPRAVIT</button>
          <button class="btn-2" onclick="return delTable({$i['iPK']}); return false;"><i class="fas fa-trash-alt icon"></i>SMAZAT</button>

          <button tableipk="{$i['iPK']}" ajax="rt-sdileni" class="btn-2 rt-sdileni"><i class="fas fa-share-alt-square icon"></i>SDÍLENÍ</button>
          
          <button ajax="rt-kopie" class="btn-2 rt-kopie" iPK="{$i['iPK']}"><i class="fas fa-copy icon"></i>KOPÍROVAT</button>

        </div>
      </div>

           {/foreach}


      </div>






