<script>
    jQuery(function ($) {

        $('#logout').click(function () {
            $.ajax({
                type: "POST",
                url: "ajax/ajax.php",
                data: 'sAction=logoutUser',
                success: function (content) {
                    location.reload();
                }

            });
            return false;
        });
        $('#sidebar .itemClick').click(function () {
            //yavreni menu
            $('.sidebarContainer').hide();


            $(this).closest('#sidebar').find('.itemClick').removeClass('sidebarSelected');
            $(this).addClass('sidebarSelected');
            var ajaxModul = $(this).attr("id");
            $.ajax({
                type: "POST",
                url: "ajax/ajax.php",
                data: "sAction=" + ajaxModul,
                success: function (content) {
                    if (content) {
                        $("#page").html(content);
                    }
                }
            });
            return false;
        });
        $(".modal-info-close").click(function () {
            $(".modal-info").hide();
            $(".modal-info").removeClass('success');
            $(".modal-info").removeClass('fail');
        });
        //sidebar vyjizdeni submenu
        $('#sidebar').on('click', '.hasSubmenu', function () {
            $(this).find('.submenu').slideToggle('slow');
        });

        //dochazka zmena datumu
        $('#page').on('change', '#dateDochazka', function () {
            let date = $('#dateDochazka').val();
            $.ajax({
                type: "POST",
                url: "ajax/ajax.php",
                data: "sAction=oddil-dochazka&date=" + date
            }).done(function (content) {
                console.log('success');
                $("#page").html(content);


            }).fail(function () {
                console.log('fail');


            }).always(function () {

            });
        });
    }); //ready
</script>


<body>

<!-- Zacatek vrchniho navigacniho pruhu -->
<menu>
    <div class="item" id="home" style="padding:2px 5px 4px 5px">
        <img id="miniLogo" src="img/miniLogo.png"/>

    </div>
    <div class="item">
        <h2 id="logoText">Deník sportovce</h2>

    </div>

    {* <div id="hamburger-container">
        <i class="fas fa-bars" id="hamburger-logo" onclick="showMask()"></i>
    </div>
    <div id="hamburger-margin">
    </div> *}
</menu>
<!-- Konec vrchniho navigacniho pruhu -->

<div class="modal-info">
    <div class="modal-info-close">
        <i class="fa fa-times-circle" aria-hidden="true"></i>
    </div>
    <div class="modal-info-container">

    </div>
</div>
<div class="container-global">

    <!-- Zacatek Hlavniho Menu -->
    <div class="sidebarContainer">
        <div id="sidebar">
            <div class="item" id="ctrl" name="">
                <span class="colorSidebarHover closeSidebar"><i class="fas fa-window-close i"></i>MENU</span>
            </div>
            {if $smarty.session.ADM == 1 || $smarty.session.ATLET == 1 }
                <div class="item hasSubmenu" id="denik">
                    <i class="fas fa-book i"></i> Deník <i class="fas fa-angle-down i2"></i>
                    <div class="submenu" style="display:none;">
                        <div class="subitem itemClick "
                             id="rocni-predpis"{if $smarty.session.TRN !=1 } style="display: none;" {/if}><i
                                    class="fas fa-table i"></i>Roční předpis
                        </div>
                        <div class="subitem itemClick"
                             id="tydenni-predpis"{if $smarty.session.TRN !=1 } style="display: none;" {/if}><i
                                    class="fas fa-th-list i"></i>Týdenní předpis
                        </div>
                        <div class="subitem itemClick "
                             id="t-reporty" {if $smarty.session.TRN !=1 } style="display: none;" {/if}>
                            <i class="fas fa-check i"></i>T - reporty
                        </div>
                        <div class="subitem itemClick "
                             id="reporty" {if $smarty.session.TRN !=1 } style="display: none;" {/if}>
                            <i class="far fa-chart-bar i"></i>Reporty
                        </div>
                        <div class="subitem itemClick "
                             id="a-reporty" {if $smarty.session.ATLET !=1 } style="display: none;" {/if}>
                            <i class="fas fa-check i"></i>A - reporty
                        </div>
                        <div class="subitem itemClick "
                             id="sdileni" {if $smarty.session.TRN !=1 } style="display: none;" {/if}>
                            <i class="fas fa-share-alt i"></i>Sdílení
                        </div>
                    </div>
                </div>
            {/if}

            <div class="item hasSubmenu"
                 id="oddil" {if $smarty.session.TRN !=1 && $smarty.session.ADM !=1 } style="display: none;" {/if}>
                <i class="fas fa-home i"></i>Oddíl<i class="fas fa-angle-down i2"></i>
                <div class="submenu" style="display:none;">
                    <div class="subitem itemClick " id="oddil-clenove"><i class="fas fa-users i"></i>Členové</div>
                    <div class="subitem itemClick " id="oddil-dochazka"><i class="fas fa-clock i"></i>Docházka</div>
                    <div class="subitem itemClick " id="oddil-prispevky"><i class="fas fa-dollar-sign i"></i>Příspěvky</div>
                    <div class="subitem itemClick " id="reportyOddil">
                        <i class="far fa-chart-bar i"></i>Reporty
                    </div>

                </div>
            </div>
            <div class="item itemClick " id="admin" {if $smarty.session.ADM !=1 } style="display: none;" {/if}>
                <i class="fas fa-wrench i"></i>Admin
            </div>
            <div class="item itemClick " id="logout" name="logout">
                <i class="fas fa-sign-out-alt i"></i>Odhlásit
            </div>
        </div>
    </div>
    <!-- Konec Hlavniho Menu -->



    <div id="page">
    </div>
</div>
<!--//container global-->

</body>

</html>

<script>

</script>
