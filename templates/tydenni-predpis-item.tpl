<script>

  //ready
  $(function () {
    // select 2
   $('.select-2').select2();
   // zmena MJ po zmene selectu
   $('.train-phase').on('change','.input-ots-id', function(){
     var mj = $(this).closest('div').find('option:selected').attr('sMj');
     //console.log(mj);
     $(this).closest('div').find('.mj').text(mj);
     //ar iPK = $(this).closest('.train-item').attr('idOtu');
     var iPK = $(this).val();
    $(this).closest('div').attr('idotu', iPK);  

     iPK = 'rp_' + iPK;
     $(this).closest('.train-item').find('.rp_prepocet').removeClass();
     $(this).closest('.train-item').find('.p_prepocet').find('span').addClass(iPK).addClass('rp_prepocet');

   });
    
   
        

  
    

  }); //function
</script>
<!-- tydenni-predpis-item.tpl -->


<div class="day-row-2 train-set">
  <div class="faze1 phase" faze="1">
    <h3>FÁZE I</h3>
    <!-- id="sFazeMisto" name="sFazeMisto" -->
    <input type="text"  class="sFazeMisto" {if isset($tydenData[1]['misto'])}value="{$tydenData[1]['misto']}" {/if} placeholder="Zadejte místo">
    <button class="prepocitat2 btn-2">Vlozit STU</button>
    <button class="smazatFazeStu btn-2">Smazat STU</button>
    <button class="vlozitFazeStu btn-2">Vlozit STU externe</button>
    <div class="train-phase" >
        {if isset($tydenData[1]['aktivity'])}
            {foreach $tydenData[1]['aktivity'] key=id item=aktivita}
                <div class="train-item" idOtu="{$aktivita['idOTU']}">
                  <div class="item">
                    <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                    <input type="hidden" class="idRadku" value="{$id}">
                    <span class=""><i class="fa fa-minus-square minus-ots" aria-hidden="true"></span></i>
                  </div>  
                  <div class="item">
                    <select class="input-ots-id select-2">
                      <option value=''>Vyberte OTS/STS</option>
                      {foreach from=$ots key=k item=i}
                        <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                      {/foreach}
                    </select>
                  </div>
                  <div class="item">  
                    <input type="text" class="sActivity inline-block" value="{$aktivita['sAktivita']}" placeholder="Zadejte aktivitu">
                  </div>
                  <div class="item">  
                    <input class="input-opakovani inline-block" onchange="prepocitejVzOpak();"  type="text" value="{$aktivita['iOpakovani']}" placeholder="opakování" />
                  </div>
                  <div class="item">  
                    <span class="nasobeni inline-block">x</span>
                  </div>
                  <div class="item">  
                    <input class="input-vzdalenost inline-block" onchange="prepocitejVzOpak();"   type="text" value="{$aktivita['vzdalenost']}" placeholder="vzdálenost" />
                  </div>
                  <div class="item">  
                  <p class="opakovaniXvzdalenost inline-block">= {$aktivita['iOpakovani'] * $aktivita['vzdalenost']}</p>
                  </div>
                  <div class="item">
                  <p class="mj inline-block">{$aktivita['ots_smj']}</p>
                  </div>
                  <div class="item">
                  <p class="p_prepocet inline-block"> |RP: <span class="rp_prepocet rp_{$aktivita['idOTU']}"></span></p>
                  </div>
                  
                </div>
            {/foreach}
            {* {else}
                <div class="train-item" >
                    <input type="hidden" class="iPK" value="0">
                    <input type="hidden" class="idRadku" value="{$id}">
                  <i style="color:red;font-size:20px;cursor:pointer" class="fa fa-minus-square minus-ots" aria-hidden="true"></i>
                  <select class="input-ots-id select-2">
                      <option value='0'>Vyberte OTS/STS</option>
                    {foreach from=$ots key=k item=i}
                        <option value="{$i.iPK}" sMj="{$i.sMj}">{$i.sName}</option>
                        {/foreach}
                  </select>
                  <input type="text" class="sActivity" value="" placeholder="Zadejte aktivitu">
                  <input class="input-opakovani" type="text" value="" placeholder="opakování" />
                  <span class="nasobeni">x</span>
                  <input class="input-vzdalenost" type="text" value="" placeholder="vzdálenost" />
                  <p class="mj"></p>
                  <p class="p_prepocet"> |RP: <span class="rp_prepocet"></span></p>
                  
                </div> *}   
        {/if}
      <div class="pridat"></div>

      <div class="plus-ots">
        <i  class="fa fa-plus-square i" aria-hidden="true"></i>
      </div>
    </div>


  </div>
  <!-- /train-phase -->
  <div class="faze2 phase" faze="2">
    <h3>FÁZE II</h3>
    <input type="text" class="sFazeMisto" {if isset($tydenData[2]['misto'])}value="{$tydenData[2]['misto']}"{/if}  placeholder="Zadejte místo">
    <button class="prepocitat2 btn-2">Vlozit STU</button>
    <button class="smazatFazeStu btn-2">Smazat STU</button>
    <button class="vlozitFazeStu btn-2">Vlozit STU externe</button>
    <div class="train-phase" >
        {if isset($tydenData[2]['aktivity'])}
            {foreach $tydenData[2]['aktivity'] key=id item=aktivita}
            <div class="train-item" idOtu="{$aktivita['idOTU']}">
                  <div class="item">
                    <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                    <input type="hidden" class="idRadku" value="{$id}">
                    <span class=""><i class="fa fa-minus-square minus-ots" aria-hidden="true"></span></i>
                  </div>  
                  <div class="item">
                    <select class="input-ots-id select-2">
                      <option value=''>Vyberte OTS/STS</option>
                      {foreach from=$ots key=k item=i}
                        <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                      {/foreach}
                    </select>
                  </div>
                  <div class="item">  
                    <input type="text" class="sActivity inline-block" value="{$aktivita['sAktivita']}" placeholder="Zadejte aktivitu">
                  </div>
                  <div class="item">  
                    <input class="input-opakovani inline-block" onchange="prepocitejVzOpak();"  type="text" value="{$aktivita['iOpakovani']}" placeholder="opakování" />
                  </div>
                  <div class="item">  
                    <span class="nasobeni inline-block">x</span>
                  </div>
                  <div class="item">  
                    <input class="input-vzdalenost inline-block" onchange="prepocitejVzOpak();"   type="text" value="{$aktivita['vzdalenost']}" placeholder="vzdálenost" />
                  </div>
                  <div class="item">  
                  <p class="opakovaniXvzdalenost inline-block">= {$aktivita['iOpakovani'] * $aktivita['vzdalenost']}</p>
                  </div>
                  <div class="item">
                  <p class="mj inline-block">{$aktivita['idOTU']}</p>
                  </div>
                  <div class="item">
                  <p class="p_prepocet inline-block"> |RP: <span class="rp_prepocet rp_{$aktivita['idOTU']}"></span></p>
                  </div>
                  
                </div>
        {/foreach}
        {/if}
      <div class="pridat"></div>

      <div class="plus-ots">
        <i  class="fa fa-plus-square i" aria-hidden="true"></i>
      </div>
    </div>




  </div>
  <!-- /train-phase -->
  <div class="faze3 phase" faze="3">
    <h3>FÁZE III</h3>
    <input type="text" class="sFazeMisto" {if isset($tydenData[3]['misto'])}value="{$tydenData[3]['misto']}"{/if} placeholder="Zadejte místo">
    <button class="prepocitat2 btn-2">Vlozit STU</button>
    <button class="smazatFazeStu btn-2">Smazat STU</button>
    <button class="vlozitFazeStu btn-2">Vlozit STU externe</button>
    <div class="train-phase"  idOtu="{$aktivita['idOTU']}">
      {if isset($tydenData[3]['aktivity'])}
            {foreach $tydenData[3]['aktivity'] key=id item=aktivita}
            <div class="train-item" idOtu="{$aktivita['idOTU']}">
                  <div class="item">
                    <input type="hidden" class="iPK" value="{$aktivita['iPK']}">
                    <input type="hidden" class="idRadku" value="{$id}">
                    <span class=""><i class="fa fa-minus-square minus-ots" aria-hidden="true"></span></i>
                  </div>  
                  <div class="item">
                    <select class="input-ots-id select-2">
                      <option value=''>Vyberte OTS/STS</option>
                      {foreach from=$ots key=k item=i}
                        <option value="{$i.iPK}" sMj="{$i.sMj}" {if $aktivita['idOTU'] == $i.iPK} selected {/if}>{$i.sName}</option>
                      {/foreach}
                    </select>
                  </div>
                  <div class="item">  
                    <input type="text" class="sActivity inline-block" value="{$aktivita['sAktivita']}" placeholder="Zadejte aktivitu">
                  </div>
                  <div class="item">  
                    <input class="input-opakovani inline-block" onchange="prepocitejVzOpak();"  type="text" value="{$aktivita['iOpakovani']}" placeholder="opakování" />
                  </div>
                  <div class="item">  
                    <span class="nasobeni inline-block">x</span>
                  </div>
                  <div class="item">  
                    <input class="input-vzdalenost inline-block" onchange="prepocitejVzOpak();"   type="text" value="{$aktivita['vzdalenost']}" placeholder="vzdálenost" />
                  </div>
                  <div class="item">  
                  <p class="opakovaniXvzdalenost inline-block">= {$aktivita['iOpakovani'] * $aktivita['vzdalenost']}</p>
                  </div>
                  <div class="item">
                  <p class="mj inline-block">{$aktivita['idOTU']}</p>
                  </div>
                  <div class="item">
                  <p class="p_prepocet inline-block"> |RP: <span class="rp_prepocet rp_{$aktivita['idOTU']}"></span></p>
                  </div>
                  
                </div>
        {/foreach}
        {/if}
      <div class="pridat"></div>

      <div class="plus-ots">
        <i  class="fa fa-plus-square i" aria-hidden="true"></i>
      </div>
    </div>


    </div>
    <!-- /train-phase -->
  </div>
  <div class="day-row-3">
      <textarea style="width:100%" rows="6" class="poznamkaTrenera" placeholder="Krátká poznámka od trenéra...">{if isset($tydenData[1]['poznamka'])}{$tydenData[1]['poznamka']}{/if}</textarea>
  </div>







