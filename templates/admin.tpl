<script>
$(function() {
  //pridani a overeni noveho uzivatele
  $(".adminPridat").on("click", function(){

    var sAction = $(this).attr("sAction");
    var emailHledat = $("#emailHledat").val();
    var idHledat = $("#idHledat").val();
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: "sAction=" + sAction + "&emailHledat="+ emailHledat + "&idHledat=" + idHledat,
      success: function(content){
        if(content){
          $("#resultHledat").html(content);
        }
      }
    });
    return false;
    
    
  });
  $(".adminRegistruj").on("click", function(){
    var sAction = $(this).attr("sAction");
    sAction = '&sAction=' + sAction;
    var sData = $("#formRegistrace").serialize();
    sData = sData + sAction;
     var password = $('#heslo').val();
    var potvrHeslo = $('#Potvrheslo').val();
    var email = $('#email').val();
    var login = $('#login').val();
    var jmeno = $('#jmeno').val();
    var kod = $('#kod').val();
    var prijmeni = $('#prijmeni').val();
    if( password == '' && potvrHeslo == '' && email == '' && login == '' && jmeno == '' && prijmeni == ''){
      alert('Vyplňte pole: heslo, potvrzení hesla, email, login, jméno a příjmení.');
      return false;

    }
    else if (password != potvrHeslo){
      alert ('Heslo a potvrzení hesla nejsou stejná! Registraci nelze provést.');
      return false;
    } else {

    
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: sData,
      success: function(content){
        if(content){
          $("#resultRegistrace").html(content);
        }
      }
    });
    return false;
    }
    
  });
  $('#Potvrheslo').focusout(function(){
    var password = $('#heslo').val();
    var potvrHeslo = $('#Potvrheslo').val();
    if (password != potvrHeslo){
      alert ('Heslo a potvrzení hesla nejsou stejná!');

    }
  }); 
  
  $('#schovatOtu').click(function(){
    $('#ajaxOtu').toggle();
  });

  $('#hledatOtu').click(function(){
    var sAction = $(this).attr('sAction');
    var sAction2 = $(this).attr('sAction2');
    var id = $('#idOtu').val();
    var nazev = $('#nazevOtu').val();
    var dataString = 'sAction='+sAction+'&sAction2='+sAction2+'&id='+id+'&nazev='+nazev;
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: dataString,
      success: function(content){
          $('#ajaxOtu').html(content);
      }
      });
    
    return false;
    

  });

  $('#hledatSpec').click(function(){
    var sAction = $(this).attr('sAction');
    var sAction2 = $(this).attr('sAction2');
    var nazev = $('#nazevSpec').val();
    var dataString = 'sAction='+sAction+'&sAction2='+sAction2+'&nazev='+nazev;
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: dataString,
      success: function(content){
          $('#ajaxSpec').html(content);
      }
      });
    
    return false;
    

  });

   $('#novaSpecBtn').click(function(){

    var sAction = $(this).attr('sAction');
    var sAction2 = $(this).attr('sAction2');
    var nazev = $('#novaSpec').val();
    if(nazev == ''){
      alert('Je potřeba vyplnit jméno specializace.');
      return false;
    }
    var dataString = 'sAction='+sAction+'&sAction2='+sAction2+'&nazev='+nazev;

    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: dataString,
      success: function(content){
          $('#ajaxSpecInfo').html(content);
      }
      });
    
    return false;
    

  });

   //nove otu
   $('#newOtu').click(function(){
    var sAction = $(this).attr('sAction');
    var nazev = $('#newName').val();
    var mj = $('#newMj').val();
    var zkratka = $('#newZkratka').val();
    var kod = $('#newKod').val();
    if(isNaN(kod)){
      alert("Položka kód musí obsahovat číslo!");
      return false;
    }
    var dataString = 'sAction='+sAction+'&name='+nazev+'&mj='+mj+'&zkratka='+zkratka + '&kod=' + kod;
    $.ajax({
      type: "POST",
      url: "ajax/ajax.php",
      data: dataString,
      success: function(content){
        alert('Záznam byl vložen do DB.');
        $('#hledatOtu').click();
        $('#newName').val('');
        $('#newMj').val('');
        $('#newZkratka').val('');
      }
      });
    
    return false;
   });
  


});//function


</script>
<div>
  <h2 class="sekce">Admin</h2>
  <h3 class="adminH3">Vyhledání uživatele v DB</h3>
  <label for="idHledat">ID</label>
  <input type="text" name="idHledat" id="idHledat" max="" />
  <label for="emailHledat">Email</label>
  <input type="email" name="emailHledat" id="emailHledat" max="" />
  <div style="clear: both;"></div>
  <button sAction="adminOveritUser" class="adminPridat btn-filtr" style="margin-bottom: 15px;">VYHLEDAT</button>
  <button sAction="adminAllUser" class="adminPridat btn-filtr" style="margin-bottom: 15px;">VYPSAT VŠECHNY UŽIVATELE</button>

  <div id="resultHledat"></div>
</div>
<div>
  <h3 class="adminH3">Registrace nového uživatele do DB</h3>
  <form id="formRegistrace">
  <div>
    <label for="login">Login</label>
    <input type="text" name="login" id="login" placeholder="Zadej uživatelův email" />
    <label for="email">Email</label>
    <input type="email" name="email" id="email" max="" />
    <label for="email">Kod</label>
    <input type="text" name="kod" id="kodl" max="" />
  </div>
  <div>
    <label for="heslo">Heslo</label>
    <input type="password" name="heslo" id="heslo" max="" />
    <label for="heslo">Potvrzení hesla</label>
    <input type="password" name="Potvrheslo" id="Potvrheslo" max="" />
  </div>
  <div>
    <label for="jmeno">jmeno</label>
    <input type="text" name="jmeno" id="jmeno" max="" />
    <label for="prijmeni">prijmeni</label>
    <input type="text" name="prijmeni" id="prijmeni" max="" />
  </div>
  <div>
    <label for="typ">typ</label>
    <select name="typ" id="typ"  >
      <option value='ATL'>Atlet</option>
      <option value='TRN'>Trenér</option>
    </select>
    <label for="popis">popis</label>
    <input type="texta" name="popis" id="popis" max="" />
  </div>
  </form>
  <button sAction="adminRegistruj" class="adminRegistruj btn-filtr" style="margin-top: 0;">REGISTRUJ</button>
  <div id="resultRegistrace"></div>
</div>
<div>
  <h3 class="adminH3">Správa specializací</h3>
  <h4>Hledat specializaci:</h4>
  <label for="nazevSpec">Název:</label>
  <input type="text" name="nazevSpec" id="nazevSpec" max="" />
  <button sAction="adminOtu" sAction2="hledatSpec"  class="btn-filtr" id="hledatSpec" style="margin-top: 0;">HLEDAT V DB</button>
  <h4>Nová specializace:</h4>
  <label for="nazevSpec">Nová specializace:</label>
  <input type="text" name="novaSpec" id="novaSpec" max="" />
  <button sAction="adminOtu" sAction2="novaSpec"  class="btn-filtr" id="novaSpecBtn" style="margin-top: 0;">Nová specializace</button>
  <div id="ajaxSpecInfo"></div>
  <div id="ajaxSpec"></div>
</div>
<div>
  <h3 class="adminH3">Správa OTU a STU</h3>
  <h4>Nový OTU/STU:</h4>
  <label for="newName">Název:</label><input type="text" name="newName" id="newName">
  <label for="newMj">MJ:</label><input type="text" name="newMj" id="newMj">
  <label for="newZkratka">Zkratka:</label><input type="text" name="newZkratka" id="newZkratka">
  <label for="newKod">Kód:</label><input type="text" name="newKod" id="newKod">
  <button sAction="newOtu"  class="btn-filtr" id="newOtu" style="margin-top: 0;">VYTVOŘIT</button>
  <div style="clear: both; margin-bottom: 10px;"></div>
  <h4>Hledat OTU/STU:</h4>
  <!--
  <button sAction="schovatOtu"  class="btn-filtr" id="schovatOtu" style="margin-top: 0;">SCHOVAT OTU</button>-->
  <label for="idOtu">ID:</label>
  <input type="text" name="idOtu" id="idOtu" max="" />
    <label for="nazevOtu">Název:</label>
    <input type="text" name="nazevOtu" id="nazevOtu" max="" />
    <label for="specializaceOtu">Specializace</label>
    <input type="text" name="specializaceOtu" id="specializaceOtu" max="" />
    <button sAction="adminOtu" sAction2="hledatOtu"  class="btn-filtr" id="hledatOtu" style="margin-top: 0;">HLEDAT V DB</button>
  <div id="ajaxOtu"></div>
</div>
<!-- FOOTER -->
 <div style="height:150px;"></div>
  


