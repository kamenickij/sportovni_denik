<script>
//pripni cast buttonu filtru po scrollu
    $(window).on("scroll", function(){
      if($("body").scrollTop() > 380){
        $('.tydenControlPin').addClass('pin1');
      }else{
        $('.tydenControlPin').removeClass('pin1');
      }
    });
function prepocitejVzOpak(el){
  //prepocet input opakovani a vzdalenost
      $('#page').on('change', '.input-opakovani', function(){
        var opakovani = $(el).val();
        opakovani = parseFloat(opakovani);
        var vzdalenost = $(el).closest('div').find('.input-vzdalenost').val();
        vzdalenost = parseFloat(vzdalenost);
        var sum = opakovani * vzdalenost;
        if( isNaN(sum)){
          sum = 0;
        }
        $(this).closest('div').find('.opakovaniXvzdalenost').text('= '+sum);
      });
	   //prepocet input opakovani a vzdalenost
      $('#page').on('change', '.input-vzdalenost', function(){
        var vzdalenost = $(el).val();
        vzdalenost = parseFloat(vzdalenost);
        var opakovani = $(el).closest('div').find('.input-opakovani').val();
        opakovani = parseFloat(opakovani);
        var sum = opakovani * vzdalenost;
        if( isNaN(sum)){
          sum = 0;
        }
        $(this).closest('div').find('.opakovaniXvzdalenost').text('= '+sum);
      });
}    
//document ready
  $(function () {
    $('.select-2').select2();
    $("#zpet").click(function(){
      $("#denik").click();
      return false;
    });
    
    $("#rocni-predpis").click(function () {
      var ajaxModul = $(this).attr("id");
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=" + ajaxModul,
        success: function (content) {
          if (content) {
            $("#page").html(content);
          }
        }
      });
      return false;
    });
    $("#tydenni-predpis").click(function () {
      var ajaxModul = $(this).attr("id");
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=" + ajaxModul,
        success: function (content) {
          if (content) {
            $("#page").html(content);
          }
        }
      });
      return false;
    });

    $("#filtruj-tydenni-predpis").click(function () {
      var ajaxModul = $(this).attr("id");
      var id_user = $('#filtr-uzivatel-select').val();
      var selectedTyden = $('#selectedTyden').val();
      //aktualniTyden = selectedTyden;
     // alert(selectedTyden);
      $('#tyden-form').val(parseInt(selectedTyden)).trigger('change');

      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=" + ajaxModul + "&id_user_atlet=" + id_user + "&tyden=" + selectedTyden,
        success: function (content) {         
        if (content) {
            $(".day-row-container").html(content);
          }
        }
      });
      $('#selectedTyden').text(selectedTyden);
      return false;
    });
    // select2
    
    //pridani radku OTS
    //rocni tabulka pridani dalsiho radku
    
    //prepinani mezi tydny skrz buttony sipek
    //global scope
    
    
    var aktualniTyden = $("#selectedTyden").val();
    $("#tydenPlus").on("click", function(){
      aktualniTyden ++;
      if (aktualniTyden == 53){
        alert("Maximální týden je 52");
        aktualniTyden --;
      }
      $('#selectedTyden').text(aktualniTyden);
      $('#selectedTyden').val(aktualniTyden);
      $('#filtruj-tydenni-predpis').click();
      return false;
    });
    
    $("#tydenMinus").click(function(){
      aktualniTyden --;
      if (aktualniTyden == 0){
        alert("Hodnota týden nemůže být 0");
        aktualniTyden ++;
      }
      $('#selectedTyden').text(aktualniTyden);
      $('#selectedTyden').val(aktualniTyden);
      $('#filtruj-tydenni-predpis').click();
      return false;
    });
        
    $('#btnUlozitSdilet').click(function(){
      var idUserCopyFrom = $('#filtr-uzivatel-select').val();
      var dataString = "sAction=ulozitAsdiletModal&idUserCopyFrom=" + idUserCopyFrom;
      $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      $('.modal-global-content').html(content);
                      $('.modal-global').show();
                    }
                  });
                  
                  return false;

    });
    
    $('#btnUlozit').click(function(){
       var id_user = $('#filtr-uzivatel-select').val();
       //alert($('.day-row-container').serialize());
       data = { };
       $('div[class="day-row"]').each(function(){
            var datum = $(this).find('.date').text();
            
            var poznamka = $(this).find('.poznamkaTrenera').val();
        
            var faze1 = $(this).find('.faze1');
            var faze2 = $(this).find('.faze2');
            var faze3 = $(this).find('.faze3');
            nazev = faze1.find('.sFazeMisto').val();
            data[datum] = { };
            data[datum]['poznamka'] = poznamka;
            data[datum]['faze1'] = { };
            data[datum]['faze2'] = { };
            data[datum]['faze3'] = { };
            data[datum]['faze1']['misto'] = nazev;
            nazev = faze2.find('.sFazeMisto').val();
            data[datum]['faze2']['misto'] = nazev;
            nazev = faze3.find('.sFazeMisto').val();
            data[datum]['faze3']['misto'] = nazev;
            faze1.find('.train-item').each(function(){
                iOTS = $(this).find('.input-ots-id').val();      
                sActivity = $(this).find('.sActivity').val();  
                iPK = $(this).find('.iPK').val();
                idRadku = $(this).find('.idRadku').val();
                data[datum]['faze1'][iOTS+'_'+idRadku] = { };
                data[datum]['faze1'][iOTS+'_'+idRadku]['sActivity'] = sActivity;                
                data[datum]['faze1'][iOTS+'_'+idRadku]['iPK'] = iPK;
                inputOpakovani = $(this).find('.input-opakovani').val();
                data[datum]['faze1'][iOTS+'_'+idRadku]['inputOpakovani'] = inputOpakovani;
                inputVzdalenost = $(this).find('.input-vzdalenost').val();
                data[datum]['faze1'][iOTS+'_'+idRadku]['inputVzdalenost'] = inputVzdalenost;                
            })
             faze2.find('.train-item').each(function(){
                iOTS = $(this).find('.input-ots-id').val();      
                sActivity = $(this).find('.sActivity').val();
                iPK = $(this).find('.iPK').val();
                idRadku = $(this).find('.idRadku').val();
                data[datum]['faze2'][iOTS+'_'+idRadku] = { };
                data[datum]['faze2'][iOTS+'_'+idRadku]['sActivity'] = sActivity;
                data[datum]['faze2'][iOTS+'_'+idRadku]['iPK'] = iPK;
                inputOpakovani = $(this).find('.input-opakovani').val();
                data[datum]['faze2'][iOTS+'_'+idRadku]['inputOpakovani'] = inputOpakovani;
                inputVzdalenost = $(this).find('.input-vzdalenost').val();
                data[datum]['faze2'][iOTS+'_'+idRadku]['inputVzdalenost'] = inputVzdalenost;                
            })
             faze3.find('.train-item').each(function(){
                iOTS = $(this).find('.input-ots-id').val();      
                sActivity = $(this).find('.sActivity').val();
                iPK = $(this).find('.iPK').val();
                idRadku = $(this).find('.idRadku').val();
                data[datum]['faze3'][iOTS+'_'+idRadku] = { };
                data[datum]['faze3'][iOTS+'_'+idRadku]['sActivity'] = sActivity;
                data[datum]['faze3'][iOTS+'_'+idRadku]['iPK'] = iPK;
                inputOpakovani = $(this).find('.input-opakovani').val();
                data[datum]['faze3'][iOTS+'_'+idRadku]['inputOpakovani'] = inputOpakovani;
                inputVzdalenost = $(this).find('.input-vzdalenost').val();
                data[datum]['faze3'][iOTS+'_'+idRadku]['inputVzdalenost'] = inputVzdalenost;                
            })
        
        });
        //console.log($.toJSON(data));
        $.ajax({
        type: "POST",
        url: "ajax/ajaxJSON.php",
        data       : { 'data':JSON.stringify(data), 'id_user': JSON.stringify(id_user) },
        success: function (content) {
          if (content) {
           console.log(content);
            //$('.modal-info-container').html('Týdenní předpis je uložen');
           infoSuccess('Týdenní předpis je uložen');
          }
        }
      });
        return false;
    });

    $('#tyden-form').on('change', function(){
      var selectedTyden = $('#tyden-form option:selected').val();
      //$('#selectedTyden').text(selectedTyden);
      $('#selectedTyden').val(selectedTyden);
      $('#selectedTyden').html(selectedTyden);
      aktualniTyden = selectedTyden;
     // $('#filtruj-tydenni-predpis').click();
      return false;

    });

    //rozdil
        var rp =0;
        var ap = 0;
        var rozdil =0;
        $('.tydenni-predpis-porovnani').find('tr').each(function(){
          rp = $(this).find('.rp').text();
          ap = $(this).find('.ap').text();
          rozdil = rp - ap;
          $(this).find('.rozdil').text(rozdil);

        });
      //smazat stu ve fazi  
      $('.day-row-container').on('click','.smazatFazeStu',function(){
        $( ".ajaxWorking" ).fadeIn( "fast" );
        $(this).closest('div').find('.train-item').each(function(){
          $(this).find('.minus-ots').click();
        });
        $( ".ajaxWorking" ).fadeOut( "fast" );

      });  
      //prepocitat 2  
      $('.day-row-container').on('click','.prepocitat2',function(){
        console.log('start');
        $( ".ajaxWorking" ).fadeIn( "fast" );
        var porovnaniTable = [];
        var iPK = '';
        $('.porovnaniTable tbody').find('tr').each(function(index){
          iPK = $(this).attr('iPKotu');
          if(iPK == undefined || iPK == '' || iPK == '0' || iPK == 0){
            return true;
          }
          //console.log('iPK=' + iPK);
          porovnaniTable[index] = iPK;
          
        });
        //console.log(porovnaniTable);
        
        //projdi fazi
        var fazeOtu = [];
        $(this).closest('div').find('.train-item').each(function(index){
            var iPK = $(this).attr('idotu');
            if(iPK == undefined || iPK == '' || iPK == '0' || iPK == 0){
              return true;
            }
            fazeOtu[index] = iPK; 
            
        });
          //porovnat dve arrays aby nam vysli chbyejici otu
          chybejiciOtu = [];
          chybejiciOtu = jQuery.grep(porovnaniTable,function (item) {
            return jQuery.inArray(item, fazeOtu) < 0;
          });
         
          console.log(porovnaniTable);
          console.log(fazeOtu);
          console.log(chybejiciOtu);
          
          //vyplneni chybejicich otu v danem .day-row do faze 1
          for (var i=0; i < chybejiciOtu.length; i++){
            //pridat pole Otu
            $(this).closest('div').find('.plus-ots').click();
            //zmenit select na spravnou hdonotu
            $(this).closest('div').find('.plus-ots').prev().prev().find('.select-2').val(chybejiciOtu[i]).trigger("change");
              
          }

      
        
        console.log('complete');
        $( ".ajaxWorking" ).fadeOut( "fast" );
        

      });
      // prepocitat
      $('#prepocitat').on('click',function(){
          var sAction ='prepocitat-tydenni-predpis';
          var soucet;
          var id_user = $('#filtr-uzivatel-select').val();
            var selectedTyden = $('#selectedTyden').val();
            
           OTU = { };
          var firstWay;
          $('div[class="day-row"]').each(function(){
            $(this).find('.train-item').each(function(){
              
                var iOTS = $(this).find('.input-ots-id').val();      
              var inputOpakovani = $(this).find('.input-opakovani').val();
              var inputVzdalenost = $(this).find('.input-vzdalenost').val();
              inputOpakovani = 10;
              inputVzdalenost = 10;
                if (iOTS > 0) {
                    if (typeof(OTU[iOTS]) !== 'object') {    
                       OTU[iOTS] = { };  
                    }
                    if (isNaN(OTU[iOTS]['soucet'])) {
                        OTU[iOTS]['soucet'] = 0;
                    }
                    soucet = (parseFloat(inputOpakovani) * parseFloat(inputVzdalenost));
                    if (isNaN(soucet)) soucet = 0;
                  //  alert(soucet);
                    OTU[iOTS]['soucet'] += soucet;
                    firstWay = 0;
                }
            });
          }); 
          
          $.ajax({
            type: "POST",
            url: "ajax/ajaxJSONporovnani.php",
            data       : { 'data':JSON.stringify(OTU), 'id_user': JSON.stringify(id_user), 'selectedTyden':JSON.stringify(selectedTyden) },
            success: function (content) {
              if (content) {
                  $('.tydenni-predpis-porovnani').html(content);
                  var porovnani = [];
                  var iPK = "";
                  var rozdil = "";
                  $(".porovnaniTable tbody").find("tr").each(function(){
                      iPK = $(this).attr("iPKotu");
                      if(iPK == undefined || iPK == "" || iPK == '0' || iPK == 0 ){
                        return true;
                      }
                      iPK = ".rp_" + iPK;
                      rozdil = $(this).find(".rozdil").text();
                     
                      porovnani[iPK] = rozdil; 
                  });//each
                console.log( porovnani);
                //index jsou nazvy class
                for(var index in porovnani) {
                  $(index).text(porovnani[index]);
                  
                }
              }//if content
            }//success ajax
          });//ajax

        
          
          return false;
        });
        

    


  }); //function
</script>
<!-- tydenni-predpis.tpl -->

<div class="filtr">
  <div class="filtr-block">
          <h3  style="margin:0">Dnes je <span class="Dnesje">{$aktualniDen}</span> - <span class="Dnesje">{$aktualniTyden}. týden</span></h3>
        </div>
  <div class="filtr-block width100">
    <label for="tyden-from">Týden</label>
    <select name="tyden-form" id="tyden-form" class="select-2" style="width:60px; display:inline">
      {for $foo=1 to 52}
      <option value="{$foo}" {if $aktualniTyden == $foo}selected{/if}>{$foo}</option>

      {/for}
    </select>
    <div class="tydnyBtns">
    <button id="tydenMinus" class="btn-filtr btn-tyden" style="margin-right:4px;">
      <i class="fa fa-arrow-left" aria-hidden="true"></i>
    </button>
    <button class="btn-filtr" style="padding: 4px 6px; margin-right:4px;" id="selectedTyden" value="{$aktualniTyden}">{$aktualniTyden}</button>
    <button id="tydenPlus" class="btn-filtr btn-tyden">
      <i class="fa fa-arrow-right" aria-hidden="true"></i>
    </button>
    </div>
  </div>
  <div class="filtr-block">
    <label for="filtr-uzivatel-select">Uživatel</label>
    <select id="filtr-uzivatel-select" class="select-2" style="width:350px;">
      {foreach from=$trenerAtleti key=k item=i}
      <option value="{$i.id_user}">[{$i.email}] {$i.titul} {$i.jmeno} {$i.prijmeni}</option>
      {/foreach}
    </select>
  </div>


  <div style="clear:both"></div>

  <div class="filtr-block filtr-filtruj">
    <button class="btn-filtr" id="filtruj-tydenni-predpis"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
  </div>
  <span class="tydenControlPin">
  <div class="filtr-block filtr-filtruj">
      <button class="btn-2" id="btnUlozit"><i class="fas fa-save icon"></i>ULOŽIT</button>
  </div>
  <div class="filtr-block filtr-filtruj">
      <button class="btn-2" id="btnUlozitSdilet"><i class="fas fa-share-alt-square icon"></i>ULOŽIT A NASDÍLET</button>
  </div>
  <div class="filtr-block filtr-filtruj">
      <button class="btn-2" id="prepocitat"><i class="fas fa-calculator icon"></i>PŘEPOČÍTAT</button>
  </div>
  </span>
  
  
</div>
<!--/filtr -->
<div style="clear:both"></div>





<div class="day-row-container">
    
</div>

  