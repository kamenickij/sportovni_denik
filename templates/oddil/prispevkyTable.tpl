<span class="table1" id="prispevkyTable">
<table>
	<thead>
		<tr>
			<th>Jméno</th>
			<th>Příjmení</th>
			<th>Ročník</th>
			<th>Poznámka</th>
			<th>Datum založení</th>
			<th>Datum posledního updatu příspěvku</th>
			<th>Placeno do</th>
			<th>Částka</th>
			<th>Typ</th>
			<th>Poznámka platba</th>
			<th>Uložit</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$aClenove item=clen}
    		<tr id_user="{$clen.iPK}">
			
				<td>
					{$clen.sJmeno}
				</td>
				<td>
					{$clen.sPrijmeni}
				</td>
				<td>
					{$clen.sRocnik}
				</td>
				<td>
					{$clen.sText}
				</td>
				<td>
					{$clen.dCreate|date_format:"%d.%m.%Y %H:%M"}
				</td>
				<td>
					{$clen.dUpdatePlacenoDo|date_format:"%d.%m.%Y %H:%M"}
				</td>
				<td>
				<input type="date" class="dPlacenoDo" value="{$clen.dPlacenoDo}">
					
				</td>
				<td>
				<input type="number" class="iCastka" value="{$clen.iCastka}" style="width: 70px;">
					
				</td>
				<td>
					<select class="sTypPlatby">
						<option value="mes" {if $clen.sTypPlatby == 'mes'}
						selected="selected"
							
						{/if}>Měsíční</option>
						<option value="ctvrt" {if $clen.sTypPlatby == 'ctvrt'}
						selected="selected"
							
						{/if}>Čtvrtletní</option>
						<option value="roc" {if $clen.sTypPlatby == 'roc'}
						selected="selected"
							
						{/if}>Roční</option>
						<option value="jin" {if $clen.sTypPlatby == 'jin'}
						selected="selected"
							
						{/if}>Jiné</option>
					</select>
				</td>
				<td>
					<textarea class="sPoznamka" value="{$clen.sPoznamka}">{$clen.sPoznamka}</textarea>
					
				</td>
				<td>
					<span title="Uložit záznam člena oddílu"><i class="fas fa-save fa-2x savePrispevek" style="cursor:pointer;"></i></span>
				</td>
				
			</tr>
		{/foreach}
		
	</tbody>
</table>
</span>

<script>
	$(document).ready(function(){
		//ulozit zaznam
		$('#prispevkyTable').on('click', '.savePrispevek', function(){
			let dPlacenoDo = $(this).closest('tr').find('.dPlacenoDo').val();
			let iCastka = $(this).closest('tr').find('.iCastka').val();
			let sTypPlatby = $(this).closest('tr').find('.sTypPlatby option:selected').val();
			let sPoznamka = $(this).closest('tr').find('.sPoznamka').val();
			let id_user =  $(this).closest('tr').attr('id_user');
			let dataString = 'sAction=prispevekClenSave&dPlacenoDo=' + dPlacenoDo + '&iCastka=' + iCastka + '&sTypPlatby=' + sTypPlatby + '&sPoznamka=' + sPoznamka + '&id_user=' + id_user;
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				
			
				

			}).fail(function(){
				console.log('fail');
				let message = 'Nepodařilo se uložit záznam. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});
		});//save
	});//ready
</script>