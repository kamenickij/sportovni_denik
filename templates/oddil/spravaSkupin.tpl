<h2>SPRÁVA SKUPIN</h2>


<div class="filtr">
	<form id="form_vytvor_skupinu">
		<div class="filtr-block" style="width:100%;">
			<h3>Vytvořit novou skupinu</h3>
		</div>
		<div class="filtr-block">
			<label>Jméno skupiny:</label>
			<input type="text" name="skupina_name" id="skupina_name">
		</div>
		<div class="filtr-block">
			<label>Poznámka:</label>
			<textarea name="skupina_poznamka" id="skupina_poznamka"></textarea>
		</div>
		<div class="filtr-block" style="width:100%;">
			<button type="button" class="btn-filtr" id="ulozitSkupinu">ULOŽIT SKUPINU</button>
		</div>
	</form>
</div>


<button class="btn-2" id="vypsatSkupiny">VYPSAT SKUPINY</button>

<div class="ajaxSkupiny">
</div>

<div class="filtr">
	<form id="filtrujClen">  
		<div class="filtr-block">
			<label for="filtr_name">Jméno:</label>
			<input type="text" id="filtr_name" name="filtr_name" >
		</div>
		<div class="filtr-block">
			<label for="filtr_name2">Příjmení:</label>
			<input type="text" id="filtr_name2" name="filtr_name2" >
		</div>
		<div class="filtr-block">
			<label for="filtr_rocnik">Ročník:</label>
			<input type="text" id="filtr_rocnik" name="filtr_rocnik" >
		</div>
		<div class="filtr-block">
		<label for="filtr_skupina">Skupina:</label>
			<select id="filtr_skupina" name="filtr_skupina">
				<option>Skupina1</option>
			</select>
		</div>
  </form>
	


  <div style="clear:both"></div>

  <div class="filtr-block filtr-filtruj">
    <button type="button" class="btn-filtr" id="filtruj-clenove"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
  </div>
  
  
  
</div>
<!--/filtr -->
<div style="clear:both"></div>

<div class="ajaxTable">
</div>

<script>
	$(document).ready(function(){
		$('#ulozitSkupinu').click(function(){
			let dataString = $('#form_vytvor_skupinu').serialize();
			dataString = dataString + '&sAction=vytvorSkupinu';
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				if (content == 1){
					let message = 'Skupina byl úspěšně uložena.';
					infoSuccess(message);
				} else{
					let message = 'Skupinu se nepodařilo uložit do DB.';
					infoFail(message);
				}

			}).fail(function(){
				console.log('fail');
				let message = 'Skupinu se nepodařilo uložit. Chyba serveru.';
				infoFail(message);
				

			}).always(function(){
				console.log('always');

			});
		});

		$('#vypsatSkupiny').click(function(){
			dataString = '&sAction=vypisSkupiny';
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				$('.ajaxSkupiny').html(content);

			}).fail(function(){
				console.log('fail');
				let message = 'Nepovedlo se načíst skupiny. Chyba serveru.';
				infoFail(message);
				

			}).always(function(){
				console.log('always');

			});
		});
	});
</script>