{assign "datum" ""}
<span class="tableRepDoch">
<table>
    <thead>
        <tr>
        <th>
            Datum
        </th>
        <th>
            Přítomní 
        </th>
        </tr>
    </thead>
    <tbody>
        
            {foreach from=$aTable item=item  }
             
                {if $item.dDate != $datum}
                     <tr class="oddelovnik">
                     <td >{$item.dDate}</td>
                     <td>{$item.sJmeno} {$item.sPrijmeni}</td>

                        
                {else}
                    <tr>
                    <td></td>
                    <td>{$item.sJmeno} {$item.sPrijmeni}</td>
                {/if}

               
                
                {assign "datum" $item.dDate}
                
                </tr>
                

            {/foreach}
    </tbody>
</table>
</span>
    
