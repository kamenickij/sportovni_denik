{* <button type="button" class="btn-2" id="sprava_skupin">SPRÁVA SKUPIN</button> *}
<button type="button" class="btn-2" style="margin-bottom:30px; margin-top:25px;" id="nasdiletKsobeClena">NASTAVIT K SOBĚ ČLENY ODDÍLU</button>
{* <button type="button" class="btn-2" id="zobrazNasdilene">ZOBRAZIT NASDÍLENÉ</button> *}

<div class="dochazka_nav" >
	
	<div class="item">
	<label for="date">Docházka v den:</label>
		<input type="date" id="dateDochazka" value="{$today}">
	</div>
	
  </div>
<!--/filtr -->
<div style="clear:both"></div>


<div class="ajaxTable">
	{include file='../../templates/oddil/dochazkaTable.tpl' }
</div>

<script>
	$(document).ready(function(){
		$('#ulozitTableHeader').click(function(){
			let dataString = $('#dochazkaTableHeaderForm').serialize();
			let date = $('#dateDochazka').val();
			dataString = dataString + '&sAction=dochazkaTableHeader&date=' + date;
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
						console.log('success');
			console.log(content);
						if (content == 1 || content == '1'){
							let message = 'Stav byl úspěšně změněn.';
							infoSuccess(message);
						} else{
							let message = 'Chyba DB.';
							infoFail(message);
						}
				

			}).fail(function(){
						console.log('fail');
						let message = 'Chyba serveru.';
							infoFail(message);

			}).always(function(){

			});
		});
		$('.dochazkaTable').on('click', '.divClen', function(){
			$(this).find('.chPritomen').click();

		});
		$('.dochazkaTable').on('click', '.chPritomen', function(){
			let idClen = $(this).closest('.divClen').attr('iPK');
			let date = $('#dateDochazka').val();
			let ch = $(this).is(':checked');
			let dataString = "sAction=set_dochazka_den_clen&idClen=" + idClen + "&date=" + date + "&checked=" + ch;
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
						console.log('success');
			console.log(content);
						if (content == 1 || content == '1'){
							let message = 'Stav byl úspěšně změněn.';
							infoSuccess(message);
						} else{
							let message = 'Chyba DB.';
							infoFail(message);
						}
				

			}).fail(function(){
						console.log('fail');
						let message = 'Chyba serveru.';
							infoFail(message);

			}).always(function(){

			});
		});
		//zaskrtni vsechny
		$('.dochazkaTable').on('click', '#checkVsichni', function(){
			$('.dochazkaTable tbody tr').find('.chPritomen').each(function(){
				let isClicked = $(this).prop( "checked");
				if (isClicked){

				}else{
					$(this).click();
				}
			});

		});
		//odskrtni vsechny
		$('.dochazkaTable').on('click', '#uncheckVsichni', function(){
			$('.dochazkaTable tbody tr').find('.chPritomen').each(function(){
				let isClicked = $(this).prop( "checked");
				if (isClicked){
					$(this).click();
				}else{
					
				}
			});

		});
		//sprava skupin
		$('#nasdiletKsobeClena').click(function(){
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: "sAction=dochazka_nastavit_k_sobe_clena"
			}).done(function(content){
				console.log('success');
				$("#page").html(content);
			
				

			}).fail(function(){
				console.log('fail');
				

			}).always(function(){

			});

		});
		{* //filtruj clenove
		$('#filtruj-clenove').click(function(){
			let dataString = $('#filtrujClen').serialize();
			dataString = dataString + '&sAction=oddil_filtr_prispevky';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				$('.ajaxTable').html(content);
			
				

			}).fail(function(){
				console.log('fail');
				let message = 'Uživatele se nepodařilo filtrovat. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});

		}); *}
		$('#zobrazNasdilene').click(function(){
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: "sAction=zobrazSeznamDochazkaNasdileni"
			}).done(function(content){
				console.log('success');
				console.log(content);
				$('.ajaxTable').html(content);
			}).fail(function(){
				console.log('fail');
				let message = 'Uživatele se nepodařilo filtrovat. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});
			


		});
		

		
	});//ready
</script>
