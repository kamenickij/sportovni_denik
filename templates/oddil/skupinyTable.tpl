<span class="table1">
	<table>
		<thead>
			<tr>
				<th>
				Info
				</th>
				<th>
				Jméno
				</th>
				<th>
				Poznámka
				</th>
				<th>
				SMAZAT
				</th>
			</tr>	
		</thead>
		<tbody>
			{foreach from=$aSkupiny item=foo}
    			<tr iPK = "{$foo.iPK} ">
					<td>
						<i class="fas fa-info-circle infoSkupina"></i>
					</td>
					<td>
						{$foo.sJmeno}
					</td>
					<td>
						{$foo.sPoznamka}
					</td>
					<td>
						<i class="fas fa-trash fa-lg deleteSkupina"></i>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
</span>
