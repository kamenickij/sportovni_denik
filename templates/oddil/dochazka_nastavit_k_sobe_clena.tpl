<div class="filtr">
	<form id="filtrujClen">  
		<div class="filtr-block">
			<label for="filtr_name">Jméno:</label>
			<input type="text" id="filtr_name" name="filtr_name" >
		</div>
		<div class="filtr-block">
			<label for="filtr_name2">Příjmení:</label>
			<input type="text" id="filtr_name2" name="filtr_name2" >
		</div>
		<div class="filtr-block">
			<label for="filtr_rocnik">Ročník:</label>
			<input type="text" id="filtr_rocnik" name="filtr_rocnik" >
		</div>
        <div class="filtr-block">
			<label for="filtr_shared">Nasdílený</label>
			<input type="checkbox" id="filtr_shared" name="filtr_shared" >
		</div>
		<div class="filtr-block">
			<label for="filtr_trener">Trenér</label>
			<input type="checkbox" id="filtr_trener" name="filtr_trener" >
		</div>
		<div class="filtr-block">
			<label for="filtr_archive">Archivovaný</label>
			<input type="checkbox" id="filtr_archive" name="filtr_archive" >
		</div>
  </form>
	


  <div style="clear:both"></div>

  <div class="filtr-block filtr-filtruj">
    <button type="button" class="btn-filtr" id="filtruj-clenove"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
  </div>

  </div>
<!--/filtr -->
<div style="clear:both"></div>

<div class="ajaxTable">
</div>

<script>
	$(document).ready(function(){
		
		//filtruj clenove
		$('#filtruj-clenove').click(function(){
			let dataString = $('#filtrujClen').serialize();
			dataString = dataString + '&sAction=oddil_filtr_nastavit_k_sobe_clena';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				$('.ajaxTable').html(content);
			
				

			}).fail(function(){
				console.log('fail');
				let message = 'Uživatele se nepodařilo filtrovat. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});

		});

		
	});//ready
</script>