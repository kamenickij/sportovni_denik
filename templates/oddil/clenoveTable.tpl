<span class="table1 clenoveTable">
<table>
	<thead>
		<tr>
			<th>Jméno</th>
			<th>Příjmení</th>
			<th>Ročník</th>
			<th>Poznámka</th>
			<th>Datum založení</th>
			<th>Datum posledního updatu</th>
			<th>Trenér</th>
			<th>Archivovaný</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$aClenove item=clen}
    		<tr style="cursor:pointer;" iPK="{$clen.iPK}">
				
				<td>
					{$clen.sJmeno}
				</td>
				<td>
					{$clen.sPrijmeni}
				</td>
				<td>
					{$clen.sRocnik}
				</td>
				<td>
					{$clen.sText}
				</td>
				<td>
					{$clen.dCreate|date_format:"%d.%m.%Y %H:%M"}
				</td>
				<td>
					{$clen.dUpdate|date_format:"%d.%m.%Y %H:%M"}
				</td>
				<td>
					{$clen.iTrener}
				</td>
				<td>
					{$clen.iArchive}
				</td>
			</tr>
		{/foreach}
		
	</tbody>
</table>
</span>

<script>
	$(document).ready(function(){
		$('.clenoveTable tbody').on('click', 'tr', function(){
			let iPK = $(this).closest('tr').attr('iPK');
			let dataString = '&sAction=detailClenoveTable&iPK=' + iPK;
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
						console.log('success');
						$('.modal-global-content').html(content);
						$('.modal-global').fadeIn('slow');
			
				

			}).fail(function(){
						console.log('fail');
						let message = 'Chyba serveru.';
							infoFail(message);

			}).always(function(){

			});
		})//click
	});//ready
</script>