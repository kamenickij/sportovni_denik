<div class="reportHeaderSection">
	<h2 toggle="clen">ČLENOVÉ <i class="fas fa-angle-down i2"></i></h2>
</div>
<div class="reportSection" toggle="clen">
	<div class="reportSection-uzivatele flex-row">
		<div class="item">
			<i class="fas fa-users i"></i>
			<h3>Celkem členů v databázi</h3>
			<h2 class="numero">{$aClenove}</h2>
			<h3 >100%</h3>
		</div>
		<div class="item">
			<i class="fas fa-user-check i"></i>
			<h3>Aktivní</h3>
			<h2 class="numero">{$aAktivni}</h2>
			<h3 >{($aAktivni / $aClenove * 100)|number_format:2:".":""} %</h3>
		</div>
		<div class="item">
			<i class="fas fa-user-clock i"></i>
			<h3>Aktivní - Trenéři</h3>
			<h2 class="numero">{$aTreneri}</h2>
			<h3 >{($aTreneri / $aClenove * 100)|number_format:2:".":""} %</h3>
		</div>
	</div>
</div>

<div class="reportHeaderSection">
	<h2 toggle="dochazka">DOCHÁZKA <i class="fas fa-angle-down i2"></i></h2>
</div>
<div class="reportSection" toggle="dochazka" style="display:none;">
	<div class="reportSection-dochazka">
		<div class="reportSection-dochazka-filtr">
			<div class="filtr">
				<form id="filtrujDochazka">  
				<div class="filtr-block">
						<label for="od">Od:</label>
						<input type="date" id="od" name="od" value="{$last_week}" >
						<label for="do">Do:</label>
						<input type="date" id="do" name="do" value="{$today}">
					</div>
					<div class="filtr-block">
						<label for="jmeno">Jméno:</label>
						<input type="text" id="jmeno" name="jmeno" >
					</div>
					<div class="filtr-block">
						<label for="prijmeni">Příjmení:</label>
						<input type="text" id="prijmeni" name="prijmeni" >
					</div>
					<div class="filtr-block">
						<label for="rocnik">Ročník:</label>
						<input type="text" id="rocnik" name="rocnik" >
					</div>
					<div class="filtr-block">
						<label for="trener">Atleti trenéra:</label>
						<select id="trener" name="trener">
							<option value="">Všichni</option>
						</select>
					</div>
					
			</form>
				


			<div style="clear:both"></div>

			<div class="filtr-block filtr-filtruj">
				<button type="button" class="btn-filtr" id="filtruj-dochazka"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
			</div>

			</div>
			<!--/filtr -->
			<div style="clear:both"></div>

			<div class="dochazkaAjax">
			</div>
			
		</div>
		
	</div>
</div>

<div class="reportHeaderSection">
	<h2 toggle="ekonom">EKONOMICKÝ PROVOZ <i class="fas fa-angle-down i2"></i></h2>
</div>
<div class="reportSection" toggle="ekonom" style="display:none;">
	<div class="reportSection-ekonomika">
	<h3>Roční přehled </h3><button class="save" id="ulozitEkonomiku"><i class="fas fa-save"></i>ULOŽIT</button>
	<h4>NÁKLADY</h4>
	<div class="section1">
		<div class="addItem"></div>
		<div class="item">
			<span class="i.item"><i class="fas fa-minus-circle"></i> </span><input class="i.item"  type="text" placeholder="položka"><input placeholder="částka" type="text" class="i.item">
		</div>
		<div class="plus"><i class="fas fa-plus-circle"></i></div>
	</div>
	<h4>VÝNOSY</h4>
	<div class="section1">
	</div>
	<h4>ZISK</h4>
		
	</div>
</div>





<script>
	$(document).ready(function(){
		//zobrazovani skeci
		$('.reportHeaderSection').on('click', 'h2', function(){
			let whatToggle = $(this).attr('toggle');
			$(".reportSection[toggle='" + whatToggle  + "']").slideToggle();
		})

		//filtruj dochazku
		$('#filtruj-dochazka').click(function(){
			let dataString = $('#filtrujDochazka').serialize();
			dataString = dataString + '&sAction=reportFiltrDochazka';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				$('.dochazkaAjax').html(content);
			
				

			}).fail(function(){
				console.log('fail');
				let message = 'Uživatele se nepodařilo filtrovat. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});

		});
		
	});
</script>