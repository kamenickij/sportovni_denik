<div class="nadFiltrBtns">
	<button class="btn-2" id="toggleNewClen">NOVÝ ČLEN</button>
</div>
<div class="oddil-new-clen">
	<h2>Vložit nového člena</h2>
	<form id="newClen">
		<div class="new-clen-container">
			<div class="item">
				<label>Jméno:</label>
				<input type="text" name="sName" id="sName">
			</div>
			<div class="item">
				<label>Příjmení:</label>
				<input type="text" name="sName2" id="sName2">
			</div>
			<div class="item">
				<label>Ročník:</label>
				<input type="text" name="sRok" id="sRok">
			</div>
			<div class="item">
				<label>Poznámka:</label>
				<input type="text" name="sNote" id="sNote">
			</div>
			<div class="item">
				<button type="button" class="btn-filtr" id="ulozitClena">ULOŽIT</button>
			</div>
		</div>
	</form>
</div>

<div class="filtr">
	<form id="filtrujClen">  
		<div class="filtr-block">
			<label for="filtr_name">Jméno:</label>
			<input type="text" id="filtr_name" name="filtr_name" >
		</div>
		<div class="filtr-block">
			<label for="filtr_name2">Příjmení:</label>
			<input type="text" id="filtr_name2" name="filtr_name2" >
		</div>
		<div class="filtr-block">
			<label for="filtr_rocnik">Ročník:</label>
			<input type="text" id="filtr_rocnik" name="filtr_rocnik" >
		</div>
		<div class="filtr-block">
			<label for="filtr_trener">Trenér</label>
			<input type="checkbox" id="filtr_trener" name="filtr_trener" >
		</div>
		<div class="filtr-block">
			<label for="filtr_archive">Archivovaný</label>
			<input type="checkbox" id="filtr_archive" name="filtr_archive" >
		</div>
  </form>
	


  <div style="clear:both"></div>

  <div class="filtr-block filtr-filtruj">
    <button type="button" class="btn-filtr" id="filtruj-clenove"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
  </div>
  
  
  
</div>
<!--/filtr -->
<div style="clear:both"></div>

<div class="ajaxTable">
</div>

<script>
	$(document).ready(function(){
		//zobraz pridani noveho clena
		$('#toggleNewClen').click(function(){
			$('.oddil-new-clen').slideToggle();
		});
		//ulozit noveho clena
		$('#ulozitClena').click(function(){
			let dataString = $('#newClen').serialize();
			dataString = dataString + '&sAction=oddil_new_clen';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
						console.log('success');
			console.log(content);
						if (content == 1){
							let message = 'Uživatel byl úspěšně uložen.';
							infoSuccess(message);
						} else{
							let message = 'Uživatele se nepodařilo uložit do DB.';
							infoFail(message);
						}
				

			}).fail(function(){
						console.log('fail');
						let message = 'Uživatele se nepodařilo uložit. Chyba serveru.';
							infoFail(message);

			}).always(function(){

			});
		});
		//filtruj clenove
		$('#filtruj-clenove').click(function(){
			let dataString = $('#filtrujClen').serialize();
			dataString = dataString + '&sAction=oddil_filtr_clen';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				$('.ajaxTable').html(content);
			
				

			}).fail(function(){
				console.log('fail');
				let message = 'Uživatele se nepodařilo filtrovat. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});

		});

		
	});//ready
</script>