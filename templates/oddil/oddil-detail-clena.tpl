<form id="detailClenaForm">
<div class="oddilDetailClena">
	<div class="item">
		<label>Jméno: </label><input type="text" id="sJmeno" name="sJmeno" maxlength="50" value="{$aDetail[0]['sJmeno']}" />
	</div>
	<div class="item">
		<label>Příjmení: </label><input type="text" id="sPrijmeni" maxlength="50" name="sPrijmeni" value="{$aDetail[0]['sPrijmeni']}" />
	</div>
	<div class="item">
		<label>Poznámka: </label><input type="text" id="sText" name="sText" value="{$aDetail[0]['sText']}" />
	</div>
	<div class="item">
		<input type="hidden" id="iPK" name="iPK" value="{$aDetail[0]['iPK']}" />
		<label>Ročník: </label><input type="text" id="sRocnik" maxlength="10" name="sRocnik" value="{$aDetail[0]['sRocnik']}" />
	</div>
	<div class="item">
		<label>Vytvořeno: </label><input disabled="disabled" type="date" id="dCreate" name="dCreate" value="{$aDetail[0]['dCreate']|date_format:'%Y-%m-%d'}" />
	</div>
	<div class="item">
		<label>Poslední update: </label><input disabled="disabled" type="date" id="dUpdate" name="dUpdate" value="{$aDetail[0]['dUpdate']|date_format:'%Y-%m-%d'}" />
	</div>
	<div class="item">
		<label>Trenér: </label><input  type="checkbox" id="iTrener" name="iTrener" {if $aDetail[0]['iTrener'] == 1}checked="checked"{/if} />
	</div>
	<div class="item">
		<label>Archivovaný: </label><input type="checkbox" id="iArchive" name="iArchive" {if $aDetail[0]['iArchive'] == 1}checked="checked"{/if}/>
	</div>
</div>
</form>
<button type="button" class="btn-filtr" id="saveDetailClena">ULOŽIT</button>

<script>
	$(document).ready(function(){
		$('#saveDetailClena').click(function(){
			let dataString = $('#detailClenaForm').serialize();
			dataString = dataString + '&sAction=saveDetailClena';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
						console.log('success');

			console.log(content);
						if (content == 1 || content == '1'){
							let message = 'Stav byl úspěšně změněn.';
							infoSuccess(message);
							$('.modal-global-close').click();
							$('#filtruj-clenove').click();
						} else{
							let message = 'Chyba DB.';
							infoFail(message);
						}
				

			}).fail(function(){
						console.log('fail');
						console.log(content);
						let message = 'Chyba serveru.';
							infoFail(message);

			}).always(function(){

			});

		});
	});//ready
</script>