<span class="table1">
<table class="tableNasdilejClenaDochazka">
	<thead>
		<tr>
			<th>Příjmení</th>
			<th>Jméno</th>
			<th>Ročník</th>
            <th>Poznámka</th>
			<th>Nasdílet</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$aClenove item=clen}
    		<tr id_user="{$clen.iPK}" class="pointer">
				<td>
					{$clen.sPrijmeni}
				</td>
				<td>
					{$clen.sJmeno}
				</td>
				<td>
					{$clen.sRocnik}
				</td>
				<td>
					{$clen.sText}
				</td>
				<td>
					<input type="checkbox" class="chNasdilet checkbox2x" {if $clen.iShared == 1}
                    checked="checked" 
                    {/if} >
				</td>
			</tr>
		{/foreach}
		
	</tbody>
</table>
</span>
<script>
$(document).ready(function(){
	$('.tableNasdilejClenaDochazka').on('click', 'tr', function(){
		$(this).find('.chNasdilet').click();
	});
    $('.tableNasdilejClenaDochazka').on('click', '.chNasdilet', function(){
        let id_user = $(this).closest('tr').attr('id_user');
        let ch = $(this).is(':checked');
        let dataString = "sAction=oddil_dochazka_nasdilej_clena&checked=" + ch + "&id_user=" + id_user;
        console.log(dataString);
        $.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
						console.log('success');
			console.log(content);
						if (content == 1){
							let message = 'Stav byl úspěšně změněn.';
							infoSuccess(message);
						} else{
							let message = 'Chyba DB.';
							infoFail(message);
						}
				

			}).fail(function(){
						console.log('fail');
						let message = 'Chyba serveru.';
							infoFail(message);

			}).always(function(){

			});
    });
});
</script>