
<div class="oddil-new-clen">
	<h2>Filtrovat člena</h2>
	<form id="newClen">
		<div class="new-clen-container">
			<div class="item">
				<label>Jméno:</label>
				<input type="text" name="sName" id="sName">
			</div>
			<div class="item">
				<label>Příjmení:</label>
				<input type="text" name="sName2" id="sName2">
			</div>
			<div class="item">
				<label>Ročník:</label>
				<input type="text" name="sRok" id="sRok">
			</div>
			<div class="item">
				<label>Poznámka:</label>
				<input type="text" name="sNote" id="sNote">
			</div>
			<div class="item">
				<button type="button" class="btn-filtr" id="ulozitClena">ULOŽIT</button>
			</div>
		</div>
	</form>
</div>

<div class="filtr">
	<form id="filtrujClen">  
		<div class="filtr-block">
			<label for="filtr_name">Jméno:</label>
			<input type="text" id="filtr_name" name="filtr_name" >
		</div>
		<div class="filtr-block">
			<label for="filtr_name2">Příjmení:</label>
			<input type="text" id="filtr_name2" name="filtr_name2" >
		</div>
		<div class="filtr-block">
			<label for="filtr_rocnik">Ročník:</label>
			<input type="text" id="filtr_rocnik" name="filtr_rocnik" >
		</div>
		<div class="filtr-block">
		<label for="filtr_chPlacenoDo">Placeno do:</label>
			<input type="checkbox" id="filtr_chPlacenoDo" name="filtr_chPlacenoDo" >
			<input type="date" id="filtr_placenoDo" name="filtr_placenoDo" >
		</div>
  </form>
	


  <div style="clear:both"></div>

  <div class="filtr-block filtr-filtruj">
    <button type="button" class="btn-filtr" id="filtruj-clenove"><i class="fas fa-angle-double-down icon"></i>FILTRUJ</button>
  </div>
  
  
  
</div>
<!--/filtr -->
<div style="clear:both"></div>

<div class="ajaxTable">
</div>

<script>
	$(document).ready(function(){
		
		//filtruj clenove
		$('#filtruj-clenove').click(function(){
			let dataString = $('#filtrujClen').serialize();
			dataString = dataString + '&sAction=oddil_filtr_prispevky';
			console.log(dataString);
			$.ajax({
			type: "POST",
			url: "ajax/ajaxOddil.php",
			data: dataString
			}).done(function(content){
				console.log('success');
				console.log(content);
				$('.ajaxTable').html(content);
			
				

			}).fail(function(){
				console.log('fail');
				let message = 'Uživatele se nepodařilo filtrovat. Chyba serveru.';
					infoFail(message);

			}).always(function(){

			});

		});

		
	});//ready
</script>