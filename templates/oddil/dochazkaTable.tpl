

{if $aDochazkaHeader == null}
	<span class="dochazkaInfo">V tomto datu nemáte vyplněný trénink a nebo nemáte vyplněnou hlavičku tréninku.</span>
{/if}


<form id="dochazkaTableHeaderForm">
<div class="dochazkaTableHeader">
	<h3>Hlavička docházky za daný den</h3>

	<div class="item">
		<label for="od">Od:</label>
		<input type="time" id="od" name="od" value="{if $aDochazkaHeader != null}{$aDochazkaHeader[0]['od']|date_format:"H:m"}{else}16:00{/if}">
	</div>	
	<div class="item">	
		<label for="do">Do:</label>
		<input type="time" id="do" name="do" value="{if $aDochazkaHeader != null}{$aDochazkaHeader[0]['do']|date_format:"H:m"}{else}17:00{/if}">
	</div>
	<div class="item">
		<label for="misto">Místo:</label>
		<input type="text" id="misto" name="misto" maxlength="45" value="{$aDochazkaHeader[0]['misto']}">
	</div>
	<div class="item">
		<label for="poznamka">Poznámka:</label>
		<textarea id="poznamka" name="poznamka" rows="3" cols="30" >{$aDochazkaHeader[0]['poznamka']}</textarea>
	</div>
	<div class="item">
		<button type="button" id="ulozitTableHeader" class="btn-filtr">ULOŽIT</button>
	</div>
</div>
	</form>

{if $aDochazkaClenove == null}
	<span class="dochazkaInfo">V tomto datu nemáte vyplněný trénink a nebo jste nazaškrtnul jediného člena oddílu.</span>

{/if}	

{*<span class="table1 dochazkaTable">
<table>
	<thead>
		<tr>
			<th>Příjmení</th>
			<th>Jméno</th>
			<th>Ročník</th>
			<th>Poznámka</th>
			<th><i class="fas fa-dollar-sign"></i></th>
			<th>Přítomen <span id="checkVsichni" class="colorWhite"><i class="fas fa-check-square"></i></span><span class="colorRed" id="uncheckVsichni" ><i class="fas fa-check-square"></i></span></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$aClenove item=clen}
    		<tr iPK = {$clen.iPK} class="pointer">
				<td class="asClick">
					{$clen.sPrijmeni}
				</td>
				<td class="asClick">
					{$clen.sJmeno}
				</td>
				
				<td class="asClick">
					{$clen.sRocnik}
				</td>
				<td class="asClick">
					{$clen.sText}
				</td>
				<td class="asClick">
					{if $clen.zaplaceno == 1}<i class="fas fa-check-circle" style="color:green;"></i>{/if}
					{if $clen.zaplaceno == 0}<i class="fas fa-exclamation-circle" style="color:red;"></i>{/if}
				</td>
				<td>
					<input type="checkbox" class="chPritomen checkbox2x" {if in_array($clen.iPK, $arrIpk)} checked="checked"{/if} >
				</td>
				
			</tr>
		{/foreach}
		
	</tbody>
</table>
</span>*}
<span class="dochazkaTable">
	<div class="dochazkaContainer">
		{foreach from=$aClenove item=clen}
			<div class="divClen" iPK = {$clen.iPK} class="pointer">
				<div class="item2">
					<div class="item21">
						<input type="checkbox" class="chPritomen checkbox2x" {if in_array($clen.iPK, $arrIpk)} checked="checked"{/if} >
					</div>
				</div>
				<div class="item1">
					<div class="item1Up">
						<div class="item11 bold">{$clen.sPrijmeni}</div>
						<div class="item11 bold">{$clen.sJmeno}</div>
						<div class="item11 bold">{$clen.sRocnik}</div>
					</div>
					<div class="item1Down">
						<div class="item11">Zaplaceno:</div>
						<div class="item11">{if $clen.zaplaceno == 1}<i class="fas fa-check-circle" style="color:green;"></i> ANO{/if}
							{if $clen.zaplaceno == 0}<i class="fas fa-exclamation-circle" style="color:red;"></i>{/if} NE</div>
						<div class="item11">Pozn.: {$clen.sText}</div>

					</div>




				</div>


			</div>
		{/foreach}
	</div>
</span>
<script>
	$(document).ready(function(){
		
		
		
	});//ready
</script>
