<script>
  $(function () {
    // select2
    $('.select-2').select2();

    //pridani radku
    {* $(".day-row-2").on("click", ".plus-ots", function () {
      var replace =
        " <div class='train-item'><i style='color:red;font-size:20px;cursor:pointer;margin-right:5px;' class='fa fa-minus-square minus-ots' aria-hidden='true'></i><input type='text' id='sActivity' name='sActivity' placeholder='Zadejte aktivitu'><input style='margin-right:5px;margin-left:5px;' class='input-opakovani' type='text' placeholder='opakování' /><span style='margin-right:5px;' class='nasobeni'>x</span><input style='margin-right:5px;' class='input-vzdalenost' type='text' placeholder='vzdálenost' /></div><div class='train-item pridat'></div>";



      $(this).closest(".train-phase").find(".pridat").replaceWith(replace);      
        $('.select-2').select2();      
    }); *}

     

    

    var aktualniTyden = $("#selectedTyden").val();
    $("#tydenPlus").on("click", function(){
      aktualniTyden ++;
      if (aktualniTyden == 53){
        alert("Maximální týden je 52");
        aktualniTyden --;
      }
      $('#selectedTyden').text(aktualniTyden);
      $('#selectedTyden').val(aktualniTyden);
      $('#filtruj-a-report').click();
      return false;
    });
    
    $("#tydenMinus").click(function(){
      aktualniTyden --;
      if (aktualniTyden == 0){
        alert("Hodnota týden nemůže být 0");
        aktualniTyden ++;
      }
      $('#selectedTyden').text(aktualniTyden);
      $('#selectedTyden').val(aktualniTyden);
      $('#filtruj-a-report').click();
      return false;
    });
    
    
    $("#filtruj-a-report").click(function () {
      var ajaxModul = $(this).attr("id");
      //var id_user = $('#filtr-uzivatel-select').val();
      var selectedTyden = $('#selectedTyden').val();
      aktualniTyden = selectedTyden;
      $('#tyden-form').val(selectedTyden).trigger('change');
      var iStav = $('#filtr-stav-select').val();
   //   alert(ajaxModul + id_user + selectedTyden);
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=" + ajaxModul + "&tyden=" + selectedTyden + "&iStav=" + iStav,
        success: function (content) {         
        if (content) {
            $(".day-row-container").html(content);
          }
        }
      });
      return false;
    });
    $('#tyden-form').on('change', function(){
      var selectedTyden = $('#tyden-form option:selected').val();
      //$('#selectedTyden').text(selectedTyden);
      $('#selectedTyden').val(selectedTyden);
      $('#selectedTyden').html(selectedTyden);
      aktualniTyden = selectedTyden;
      return false;

    });




  }); //function
</script>
<!-- main2.tpl -->
<div class="filtr">
  <div class="filtr-block">
    <label for="tyden-from">Tyden</label>
    <select name="tyden-form" id="tyden-form" class="select-2" style="width:60px;">
      {for $foo=1 to 52}
      <option value="{$foo}" {if $aktualniTyden == $foo}selected{/if}>{$foo}</option>
      {/for}
    </select>
  </div>

  <div class="filtr-block">
    <label for="filtr-stav-select">Stav</label>
    <select id="filtr-stav-select">
        <option value="0">
         Vše
      </option>
        <option value="1">
         Nepotvrzeno
      </option>
      <option value="2">
         Potvrzeno
      </option>
    </select>
  </div>

  <div style="clear:both"></div>

  <div class="filtr-block filtr-filtruj">
    <button class="btn-filtr" id="filtruj-a-report">FILTRUJ</button>
  </div>



</div>
<!--/filtr -->
<div style="clear:both"></div>

<button id="tydenMinus" class="btn-filtr">
  <i class="fa fa-arrow-left" aria-hidden="true"></i>
</button>
<button class="btn-filtr" id="selectedTyden" value="{$aktualniTyden|intval}">{$aktualniTyden|intval}</button>
<button id="tydenPlus" class="btn-filtr">
  <i class="fa fa-arrow-right" aria-hidden="true"></i>
</button>

<div>
    Jednotky zatížení: <span id="jednotkyZatizeni"></span>
</div>

<div style="clear:both"></div>


<div class="day-row-container">
  
</div>