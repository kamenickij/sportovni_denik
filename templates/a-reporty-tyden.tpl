<script>
  $(function () {

      $(".day-row-2").on("click", ".plus-ots", function () {
          var idRadku = $(this).closest('.train-item').find('.idRadku').val();

          idRadku = parseInt(idRadku) + 1;
          var replace = " <div class='train-item'><input type='hidden' class='iPK' value='0'><input type='hidden' class='idRadku' value=" + idRadku + " ><i style='color:red;font-size:20px;cursor:pointer;margin-right:5px;' class='fa fa-minus-square minus-ots' aria-hidden='true'></i><select style='margin-right:5px;' class='input-ots-id select-2'><option value='0'>Vyberte OTS/STS</option>"; 
            {foreach from = $ots key = k item = i}
              replace = replace + '<option sMj="{$i.sMj}" value="{$i.iPK}">{$i.sName}</option>'; 
            {/foreach}
            replace = replace + "</select><input type='text' class='sActivity' placeholder='Zadejte aktivitu'><input style='margin-right:5px;margin-left:5px;' class='input-opakovani'  type='text' placeholder='opakování' /><span style='margin-right:5px;' class='nasobeni'>x</span><input style='margin-right:5px;' class='input-vzdalenost' type='text' placeholder='vzdálenost' /><p class='opakovaniXvzdalenost'>= 0</p><p class='mj'></p><p class='p_prepocet'> </p></div><div class='pridat'></div>";

            var selectOption = $(this).closest(".train-phase").find(".pridat").replaceWith(replace);
            $('.select-2').select2();
            $('.sActivity').autocomplete({

              source: function (request, response) {
                $.ajax({
                  url: 'include/autocomplete.php?sAction=sAktivita&string=' + request.term,
                  dataType: "json",
                  beforeSend: function () {

                  },
                  data: {
                    term: request.term
                  },
                  success: function (data) {
                    response(data);
                  }
                });
              },
              minLength: 1,
              autoFocus: true,
              select: function (event, ui) {
                $(this).val(ui.item.value);
              }
            });
            //  });
          });




        $('.sFazeMisto').autocomplete({

          source: function (request, response) {
            $.ajax({
              url: 'include/autocomplete.php?sAction=sFazeMisto&string=' + request.term,
              dataType: "json",
              beforeSend: function () {

              },
              data: {
                term: request.term
              },
              success: function (data) {
                response(data);
              }
            });
          },
          minLength: 1,
          autoFocus: true,
          select: function (event, ui) {
            $(this).val(ui.item.value);
          }
        });

        $('.sActivity').autocomplete({

          source: function (request, response) {
            $.ajax({
              url: 'include/autocomplete.php?sAction=sAktivita&string=' + request.term,
              dataType: "json",
              beforeSend: function () {

              },
              data: {
                term: request.term
              },
              success: function (data) {
                response(data);
              }
            });
          },
          minLength: 1,
          autoFocus: true,
          select: function (event, ui) {
            $(this).val(ui.item.value);
          }
        });


        $(".day-row-2").on("click", ".minus-ots", function () {
          $(this).closest(".train-item").remove();
        });

        $(".day-row").on("click", ".zamitnoutPolozku", function () {
          var iPK = $(this).closest('.train-item').find('.iPK').val();

          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data: "sAction=zamitnoutPolozku&iPK=" + iPK,
            success: function (content) {
              if (content) {
                $('.modal-global-info-content').html('Stav aktivity je uložen.');
                infoSuccess();
                $("#filtruj-a-report").click();
              }
            }
          });
          return false;
        });

        $(".day-row").on("click", ".confirmPolozku", function () {
          var iPK = $(this).closest('.train-item').find('.iPK').val();
          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data: "sAction=confirmPolozku&iPK=" + iPK,
            success: function (content) {
              if (content) {
                $('.modal-global-info-content').html('Stav aktivity je uložen.');
                infoSuccess();
                $("#filtruj-a-report").click();
              }
            }
          });
          return false;
        });

        $(".day-row").on("click", ".ulozDataAtleta", function () {
          var datum = $(this).closest('.day-row').parent().find('.date').text();
          var poznamka = $(this).closest('.day-row').find('.poznamkaAtleta').val();
          var iNemoc = $(this).closest('.day-row').find('.iNemoc').val();
          var sNemoc = $(this).closest('.day-row').find('.inputnemoc').val();
          var iZavod = $(this).closest('.day-row').find('.iZavod').val();
          var sZavod = $(this).closest('.day-row').find('.pocetstart').val();
          console.log(sZavod);
          var casZat = $(this).closest('.day-row').find('.casZat').val();
          var casReg = $(this).closest('.day-row').find('.reg').val();
          var apa_ipk = $(this).closest('.day-row').find('.apa_ipk').val();
          var idTrener = $('.day-row-container').find('.idTrener').val();
          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data: "sAction=dataDenAtlet&poznamka=" + poznamka + "&den=" + datum + "&idTrener=" + idTrener + "&iNemoc=" + iNemoc + "&sNemoc=" + sNemoc + "&iZavod=" + iZavod + "&sZavod=" + sZavod + "&casZat=" + casZat + "&casReg=" + casReg + "&apa_ipk=" + apa_ipk,
            success: function (content) {
              if (content) {
                console.log(content);
                $('.modal-global-info-content').html('Poznámka je uložena.');
                infoSuccess();
                $("#filtruj-a-report").click();
              }
            }
          });
          return false;
        });


        //klikni na nemoc
        $(".btn-nemoc-btn").click(function () {
          $(this).closest('.day-row').find(".btn-nemoc").toggle();
          if ($(this).closest('.day-row').find('.btn-zavod').is(':visible')) {
            $(this).closest('.day-row').find('.btn-zavod').hide();
          }
          if ($(this).closest('.day-row').find('.btn-nemoc').is(":visible")) {
            $(this).closest('.day-row').find('.iNemoc').val(1);
          } else {
            $(this).closest('.day-row').find('.iNemoc').val(0);
          }
          var checkZavod = $(this).closest('.day-row').find('.iZavod').val();
          if (checkZavod == "1") {
            $(this).closest('.day-row').find('.iZavod').val(0);
          } else {
            $(this).closest('.day-row').find(".btn-ctrl").toggle();

          }
        });
        //klikni na zavod
        $(".btn-zavod-btn").click(function () {
          $(this).closest('.day-row').find(".btn-zavod").toggle();
          if ($(this).closest('.day-row').find('.btn-nemoc').is(':visible')) {
            $(this).closest('.day-row').find('.btn-nemoc').hide();
          }
          if ($(this).closest('.day-row').find('.btn-zavod').is(":visible")) {
            $(this).closest('.day-row').find('.iZavod').val(1);
          } else {
            $(this).closest('.day-row').find('.iZavod').val(0);
          }
          var checkNemoc = $(this).closest('.day-row').find('.iNemoc').val();
          if (checkNemoc == "1") {
            $(this).closest('.day-row').find('.iNemoc').val(0);
          } else {
            $(this).closest('.day-row').find(".btn-ctrl").toggle();

          }
        });

        //prepocet input opakovani a vzdalenost
        $('.include-container').on('change', '.input-opakovani', function () {
          var opakovani = $(this).val();
          opakovani = parseFloat(opakovani);
          var vzdalenost = $(this).closest('.train-tem').find('.input-vzdalenost').val();
          vzdalenost = parseFloat(vzdalenost);
          var sum = opakovani * vzdalenost;
          if (isNaN(sum)) {
            sum = 0;
          }
          $(this).closest('div').find('.opakovaniXvzdalenost').text('= ' + sum);
        });
        //prepocet input opakovani a vzdalenost
        $('.include-container').on('change', '.input-vzdalenost', function () {
          var vzdalenost = $(this).val();
          vzdalenost = parseFloat(vzdalenost);
          var opakovani = $(this).closest('.train-tem').find('.input-opakovani').val();
          opakovani = parseFloat(opakovani);
          var sum = opakovani * vzdalenost;
          if (isNaN(sum)) {
            sum = 0;
          }
          $(this).closest('div').find('.opakovaniXvzdalenost').text('= ' + sum);
        });

        //do jine faze klik
        $('.day-row').on('click', '.jinaFaze', function () {
          let iPK = $(this).closest('.train-item').find('.iPK').val();
          let doFaze = $(this).attr('doFaze');
          let el = this;
          let fazeNo = '';
          if (doFaze == '4') {
            fazeNo = '1';
          } else if (doFaze == '5') {
            fazeNo = '2';
          } else if (doFaze == '6') {
            fazeNo = '3';
          }
          fazeNo = "<span class='presunutoDoFaze'>" + fazeNo + "</span>";
          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data: "sAction=doJineFaze&iPK=" + iPK + "&doFaze=" + doFaze
          }).done(function (content) {
            console.log(content);
            $('.modal-global-info-content').html('STU bylo přeneseno do jiné fáze.');
            infoSuccess();
            $(el).closest('.train-item').find('.presunutoDoFaze').remove();
            $(el).closest('.train-item').find('.idRadku').after(fazeNo);
            $('#filtruj-a-report').click();

          }).fail(function () {

          }).always(function () {

          });

        }); //do jine faze klik

        $('#jednotkyZatizeni').html({$tydenRozpis['jednotkyZatizeni']});
      }); //function
</script>
<input type="hidden" id="idTrener" value="{$idTrener}">
<div class="day-row">
    
    <div class="day-row-1">
      <h3 class="den">Pondělí</h3>
      <p class="date">{$tyden[1]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 1" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 1" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

        
      </div>
    </div>
      
      <div class="include-container">
        <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[1][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
            <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[1][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
        <span class="btn-ctrl">
          {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[1] pozn=$poznamka[1] denTyden=1}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Úterý</h3>
      <p class="date">{$tyden[2]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 2" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 2" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

         
      </div>
    </div>
      
      <div class="include-container">
      <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[2][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
          <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[2][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
      <span class="btn-ctrl">
       {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[2] pozn=$poznamka[2] denTyden=2}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Středa</h3>
      <p class="date">{$tyden[3]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 3" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 3" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

       
      </div>
    </div>
      
      <div class="include-container">
      <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[3][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
          <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[3][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
      <span class="btn-ctrl">
       {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[3] pozn=$poznamka[3] denTyden=3}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Čtvrtek</h3>
      <p class="date">{$tyden[4]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 4" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 4" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

       
      </div>
    </div>
      
      <div class="include-container">
      <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[4][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
          <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[4][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
      <span class="btn-ctrl">
       {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[4] pozn=$poznamka[4] denTyden=4}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Pátek</h3>
      <p class="date">{$tyden[5]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 5" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 5" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

       
      </div>
    </div>
      
      <div class="include-container">
      <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[5][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
          <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[5][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
      <span class="btn-ctrl">
       {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[5]  pozn=$poznamka[5] denTyden=5}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Sobota</h3>
      <p class="date">{$tyden[6]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 6" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 6" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

      
      </div>
    </div>
      
      <div class="include-container">
      <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[6][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
            <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[6][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
      <span class="btn-ctrl">
       {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[6]  pozn=$poznamka[6] denTyden=6}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Neděle</h3>
      <p class="date">{$tyden[7]}</p>
      <button type="button" class="btn1 btn-nemoc-btn 7" title="Nemoc"><i class="fas fa-medkit"></i></button>
      <button type="button" class="btn1 btn-zavod-btn 7" title="Závod"><i class="fas fa-hand-peace"></i></button>
      <div class="report-stav">

      
      </div>
    </div>
      
      <div class="include-container">
      <div class="btn-nemoc">
          <h3>Den zdravotního omezení.</h3>
          <input class="inputNemoc" type="text" placeholder="Vyplňte nemoc" value="{$tydenRozpis[7][1]['sNemoc']}" style="width: 300px" >
          <div style="height:60px;"></div>
        </div>
        <div class="btn-zavod">
          <h3>Závod.</h3><h3> Vyplňte počet startů:</h3><input class="pocetstart" type="text" placeholder="Vyplňte počet startů" value="{$tydenRozpis[7][1]['sZavod']}" style="width:300px">
          <div style="height:60px;"></div>
        </div>
      <span class="btn-ctrl">
       {include file='../templates/a-reporty-item.tpl' tydenData=$tydenRozpis[7]  pozn=$poznamka[7] denTyden=7}
       </span>
       <button class="btn-filtr ulozDataAtleta">Uložit</button>
       </div>
  </div>