<script>
   $(function () { 

     

     $(".day-row-2").on("click", ".plus-ots", function () {
       var idRadku = $(this).prev().prev().find('.idRadku').val();
    
        if (isNaN(idRadku)) idRadku = 0;
    
       idRadku = parseInt(idRadku) + 1; 
      var replace = " <div class='train-item'><div class='trainItem'><input type='hidden' class='iPK' value='0'><input type='hidden' class='idRadku' value="+idRadku+" ></div><div class='trainItem'><i  class='fa fa-minus-square minus-ots' aria-hidden='true'></i></div><div class='trainItem' style='padding-top:0px;'><select style='margin-right:15px; width: 250px;' class='input-ots-id select-2'><option value='0'>Vyberte STU</option>";
        {foreach from=$ots key=k item=i}
            replace = replace +'<option sMj="{$i.sMj}" value="{$i.iPK}">{$i.sName}</option>';
        {/foreach}
        replace = replace + "</select></div><div class='trainItem'><input type='text' class='sActivity' placeholder='Zadejte aktivitu'><input style='margin-right:5px;margin-left:5px;' class='input-opakovani' onchange='prepocitejVzOpak(this);'   type='text' placeholder='opakování' /></div><div class='trainItem'><span style='margin-right:5px;' class='nasobeni'>x</span></div><div class='trainItem'><input style='margin-right:5px;' class='input-vzdalenost' onchange='prepocitejVzOpak(this);' type='text' placeholder='vzdálenost' /></div><div class='trainItem'><p class='opakovaniXvzdalenost'>= 0</p></div><div class='trainItem'><p class='mj'></p></div><div class='trainItem'><p class='p_prepocet'> |RP: <span class='rp_prepocet'></span></p></div></div><div class='pridat'></div>";
      
      var selectOption = 
      

      $(this).closest(".train-phase").find(".pridat").replaceWith(replace);
    //  $(this).closest(".train-phase").find(".select-2").each(function(){
        $('.select-2').select2();
        $('.sActivity').autocomplete({
       
            source: function (request, response) {
              $.ajax({
                  url: 'include/autocomplete.php?sAction=sAktivita&string='+request.term,
                  dataType: "json",
                  beforeSend: function() {

                  },
                  data: {
                      term: request.term
                  },
                  success: function (data) {
                      response(data);
                  }
              });
            },
            minLength: 1,
            autoFocus: true,
            select: function (event,ui) {
                $(this).val(ui.item.value);
            }
          });
    //  });
    });
    
    $(".day-row-2").on("click", ".minus-ots", function () {
      $(this).closest(".train-item").remove();
    });
    
    
    // ZPET
    $("#zpet").click(function(){
    $("#denik").click();
    return false;
  });

$('.sFazeMisto').autocomplete({
       
      source: function (request, response) {
        $.ajax({
            url: 'include/autocomplete.php?sAction=sFazeMisto&string='+request.term,
            dataType: "json",
            beforeSend: function() {
                
            },
            data: {
                term: request.term
            },
            success: function (data) {
                response(data);
            }
        });
      },
      minLength: 1,
      autoFocus: true,
      select: function (event,ui) {
          $(this).val(ui.item.value);
      }
    });
    
    $('.sActivity').autocomplete({
       
      source: function (request, response) {
        $.ajax({
            url: 'include/autocomplete.php?sAction=sAktivita&string='+request.term,
            dataType: "json",
            beforeSend: function() {
                
            },
            data: {
                term: request.term
            },
            success: function (data) {
                response(data);
            }
        });
      },
      minLength: 1,
      autoFocus: true,
      select: function (event,ui) {
          $(this).val(ui.item.value);
      }
    });
     //rozdil
        var rp =0;
        var ap = 0;
        var rozdil =0;
        $('.tydenni-predpis-porovnani').find('tr').each(function(){
          rp = $(this).find('.rp').text();
          ap = $(this).find('.ap').text();
          rozdil = rp - ap;
          $(this).find('.rozdil').text(rozdil);

        });
      // prepocitat
        $('#prepocitat').on('click',function(){
          var sAction ='prepocitat-tydenni-predpis';
          var id_user = $('#filtr-uzivatel-select').val();
            var selectedTyden = $('#selectedTyden').val();
            
           OTU = { };
          var firstWay;
          $('div[class="day-row"]').each(function(){
            $(this).find('.train-item').each(function(){
              
                var iOTS = $(this).find('.input-ots-id').val();      
              var inputOpakovani = $(this).find('.input-opakovani').val();
              var inputVzdalenost = $(this).find('.input-vzdalenost').val();
                if (iOTS > 0) {
                    if (typeof(OTU[iOTS]) !== 'object') {    
                       OTU[iOTS] = { };  
                    }
                    if (isNaN(OTU[iOTS]['soucet'])) {
                        OTU[iOTS]['soucet'] = 0;
                    }
                    OTU[iOTS]['soucet'] += (parseFloat(inputOpakovani) * parseFloat(inputVzdalenost));
                    firstWay = 0;
                }
            });
          });
          
          $.ajax({
            type: "POST",
            url: "ajax/ajaxJSONporovnani.php",
            data       : { 'data':JSON.stringify(OTU), 'id_user': JSON.stringify(id_user), 'selectedTyden':JSON.stringify(selectedTyden) },
            success: function (content) {
              if (content) {
               $('.tydenni-predpis-porovnani').html(content);
                
              }
            }
          });

          $('.tydenni-predpis-porovnani').find('tr').each(function(){
            rp = $(this).find('.rp').text();
            ap = $(this).find('.ap').text();
            rozdil = rp - ap;
            $(this).find('.rozdil').text(rozdil);

           });
          
          return false;
        });
        //vlozit stu externe
        $('.vlozitFazeStu').click(function(){
          let faze = $(this).closest('.phase').attr('faze');
          let den = $(this).closest('.day-row').attr('den');
          $('#extStuDen').val(den);
          $('#extStuDenFaze').val(faze);
          let dataString = 'sAction=vlozitExterneStu' + '&den=' + den + '&faze=' + faze;

          console.log(dataString);
          $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data:     dataString,
            success: function (content) {
              if (content) {
                $('.modal-global-content').html(content);
                $('.modal-global').show();
                
              }
            }
          });
        });
       

  });//ready
    </script>

    {* inputy pro externi vkladani stu *}
    <input type="hidden" id="extStuDen">
    <input type="hidden" id="extStuDenFaze">
    <div class="tydenni-predpis-porovnani">
    {include file='../templates/porovnani.tpl'} 
    </div>   
    <div style="clear:both"></div>
<div class="day-row" den="1">
    <div class="day-row-1" >
      <h3 class="den">Pondělí</h3>
      <p class="date">{$tyden[1]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[1]}
       </div>
  </div>
  <div class="day-row" den="2">
    <div class="day-row-1">
      <h3 class="den">Úterý</h3>
      <p class="date">{$tyden[2]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[2]}
       </div>
  </div>
  <div class="day-row" den="3">
    <div class="day-row-1">
      <h3 class="den">Středa</h3>
      <p class="date">{$tyden[3]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[3]}
       </div>
  </div>
  <div class="day-row" den="4">
    <div class="day-row-1">
      <h3 class="den">Čtvrtek</h3>
      <p class="date">{$tyden[4]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[4]}
       </div>
  </div>
  <div class="day-row" den="5">
    <div class="day-row-1">
      <h3 class="den">Pátek</h3>
      <p class="date">{$tyden[5]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[5]}
       </div>
  </div>
  <div class="day-row" den="6">
    <div class="day-row-1">
      <h3 class="den">Sobota</h3>
      <p class="date">{$tyden[6]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[6]}
       </div>
  </div>
  <div class="day-row" den="7">
    <div class="day-row-1">
      <h3 class="den">Neděle</h3>
      <p class="date">{$tyden[7]}</p>
    </div>
      
      <div class="include-container">
       {include file='../templates/tydenni-predpis-item.tpl' tydenData=$tydenRozpis[7]}
       </div>
  </div>