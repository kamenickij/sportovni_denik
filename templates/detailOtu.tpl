<form id="editOtuForm">
<div class="detailOtu">
	<div>
		<label for="sipk">iPK</label>
		<input type="text" id="sipk" name="sipk" value="{$otuSpec['iPK']}" disabled="disabled">
	</div>	
	<div>
		<label for="sname">Název</label>
		<input type="text" id="sname" name="sname" value="{$otuSpec['sName']}">
	</div>	
	<div>
		<label for="smj">MJ</label>
		<input type="text" id="smj" name="smj" value="{$otuSpec['sMj']}">
	</div>
	<div>
		<label for="kod">Kod</label>
		<input type="text" id="kod" name="kod" value="{$otuSpec['kod']}">
	</div>	
</div>
</form>
<button id="ulozitEditOtu" class="btn-filtr">ULOŽIT</button>
<script type="text/javascript">
	$(function() {
		$('#ulozitEditOtu').click(function(){
			var ipk = $('#sipk').val();
			var dataString = $('#editOtuForm').serialize();
			dataString = dataString + '&sAction=editOtu&sipk=' + ipk;
			console.log(dataString);
			 $.ajax({
	           type: "POST",
	           url: "ajax/ajax.php",
	           data: dataString,
	           success: function(content){
	           		console.log(content);
					   var message = "Uloženo!";
	               infoSuccess(message);
	               $('.modal-global').hide();
	               $('#hledatOtu').click();

	           	}
			});//ajax
		});//click
    	
	});//function 
</script>


