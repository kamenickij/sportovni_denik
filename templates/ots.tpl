
<script>
    $(function() {

      $('#zpet').click(function() {
        $.ajax({
   type: "POST",
   url: "ajax/ajax.php",
   data: "sAction=rocni-predpis",
   success: function(content){
    $("#page").html(content);
     }
   });
    return false;
      });
       

});

</script>



  <div class="container-global">
    <div id="page">
<h2 class="sekce">Deník / Seznam OTU a STU</h2>
      <div id="denik-menu-btns">
      <button id="zpet" class="btn-sub-menu"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
      </div>
      <h3>Výpis OTU / STU</h3>
      <div class="filtr">
        <div class="filtr-block">
          <label for="nazev">Název</label>
          <input type="text" name="nazev" id="nazev" max="" value="{$nazevTabulky}" style="width: 130px;">

        </div>

        <div class="filtr-block">
          <label for="filtr-uzivatel-select"> Uživatel</label>
          <select id="filtr-uzivatel-select" class="select-2" style="width: 250px;">
              <option value="0" {if $atlet == 0} selected {/if}>Všichni uživatelé</option>
          {foreach from=$selectUzivatelu item=i}
            <option value="{$i.id_user}" {if $atlet == $i.id_user} selected {/if}>{$i.id_user}-{$i.jmeno} {$i.prijmeni}-{$i.email}</option>
          {/foreach}
          </select>
        </div>


        <div style="clear:both"></div>

        <div class="filtr-block filtr-filtruj">
          <button id="filtruj" class="btn-filtr"><i class="fas fa-angle-double-down icon" style="margin-right: 5px;"></i>FILTRUJ</button>
        </div>
      </div> <!-- /filtr -->
      <div style="clear:both"></div>
        
        <div class="OTU">
          <span class="table1">
          <table class="">
              <thead>
                <tr>
                  <th colspan="">
                    ID
                  </th>
                  <th colspan="">
                    Název
                  </th>
                  <th colspan="">
                    MJ
                  </th>
                </tr>
              </thead>
              <tbody>
                {foreach from=$rozpisOts key=k item=i}
        
        
                <tr class="rt-tr-input">
                  <td>
                      {$i.iPK}
                </td>
                <td colspan="">
                    {$i.sName}
                </td>
                <td colspan="">
                  {$i.sMj}
                </td>
        
                </tr>
                {/foreach}
                </tbody>
        
            </table>
            </span>
          </div>
    
  </div>
  </div>      
    

