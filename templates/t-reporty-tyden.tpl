<script>
   $(function () { 

     $(".day-row-2").on("click", ".plus-ots", function () {
       var idRadku = $(this).prev().prev().find('.idRadku').val();
    
       idRadku = parseInt(idRadku) + 1; 
      var replace = " <div class='train-item'><input type='hidden' class='iPK' value='0'><input type='hidden' class='idRadku' value="+idRadku+" ><i style='color:red;font-size:20px;cursor:pointer;margin-right:5px;' class='fa fa-minus-square minus-ots' aria-hidden='true'></i><select style='margin-right:5px;' class='input-ots-id select-2'><option value='0'>Vyberte OTS/STS</option>";
        {foreach from=$ots key=k item=i}
            replace = replace +'<option value="{$i.iPK}">{$i.sName}</option>';
        {/foreach}
        replace = replace + "</select><input type='text' class='sActivity' placeholder='Zadejte aktivitu'><input style='margin-right:5px;margin-left:5px;' class='input-opakovani' type='text' placeholder='opakování' /><span style='margin-right:5px;' class='nasobeni'>x</span><input style='margin-right:5px;' class='input-vzdalenost' type='text' placeholder='vzdálenost' /><p class='mj'>m</p></div><div class='pridat'></div>";
      
      

      $(this).closest(".train-phase").find(".pridat").replaceWith(replace);
    //  $(this).closest(".train-phase").find(".select-2").each(function(){
        $('.select-2').select2();
        $('.sActivity').autocomplete({
       
            source: function (request, response) {
              $.ajax({
                  url: 'include/autocomplete.php?sAction=sAktivita&string='+request.term,
                  dataType: "json",
                  beforeSend: function() {

                  },
                  data: {
                      term: request.term
                  },
                  success: function (data) {
                      response(data);
                  }
              });
            },
            minLength: 1,
            autoFocus: true,
            select: function (event,ui) {
                $(this).val(ui.item.value);
            }
          });
    //  });
    });
    
    $(".day-row-2").on("click", ".minus-ots", function () {
      $(this).closest(".train-item").remove();
    });
    
    
    // ZPET
    $("#zpet").click(function(){
    $("#denik").click();
    return false;
  });

$('.sFazeMisto').autocomplete({
       
      source: function (request, response) {
        $.ajax({
            url: 'include/autocomplete.php?sAction=sFazeMisto&string='+request.term,
            dataType: "json",
            beforeSend: function() {
                
            },
            data: {
                term: request.term
            },
            success: function (data) {
                response(data);
            }
        });
      },
      minLength: 1,
      autoFocus: true,
      select: function (event,ui) {
          $(this).val(ui.item.value);
      }
    });
    
    $('.sActivity').autocomplete({
       
      source: function (request, response) {
        $.ajax({
            url: 'include/autocomplete.php?sAction=sAktivita&string='+request.term,
            dataType: "json",
            beforeSend: function() {
                
            },
            data: {
                term: request.term
            },
            success: function (data) {
                response(data);
            }
        });
      },
      minLength: 1,
      autoFocus: true,
      select: function (event,ui) {
          $(this).val(ui.item.value);
      }
    });
    
        
    //ulozPoznamkuAtleta
  });
    </script>
<h3>OTU v týdnu:</h3>
<div class="otuTreport">
  <div class="item">
    <span class="item_nazev">Čas zatížení:</span>
    <span class="item_hodnota">{$tydenRozpis[1][1]['casZatizeni']}</span>
  </div>
  <div class="item">
    <span class="item_nazev">Čas regenerace:</span>
    <span class="item_hodnota">{$tydenRozpis[1][1]['casRegenerace']}</span>
  </div>
  <div class="item">
    <span class="item_nazev">Jednotky zatížení:</span>
    <span class="item_hodnota">{$tydenRozpis['jednotkyZatizeni']}</span>
  </div>
  <div class="item">
    <span class="item_nazev">Dny zatížení:</span>
    <span class="item_hodnota">{$tydenRozpis['dnyZatizeni']}</span>
  </div>
</div>    
<input type="hidden" id="idTrener" value="{$idTrener}">
<div class="day-row">
    
    <div class="day-row-1">
      <h3 class="den">Pondělí</h3>
      <p class="date">{$tyden[1]}</p>
      <div class="report-stav">
       
      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[1] pozn=$poznamka[1]}
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Úterý</h3>
      <p class="date">{$tyden[2]}</p>
      <div class="report-stav">

          
      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[2] pozn=$poznamka[2]}
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Středa</h3>
      <p class="date">{$tyden[3]}</p>
      <div class="report-stav">

        
      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[3] pozn=$poznamka[3]}
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Čtvrtek</h3>
      <p class="date">{$tyden[4]}</p>
      <div class="report-stav">

        
      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[4] pozn=$poznamka[4]}
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Pátek</h3>
      <p class="date">{$tyden[5]}</p>
      <div class="report-stav">

       
      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[5]  pozn=$poznamka[5]}
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Sobota</h3>
      <p class="date">{$tyden[6]}</p>
      <div class="report-stav">

      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[6]  pozn=$poznamka[6]}
       </div>
  </div>
  <div class="day-row">
    <div class="day-row-1">
      <h3 class="den">Neděle</h3>
      <p class="date">{$tyden[7]}</p>
      <div class="report-stav">

      </div>
    </div>
      
      <div class="include-container">
       {include file='../templates/t-reporty-item.tpl' tydenData=$tydenRozpis[7]  pozn=$poznamka[7]}
       </div>
  </div>