
<script>
    $(function() {
        //oznaceni tr bg svetle mddrou
        $(".rt-modal-table").on("click", "tbody tr", function () {
          $('.rt-modal-table').find('.bgsvmodra').removeClass('bgsvmodra');
          $(this).addClass('bgsvmodra');
        });

       $('#submit').click(function(){

        var data = $('#rocni_predpis_form').serialize();
        var test = $('#nazevTable').val();
        if (test == ""){
          alert("Je nezbytné doplnit název tabulky.");
          return;

        } else {          
       $.ajax({
            type: "POST",
            url: "ajax/ajax.php",
            data: "sAction=rocni_rozpis_update_save&"+data,
            success: function(content){
              if(content){
                //$(".modal-content-action").html(content); 
                var message = "Tabulka byla v pořádku uložena."; 
                infoSuccess(message);              
              } else {
                var message = "Tabulka byla v pořádku uložena."; 
                infoFail(message); 
              }
            }
        });
        return false;
        }
    });
    // rocni tabulka odebrani radku
    $(".rt-modal-table").on("click", ".fa-minus-square", function () {
      $(this).closest('tr').remove();
      return false;
    });
    //rocni tabulka pridani dalsiho radku
    $("#plus").click(function(){
      $.ajax({
           type: "POST",
           url: "ajax/ajax.php",
           data: "sAction=plus",
           success: function(content){
             if(content){
               $( "#pridatTr" ).replaceWith( content );
               $("table").find(".select-2").each(function () {
                  $(".select-2").select2();
                });
             }
           }
       });

      return false;
    });
    $(".close").click(function(){
        $('#filtruj').click();
      $(".modal").css( "display", "none" );
    });

     //select on change
    $('form').on('change', '.aktivita', function () {
      var aktivitaId = $(this).val();
      var check = 0;
      //check jestli uz tuto aktivitu nema vybranou
      $(this).closest('table').find('.checkInput').each(function(){
        if (aktivitaId == $(this).attr('iPKots')){
          alert('Tuto aktivitu máze již v roční tabulce vybranou. Vyberte prosím jinou.');
          check = 1;
          return false;

        }
      });
        if (check == 1){
          $(this).val("0").trigger("change");
          return false;
        }

      $(this).closest('tr').find('input').each(function () {
        //console.log($(this).attr('id'));
        var id = $(this).attr('id');
        if (id.indexOf('_') > 0) {
          id = id.substring(0, id.indexOf('_'));
        }
        $(this).removeAttr('id');
        $(this).removeAttr('name');
        $(this).attr('id', id + '_' + aktivitaId);
        $(this).attr('name', id + '_' + aktivitaId);


        // "this" is the current element in the loop
      });
      //$('div').each(function (index, value) {
      //  console.log('div' + index + ':' + $(this).attr('id'));
      //});
    });

    $('#spec').change(function(){
        var spec_iPK = $(this).val();
        $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=get-spec-otu&spec_iPK="+spec_iPK,
        success: function (content) {
          if (content) {
           //$("#page").html(content); 
           setOTUfromSpec(content);
          }
        }
      });
       
        
        return false;
    });


     // select2
    $(".rt-modal-table").find(".select-2").each(function () {
                  $(this).select2();
    });
    




});//ready

function setOTUfromSpec(otu) {
   var otuArray = otu.split(',');
    otuArray.forEach(function(otu_iPK) {
        console.log(otu_iPK);
    });
}    

</script>

<span class="close modal-rt-close btn-sub-menu"><i class="fas fa-times"></i></span>

<div class="rt-modal-popis">

</div>
<!-- modal-rt.tpl-->
<div class="rt-modal-table">
    <div class="modal-content-action"></div>
    <form method="post" id="rocni_predpis_form" action="" onsubmit="return false;">
        <input type="text" placeholder="Název tabulky" id="nazevTable" name="nazevTable" value="{$nazevTabulky}" size="" required>
        <input type="hidden" id="iPKHeader" name="iPKHeader" value="{$iPKHeader}">
        <div style="clear: both;"></div>
        <label for="spec" style="margin-left: 35px; margin-right: 8px;">Specializace:</label>
        <input name="spec" type="text" disabled value="{if $rpSpec.sName == ''} Žádná specializace {else} {$rpSpec.sName} {/if}" style="font-size: 12pt;">
  <table style="margin-top: 10px;" class="modal-rt-table-frozen">
    <thead>
      
      <tr>
         <th class="modal-rt-frozen-td" style="width:272px;">
          týden
        </th>
        <th >
          41
        </th>
        <th>
          42
        </th>
        <th>
          43
        </th>
        <th>
          44
        </th>
        <th>
          45
        </th>
        <th>
          46
        </th>
        <th>
          47
        </th>
        <th>
          48
        </th>
        <th>
          49
        </th>
        <th>
          50
        </th>
        <th>
          51
        </th>
        <th>
          52
        </th>
        <th>
          1
        </th>
        <th>
          2
        </th>
        <th>
          3
        </th>
        <th>
          4
        </th>
        <th>
          5
        </th>
        <th>
          6
        </th>
        <th>
          7
        </th>
        <th>
          8
        </th>
        <th>
          9
        </th>
        <th>
          10
        </th>
        <th>
          11
        </th>
        <th>
          12
        </th>
        <th>
          13
        </th>
        <th>
          14
        </th>
        <th>
          15
        </th>
        <th>
          16
        </th>
        <th>
          17
        </th>
        <th>
          18
        </th>
        <th>
          19
        </th>
        <th>
          20
        </th>
        <th>
          21
        </th>
        <th>
          22
        </th>
        <th>
          23
        </th>
        <th>
          24
        </th>
        <th>
          25
        </th>
        <th>
          26
        </th>
        <th>
          27
        </th>
        <th>
          28
        </th>
        <th>
          29
        </th>
        <th>
          30
        </th>
        <th>
          31
        </th>
        <th>
          32
        </th>
        <th>
          33
        </th>
        <th>
          34
        </th>
        <th>
          35
        </th>
        <th>
          36
        </th>
        <th>
          37
        </th>
        <th>
          38
        </th>
        <th>
          39
        </th>
        <th>
          40
        </th>
      </tr>
    </thead>
      <tbody>
      {foreach from=$tydenData item=i key=k}          
      <tr class="rt-tr-input">
        <td nowrap check="1"  class="modal-rt-frozen-td" style="background-color:#fff;">
            <span class="td-minus"><i  style="color:red;font-size:20px;cursor:pointer" class="fa fa-minus-square" aria-hidden="true"></i></span>
                {foreach from=$ots item=itemOTS}
                    {if $itemOTS.iPK == $k} 
                      <input style=" height: 30px;"   type="text" class="checkInput" iPKots="{$itemOTS.iPK}" value="{$itemOTS.sName}" disabled/>
                    {/if}
                  {/foreach}             
        
      </td>
      {foreach from=$i item=rozpisValue key=tydenKey}
      <td colspan="1" nowrap>
          <span class="rtKey">{$tydenKey}:</span><input type="text" id="{$tydenKey}_{$k}" name="{$tydenKey}_{$k}" value="{$rozpisValue}"  />
      </td> 
       {/foreach}
      
      </tr>
       {/foreach}
      <tr id="pridatTr" >

      </tr>

    </tbody>
  </table>
        <i id="plus" style="color:green;font-size:28px;cursor:pointer;margin-top:10px;" class="fa fa-plus-square" aria-hidden="true"></i>
        <input type="submit" id="submit" value="ULOŽIT" class="btn-sub-menu modal-rt-ulozit">
</form>
</div>
