<script>
$(function() {
  // select2
    $('.select-2').select2();

$("#filtruj-t-report").click(function () {
      var ajaxModul = $(this).attr("id");
      //var id_user = $('#filtr-uzivatel-select').val();
      var selectedTyden = $('#selectedTyden').html();
      $('#tyden-form').val(selectedTyden).trigger('change');
      var iStav = $('#filtr-stav-select').val();
      
      var atlet_id_user = $('#filtr-uzivatel-select').val();
   
      $.ajax({
        type: "POST",
        url: "ajax/ajax.php",
        data: "sAction=" + ajaxModul + "&tyden=" + selectedTyden + "&iStav=" + iStav + "&atlet_id_user=" + atlet_id_user,
        success: function (content) {          
        if (content) {
            $(".day-row-container").html(content);
          }
        }
      });
      return false;
    });
    
    var aktualniTyden = $("#selectedTyden").val();
    
    $("#tydenPlus").on("click", function(){
      aktualniTyden ++;
      if (aktualniTyden == 53){
        alert("Maximální týden je 52");
        aktualniTyden --;
      }
      $('#selectedTyden').text(aktualniTyden);
      $('#selectedTyden').val(aktualniTyden);
      $('#filtruj-t-report').click();
      return false;
    });
    
    $("#tydenMinus").click(function(){
      aktualniTyden --;
      if (aktualniTyden == 0){
        alert("Hodnota týden nemůže být 0");
        aktualniTyden ++;
      }
      $('#selectedTyden').text(aktualniTyden);
      $('#selectedTyden').val(aktualniTyden);
      $('#filtruj-t-report').click();
      return false;
    });
    $('#tyden-form').on('change', function(){
      var selectedTyden = $('#tyden-form option:selected').val();
      //$('#selectedTyden').text(selectedTyden);
      $('#selectedTyden').val(selectedTyden);
      $('#selectedTyden').html(selectedTyden);
      aktualniTyden = selectedTyden;
      return false;

    });


});//function


</script>
<!-- t-reporty.tpl -->
<h2 class="sekce">T - reporty</h2>
      <div class="filtr">
        <div class="filtr-block">
    <label for="tyden-from">Tyden</label>
    <select name="tyden-form" id="tyden-form" class="select-2" style="width:60px;">
      {for $foo=1 to 52}
      <option value="{$foo}" {if $aktualniTyden == $foo}selected{/if}>{$foo}</option>
      {/for}
    </select>
  </div>
        <div class="filtr-block">
    <label for="filtr-uzivatel-select">Uživatel</label>
    <select id="filtr-uzivatel-select" class="select-2" style="width:350px;">
      {foreach from=$trenerAtleti key=k item=i}
      <option value="{$i.id_user}">[{$i.email}] {$i.titul} {$i.jmeno} {$i.prijmeni}</option>
      {/foreach}
    </select>
  </div>
        <div class="filtr-block">
          <label for="filtr-stav-select">Stav</label>
            <select id="filtr-stav-select">
        <option value="0">
         Vše
      </option>
        <option value="1">
         Nepotvrzeno
      </option>
      <option value="2">
         Potvrzeno
      </option>
    </select>
        </div>

        <div style="clear:both"></div>

        <div class="filtr-block filtr-filtruj">
            <button class="btn-filtr" id="filtruj-t-report">FILTRUJ</button>
        </div>
        <div class="filtr-block filtr-filtruj">
    <button class="btn-filtr">ULOŽIT</button>
  </div>


      </div>
      <!--/filtr -->
      <div style="clear:both"></div>

<button id="tydenMinus" class="btn-filtr">
  <i class="fa fa-arrow-left" aria-hidden="true"></i>
</button>
<button class="btn-filtr" id="selectedTyden" value="{$aktualniTyden|intval}">{$aktualniTyden|intval}</button>
<button id="tydenPlus" class="btn-filtr">
  <i class="fa fa-arrow-right" aria-hidden="true"></i>
</button>

<div class="day-row-container">
  
</div>

    
