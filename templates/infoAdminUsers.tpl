<div class="info table1">
	<table class="admin-users">
		<thead>
		<tr>
			<th>ID</th>
			<th>KOD</th>
			<th>Login</th>
			<th>Email</th>
			<th>Titul</th>
			<th>Jmeno</th>
			<th>Prijmeni</th>
			<th>Typ</th>
			<th>Popis</th>
			<th><i class="fas fa-pencil-alt"></i></th>

		</tr>
		</thead>
		<tbody>
{foreach from=$users key=k item=i}
  <tr iduser="{$i.id_user}">
  	<td>{$i.id_user}</td>
  	<td>{$i.kod}</td>
  	<td>{$i.login}</td>
  	<td>{$i.email}</td>
  	<td>{$i.titul}</td>
  	<td>{$i.jmeno}</td>
  	<td>{$i.prijmeni}</td>
  	<td>{$i.typ_user}</td>
  	<td>{$i.popis}</td>
  	<td><span class="editUser"><i class="fas fa-pencil-alt" style="cursor:pointer;"></i></span></td>

  </tr>
{/foreach}
</tbody>
</table>
</div>
<script type="text/javascript">
$(function() {	
	$('.admin-users').on('click', '.editUser', function(){
                  var id = 0;
                  
                  id = $(this).closest('tr').attr('iduser');
                  var dataString = "sAction=adminDetailUser&id="+id;
                  $.ajax({
                    type: "POST",
                    url: "ajax/ajax.php",
                    data: dataString,
                    success: function(content){
                      $('.modal-global-content').html(content);
                      $('.modal-global').show();
                    }
                  });
                  
                  return false;

    });
});    
</script>
