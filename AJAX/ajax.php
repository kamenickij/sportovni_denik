<?php
require '../autoloader.php';


$sAction = $_POST['sAction'];


if($sAction == 'loginUser') {
    $cUser = new cUser();
    $login = $_POST['login'];
    $password = $_POST['password'];
    $returnLogin = $cUser->login($login, $password);
    header("Refresh:0; url=..");
}
if($sAction == 'logoutUser') {
    $_SESSION['IDUSER'] = 0;
}
if($sAction == 'denik') {
    $smarty->display('../templates/main2.tpl');
}
if($sAction == 'sdileni') {
  $cRozpis=new cRozpis();
  $id_user = $_SESSION['IDUSER'];
  $rozpisUsers=$cRozpis->getUsersSdileni();
  $rocniTabulky =$cRozpis->getRozpisTablesSdileni($id_user);
  
  //print_r($rozpisUsers);
  //print_r($rocniTabulky);
  $smarty->assign('rozpisUsers', $rozpisUsers);
  $smarty->assign('rocniTabulky', $rocniTabulky);
    $smarty->display('../templates/sdileni.tpl');
}
if($sAction == 'admin') {
    
      $smarty->display('../templates/admin.tpl');
  }
if($sAction == 'adminOveritUser') {
    $email = $_POST['emailHledat']; 
    $id = $_POST['idHledat']; 
    $sAction = "overitUser";
    $sql = "CALL sp_adminUser ('$sAction', '$email', '$id', '', '', '', '','','','')";
    $cDB = new cDB();
    $sql_result = $cDB->sql_query($sql);
    $cDB->sql_next_result();
    $result = $cDB->sql_fetch_assoc($sql_result);
    $message = "";
    if ( count($result) > 0 ) {
        $message = "Tento uživatel již v DB existuje.";
    } else {
        $message = "Tento uživatel v DB neexistuje.";
    }
    //print_r($result);
    $smarty->assign('message', $message);
    $smarty->assign('uzivatel', $result);
    $smarty->display('../templates/infoAdmin.tpl');

    
}
if($sAction == 'adminAllUser') {
    $cDB = new cDB();
    $sql = "SELECT `id_user`, `login`, `titul`, `jmeno`, `prijmeni`, `popis`, `typ_user`, `email`, `kod` FROM user";
    
    $sql_query = $cDB->sql_query($sql);
    $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }
   $users = $result;      
    
   // print_r($sql_query);
   // print_r($users);
   
    $smarty->assign('users', $users);
    $smarty->display('../templates/infoAdminUsers.tpl');

    
}
if($sAction == 'adminDetailUser') {
    $id = $_POST['id'];
    $cDB = new cDB();
    $sql = "SELECT `id_user`, `login`, `titul`, `jmeno`, `prijmeni`, `popis`, `typ_user`, `email`, `kod` FROM user WHERE `id_user`=$id";
    
    $sql_query = $cDB->sql_query($sql);
    $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }
   $users = $result;      
    
   // print_r($sql_query);
   //print_r($users);
   
    $smarty->assign('users', $users);
    $smarty->display('../templates/infoAdminDetailUsers.tpl');

    
}
if($sAction == 'adminRegistruj') {
    //nejdrive overeni zda-li tam uzivatel jiz neni
    $sAction = 'overitUser';
    $email = $_POST['email']; 
    $id = 0;   
    $cDB = new cDB();
    $sql = "CALL sp_adminUser ('$sAction', '$email', '$id', '', '', '', '','','','','')";
    //print_r($_POST);
    $sql_result = $cDB->sql_query($sql);
    $cDB->sql_next_result();
    $result = $cDB->sql_fetch_assoc($sql_result);
    $message = "";
    if ( count($result) > 0 ) {
        $message = "Tento uživatel již v DB existuje.";
    } else {
    $sAction = 'registrujUser';
    $email = isset($_POST['email']) ? $_POST['email'] : '' ; 
    $kod = isset($_POST['kod']) ? $_POST['kod'] : '' ; 
    $heslo = isset($_POST['heslo']) ? $_POST['heslo'] : '' ; 
    $login = isset($_POST['login']) ? $_POST['login'] : '' ; 
    $titul = isset($_POST['titul']) ? $_POST['titul'] : '' ; 
    $jmeno = isset($_POST['jmeno']) ? $_POST['jmeno'] : '' ; 
    $prijmeni = isset($_POST['prijmeni']) ? $_POST['prijmeni'] : '' ; 
    $popis = isset($_POST['popis']) ? $_POST['popis'] : '' ; 
    $typ= isset($_POST['typ']) ? $_POST['typ'] : '' ; 
    $heslo = hash('sha256', $heslo);
    
    $sql = "CALL sp_adminUser ('$sAction', '$email', '$id', '$login', '$heslo', '$jmeno', '$popis', '$prijmeni','$titul','$typ','$kod')";
    //print_r($sql);
    $sql_result = $cDB->sql_query($sql);
    $cDB->sql_next_result();
    $result = $cDB->sql_fetch_assoc($sql_result);
    $message = "Zapis do DB.";
    }

    
   
    //print_r($result);
    $smarty->assign('message', $message);
    
    $smarty->display('../templates/infoAdmin.tpl');

    
}

if($sAction == 'adminEditUser') {
    $cUser = new cUser;
    print $cUser->editUser($_POST);
}

if($sAction == 'adminOtu') {

    $cRozpis = new cRozpis();
    $sAction2 = $_POST['sAction2'];
    
    if($sAction2 == 'hledatOtu'){
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $nazev = isset($_POST['nazev']) ? $_POST['nazev'] : '';   
        $rozpisOts=$cRozpis->getOtsHledat($id, $nazev);
        $nazev ='';
        $rozpisSpec=$cRozpis->getSpecHledat($nazev);

        //print_r($rozpisOts);
        $smarty->assign('rozpisOts', $rozpisOts);
        $smarty->assign('rozpisSpec', $rozpisSpec);
        $smarty->display('../templates/otsAdmin.tpl');
    }
    if($sAction2 == 'hledatSpec'){
    $nazev = isset($_POST['nazev']) ? $_POST['nazev'] : '';   
    $rozpisSpec=$cRozpis->getSpecHledat($nazev);
    //print_r($rozpisOts);
    $smarty->assign('rozpisSpec', $rozpisSpec);
    $smarty->display('../templates/specAdmin.tpl');

    }
    if($sAction2 == 'deleteSpec'){
        $cDB = new cDB();
        $iPK = $_POST['idSpec'];
        $message = "";
        $sql = "DELETE FROM spec WHERE iPK='$iPK'";
        $sql_result = $cDB->sql_query($sql);
        $message = "Specialiazce byla smazána.";
        $smarty->assign('message', $message);
    
        $smarty->display('../templates/infoAdmin.tpl');

    }
    if($sAction2 == 'novaSpec'){
        $nazev = $_POST['nazev'];   
        $cDB = new cDB();
        $sql = "SELECT * FROM spec WHERE sName='$nazev'";
        $sql_result = $cDB->sql_query($sql);
        $result = $cDB->sql_fetch_assoc($sql_result);
        $message = "";
        if ( count($result) > 0 ) {
            $message = "Tato specializace již v DB existuje.";
        } else{
            $sql = "INSERT INTO spec (sName) VALUES ('$nazev')";
            $sql_result = $cDB->sql_query($sql);
            $message = "Specializace byla zapsána";
        }
        $smarty->assign('message', $message);
        
        $smarty->display('../templates/infoAdmin.tpl');

    }
    if($sAction2 == 'nastavitSpecOtu'){
        $otuIPK = $_POST['iPK'];
        $specValue = $_POST['specValue'];
        $specArray = explode(',', $specValue);        
        $cAdmin = new cAdmin();        
        print_r($cAdmin->delOtu_Spec($otuIPK));   
         
        print_r($cAdmin->update_Spec_In_Ots($otuIPK));           
        
        $cAdmin->setOtu_Spec($otuIPK, $specArray);
       print 'OK';
    }
}//adminOtu
if($sAction == 'sdileni-filtr') {
    $id_user = $_SESSION['IDUSER'];
    $jmeno = $_POST['jmeno'];
    $prijmeni = $_POST['prijmeni'];
    $email = $_POST['email'];
    $ID = $_POST['ID'];
    $rt = $_POST['rt'];
    $cRozpis=new cRozpis();
    
  }
if( $sAction == "sdileni-deconnect"){
    $id_user = $_SESSION['IDUSER'];
    $id_user_deconnect = $_POST["id_user_deconnect"];
    $cRozpis = new cRozpis();
    $result = $cRozpis->deconnectUser($id_user, $id_user_deconnect);
    //print_r($result);


    }  
    if( $sAction == "newOtu"){
    $id_user = $_SESSION['IDUSER'];
    $nazev = $_POST['name'];
    $mj = $_POST['mj'];
    $zkratka = $_POST['zkratka'];
    $kod = $_POST['kod'];
    $cDB = new cDB();
    $sql = "INSERT INTO ots (`sName`, `sMj`, `sZkrat`, `kod` ) VALUES('$nazev', '$mj', '$zkratka', '$kod') ";
    $sql_result = $cDB->sql_query($sql);

    //$rpSpec = $cDB->sql_fetch_assoc($sql_result);
    //print_r($result);

    } 
    if( $sAction == "adminDeleteOtu"){
    $id_user = $_SESSION['IDUSER'];
    $otuIPK = $_POST['iPK'];
    
    $cDB = new cDB();
    $sql = "DELETE from aktivityPredpis WHERE `ots_iPK`=$otuIPK";
    $sql_result = $cDB->sql_query($sql);
    $sql = "DELETE from otu_spec WHERE `ots_iPK`=$otuIPK";
    $sql_result = $cDB->sql_query($sql);
    $sql = "DELETE from ots WHERE `iPK`=$otuIPK";
    $sql_result = $cDB->sql_query($sql);

    //$rpSpec = $cDB->sql_fetch_assoc($sql_result);
    print_r($sql_result);

    } 
    if( $sAction == "adminEditOtu"){
    $id_user = $_SESSION['IDUSER'];
    $otuIPK = $_POST['iPK'];
    
    $cDB = new cDB();
    $sql = "SELECT * from ots WHERE `iPK`=$otuIPK";
    $sql_result = $cDB->sql_query($sql);

    $otuSpec = $cDB->sql_fetch_assoc($sql_result);
    //print_r($otuSpec);

    $smarty->assign('otuSpec', $otuSpec);
    $smarty->display('../templates/detailOtu.tpl');

    } 
     if( $sAction == "editOtu"){
    $id_user = $_SESSION['IDUSER'];
    $otuIPK = $_POST['sipk'];
    $sname = $_POST['sname'];
    $smj = $_POST['smj'];
    $kod = $_POST['kod'];
    
    $cDB = new cDB();
    $sql = "UPDATE ots SET `sName`='$sname', `sMj`='$smj', `kod`='$kod'  WHERE `iPK`=$otuIPK";
    print_r($sql);
    $sql_result = $cDB->sql_query($sql);
    print_r($sql_result);

    

    } 
    if($sAction == 'ulozitAsdiletModal') {
        $cRozpis=new cRozpis();
        $id_user = $_SESSION['IDUSER'];
        $idUserCopyFrom = $_POST['idUserCopyFrom'];
        $nasdilenyUsers = $cRozpis->getUsersSdileniCopyModal($idUserCopyFrom);
        //print_r($nasdilenyUsers);
        $smarty->assign('nasdilenyUsers', $nasdilenyUsers);
        $smarty->display('../templates/nasdilenyUsers.tpl');
    }
if($sAction == 'pridatUser') {
    $cRozpis=new cRozpis();
    $id_user = $_SESSION['IDUSER'];
    $idAdd = $_POST['idAdd'];
    $emailAdd = $_POST['emailAdd'];

    $messagePridani=$cRozpis->pridatUser($idAdd, $emailAdd, $id_user);
    $rozpisUsers=$cRozpis->getUsersSdileni();
    $rocniTabulky =$cRozpis->getRozpisTablesSdileni($id_user);
    //print_r($rozpisUsers);
    $smarty->assign('rozpisUsers', $rozpisUsers);
    $smarty->assign('rocniTabulky', $rocniTabulky);
    $smarty->assign('messagePridani', $messagePridani);
      $smarty->display('../templates/sdileni.tpl');
  }
if($sAction == 'priraditTabulku') {
    $cRozpis=new cRozpis();
    $id_user_tabulka = $_POST['id_user'];
    $ipk_table = $_POST['iPK'];
    $message = $cRozpis->priraditTabulku($id_user_tabulka, $ipk_table );
    $smarty->assign('message', $message);
    $smarty->display('../templates/info.tpl');

  }
if($sAction == 'home') {
    $smarty->display('../templates/home.tpl');
}
if($sAction == 'reporty') {
    $rok = date("Y");
    //print_r($rok);
    $cRozpis=new cRozpis();
    $rozpisUsers=$cRozpis->getUsersSdileni();
    $smarty->assign('rozpisUsers', $rozpisUsers);
    $smarty->assign('rok', $rok);
    $smarty->display('../templates/reporty.tpl');
}
if($sAction == 'rocni-predpis') {
    $cRozpis = new cRozpis();    
    $id_user = $_SESSION['IDUSER'];
    $atlet = @$_POST['atlet'];
    $nazevTabulky = @$_POST['name'];
    $rozpisHeader = $cRozpis->getRozpisTablesHeader($id_user,$nazevTabulky, $atlet);
    $selectUzivatelu = $cRozpis->getUsersSdileni();
    $smarty->assign('nazevTabulky', $nazevTabulky);
    $smarty->assign('atlet', $atlet);
    
    $smarty->assign('rozpisHeader', $rozpisHeader);
    $smarty->assign('selectUzivatelu', $selectUzivatelu);
    $smarty->display('../templates/rocni-predpis.tpl');
}
if($sAction == 'tydenni-predpis') {
    $cRozpis = new cRozpis();
    $cUser = new cUser();
    //$tydenniRozpis = $cRozpis->getTydenniRozpis(2, 3);
    $trenerAtleti = $cUser->getTrenerAtleti();
    date_default_timezone_set('UTC');
    $aktualniTyden = date('W');
    $aktualniRok = date('o');
    $aktualniDen = date('j.n.o');
    $ots = $cRozpis->getOts();
    $day = date('N');
    $tyden = $cRozpis->getTydenDny($aktualniTyden, $aktualniRok);
    $smarty->assign('trenerAtleti', $trenerAtleti);
    $smarty->assign('tyden', $tyden);
    $smarty->assign('aktualniTyden', $aktualniTyden);
    $smarty->assign('aktualniDen', $aktualniDen);
    $smarty->assign('ots',$ots);
   // $smarty->assign('tydenniRozpis', $tydenniRozpis);
    $smarty->display('../templates/tydenni-predpis.tpl');
}
if($sAction == 'rocni-tabulka') {
    $cRozpis = new cRozpis();
    $ots = $cRozpis->getOts();
    $nazev='';
    $rozpisSpec=$cRozpis->getSpecHledat($nazev);
    $smarty->assign('ots',$ots);
    $smarty->assign('rozpisSpec',$rozpisSpec);
    //print_r($ots);
    $smarty->display('../templates/modal-rt.tpl');
}

if($sAction == 'seznam-ots') {
    $cRozpis=new cRozpis();
    $rozpisOts=$cRozpis->getOts();
    //print_r($rozpisOts);
    $smarty->assign('rozpisOts', $rozpisOts);
    $smarty->display('../templates/ots.tpl');
}
if($sAction == 'rt-sdileni') {
    $tableIPK = $_POST['tableipk'];
    $cRozpis=new cRozpis();
    $rozpisUsers=$cRozpis->getUsersSdileniRt($tableIPK);
    //print_r($rozpisUsers);
    $smarty->assign('rozpisUsers', $rozpisUsers);
    $smarty->display('../templates/modal-users.tpl');
}
if($sAction == 't-reporty') {
    $cRozpis = new cRozpis();
    $cUser = new cUser();
    $trenerAtleti = $cUser->getTrenerAtleti();
    date_default_timezone_set('UTC');
    $aktualniTyden = date('W');
    $aktualniRok = date('o');
    $aktualniDen = date('j.n.o');
    $day = date('N');
    $tyden = $cRozpis->getTydenDny($aktualniTyden, $aktualniRok);
    
    $smarty->assign('tyden', $tyden);
    $smarty->assign('aktualniTyden', $aktualniTyden);
    $smarty->assign('aktualniDen', $aktualniDen);
    $trenerAtleti = $cUser->getTrenerAtleti();
    $smarty->assign('trenerAtleti', $trenerAtleti);
    $smarty->display('../templates/t-reporty.tpl');
}
if($sAction == 'a-reporty') {
    $cRozpis = new cRozpis();
    
    date_default_timezone_set('UTC');
    $aktualniTyden = date('W');
    $aktualniRok = date('o');
    $aktualniDen = date('j.n.o');
    $day = date('N');
    $tyden = $cRozpis->getTydenDny($aktualniTyden, $aktualniRok);
    $smarty->assign('tyden', $tyden);
    $smarty->assign('aktualniTyden', $aktualniTyden);
    $smarty->assign('aktualniDen', $aktualniDen);
    $smarty->display('../templates/a-reporty.tpl');
}
if($sAction == 'zobrazit-rt') {

            $iPK = $_POST['iPrimaryKey'];
          $cRozpis = new cRozpis();
          $id_user = $_SESSION['IDUSER'];
          $rozpisTable = $cRozpis->getRozpisTable($iPK);
            $nazevTabulky = $rozpisTable[0]['nazevTabulky'];
            $iPKHeader = $rozpisTable[0]['iPKHeader'];
          foreach($rozpisTable as $key => $data) {
           //   print_r($key);
           $tydenData[$data['specializace']][$data['tyden']]=$data['hodnota'];
                 //$tydenData[$data['tyden']]=$data['hodnota'];
          }
          $tydenMezocyklus = '41,42,43,44,45,46,47,48,49,50,51,52,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40';
          $tydenMezo = explode(',', $tydenMezocyklus);
          
          foreach($tydenMezo as $key => $item) {
              foreach($tydenData as $k2 => $i2) {
                  $tydenDataMezo[$k2][$item] = $i2[$item];
              }
            //$tydenData[$data['specializace']][$data['tyden']]=$data['hodnota'];
                 //$tydenData[$data['tyden']]=$data['hodnota'];
          }
          //print_r($tydenData);

          //specialiazace rocni tabulky
          $cDB = new cDB();
            $sql = "SELECT s.`sName` FROM rocni_predpis rp
        JOIN  spec s on s.`iPK` = rp.`specializace`
        WHERE rp.`iPK` = '$iPK'";
            $sql_result = $cDB->sql_query($sql);

            $rpSpec = $cDB->sql_fetch_assoc($sql_result);

          $ots = $cRozpis->getOts();
          $nazev='';

          $rozpisSpec=$cRozpis->getSpecHledat($nazev);

          $smarty->assign('rozpisSpec',$rozpisSpec);
          $smarty->assign('rozpisTable', $rozpisTable);
          $smarty->assign('tydenData', $tydenDataMezo);
          $smarty->assign('nazevTabulky', $nazevTabulky);
          $smarty->assign('ots', $ots);
          $smarty->assign('iPKHeader', $iPKHeader);
          $smarty->assign('rpSpec', $rpSpec);



          $smarty->display('../templates/modal-rt-db.tpl');
}
if($sAction == 'smazat-rt') {
  $iPK = $_POST['iPK'];
  $cRozpis = new cRozpis();
  //$id_user = $_SESSION['IDUSER'];
  $cRozpis->delRozpisTable($iPK);
}

if($sAction == 'rocni_rozpis_save') {
    $cRozpis = new cRozpis();
    $cRozpis->rozpis_save($_POST);
    //$smarty->display('../templates/reporty.tpl');
}

if($sAction == 'rocni_rozpis_update_save') {
    $cRozpis = new cRozpis();
    $cRozpis->rozpis_update_save($_POST);
    //$smarty->display('../templates/reporty.tpl');
}
if($sAction == 'plus') {
    $cRozpis = new cRozpis();
    $ots = $cRozpis->getOts();
    $smarty->assign('ots',$ots);
    $smarty->display('../templates/plus.tpl');
}


if($sAction == 'filtruj-tydenni-predpis') {
    $cRozpis = new cRozpis();
    date_default_timezone_set('UTC');
    //$aktualniTyden = date('W');
    $aktualniTyden = $_POST['tyden'];
    $aktualniRok = date('o');
    $aktualniDen = date('j.n.o');
    $ots = $cRozpis->getOts();
    $otsPrehled = $cRozpis->getOtsPrehled();
    $id_user_atlet = $_POST['id_user_atlet'];    
    $day = date('N');
    $tyden = $cRozpis->getTydenDny($aktualniTyden, $aktualniRok);  
    $rp = array();
    $tydenRozpis = $cRozpis->getTydenRozpis($id_user_atlet, $tyden);     
    $tydenRozpisPrehled = $cRozpis->getTydenRozpisPrehled($id_user_atlet, $tyden); 
    $rocniRozpis = $cRozpis->getRocniPredpisTyden($id_user_atlet, (int)$aktualniTyden); 
    foreach ($rocniRozpis as $key => $value) {
        $rp[$key]['rocni'] = $value;
        $rp[$key]['nazev'] = $otsPrehled[$key];
        $rp[$key]['iPK'] = $key;
        
    }
    foreach ($tydenRozpisPrehled as $key => $value) {
        $rp[$key]['tyden'] = $value;
        if(!isset($rp[$key]['rozdil']) || (int)$rp[$key]['rozdil'] == 0) {
            $rp[$key]['rozdil'] = 0;
        }
        $rp[$key]['rozdil'] = $rp[$key]['rocni'] - $rp[$key]['tyden'];
        
    }
   
    //print_r($ots);
    //print_r($rp);
    //print_r($tydenRozpis);
    //print_r($rocniRozpis);
    $smarty->assign('tyden', $tyden);
    $smarty->assign('aktualniTyden', $aktualniTyden);
    $smarty->assign('aktualniDen', $aktualniDen);
    $smarty->assign('tydenRozpis', $tydenRozpis);
    $smarty->assign('ots',$ots);
    $smarty->assign('rp', $rp);
    $smarty->display('../templates/tydenni-predpis-tyden.tpl');
}
//filtruj a-report
if($sAction == 'filtruj-a-report') {
    $cRozpis = new cRozpis();
    $cUser = new cUser();
    date_default_timezone_set('UTC');
    //$aktualniTyden = date('W');
    $aktualniTyden = $_POST['tyden'];
    $iStav = $_POST['iStav'];
    $aktualniRok = date('o');
    $aktualniDen = date('j.n.o');
    $ots = $cRozpis->getOts();
    $id_user_atlet = $_SESSION['IDUSER'];    
    $day = date('N');
    $idTrener = $cUser->getUserTrener();
    $tyden = $cRozpis->getTydenDny($aktualniTyden, $aktualniRok);  
    $tydenRozpis = $cRozpis->getTydenRozpisAtlet($id_user_atlet, $tyden, $iStav); 
    $poznamka = $cRozpis->getPoznamkaAtlet($id_user_atlet, $idTrener, $tyden);
    $smarty->assign('idTrener', $idTrener);
    $smarty->assign('tyden', $tyden);
    $smarty->assign('aktualniTyden', $aktualniTyden);
    $smarty->assign('aktualniDen', $aktualniDen);
    $smarty->assign('tydenRozpis', $tydenRozpis);
    $smarty->assign('poznamka', $poznamka);
    $smarty->assign('ots',$ots);
    $smarty->display('../templates/a-reporty-tyden.tpl');
}

if($sAction == 'filtruj-t-report') {
    $cRozpis = new cRozpis();
    $cUser = new cUser();
    date_default_timezone_set('UTC');
    //$aktualniTyden = date('W');
    $aktualniTyden = $_POST['tyden'];
    $iStav = $_POST['iStav'];
    $aktualniRok = date('o');
    $aktualniDen = date('j.n.o');
    $ots = $cRozpis->getOts();
    $id_user_atlet = $_POST['atlet_id_user'];    
    $day = date('N');
    $idTrener = $_SESSION['IDUSER'];
    $tyden = $cRozpis->getTydenDny($aktualniTyden, $aktualniRok);  
    $tydenRozpis = $cRozpis->getTydenRozpisAtlet($id_user_atlet, $tyden, $iStav); 
    $poznamka = $cRozpis->getPoznamkaAtlet($id_user_atlet, $idTrener, $tyden);     
    $smarty->assign('idTrener', $idTrener);
    $smarty->assign('tyden', $tyden);
    $smarty->assign('aktualniTyden', $aktualniTyden);
    $smarty->assign('aktualniDen', $aktualniDen);
    $smarty->assign('tydenRozpis', $tydenRozpis);
    $smarty->assign('poznamka', $poznamka);
    $smarty->assign('ots',$ots);
    $smarty->display('../templates/t-reporty-tyden.tpl');
}

if($sAction == 'zamitnoutPolozku') {
    $cRozpis = new cRozpis();
    $iPK = $_POST['iPK'];    
    $stav = $cRozpis->setRozpisStav($iPK, 3); 
    print $stav;
}

if($sAction == 'doJineFaze') {
    // 4 do faze1, 5 do faze 2, 6 do faze 3
    $cRozpis = new cRozpis();
    $iPK = $_POST['iPK'];
    $doFaze = $_POST['doFaze'];
    if ($doFaze == 4) {
        $iFaze = 1;
    } elseif ($doFaze == 5) {
        $iFaze = 2;        
    } elseif ($doFaze == 6) {
        $iFaze = 3;
    } else {
        $iFaze = 0;
    }
    $stav = $cRozpis->setRozpisStav($iPK, $doFaze); 
    
    if ($iFaze > 0) {
        print_r($cRozpis->presunDoFaze($iPK, $iFaze));
    }
    print $stav;
}
    
if($sAction == 'confirmPolozku') {
    $cRozpis = new cRozpis();
    $iPK = $_POST['iPK'];    
    $stav = $cRozpis->setRozpisStav($iPK, 2); 
    print $stav;
}

if($sAction == 'dataDenAtlet') {
    $cRozpis = new cRozpis();
    $sPoznamka = $_POST['poznamka'];  
    $den = $_POST['den'];
    $iNemoc = $_POST['iNemoc'];
    $iZavod = $_POST['iZavod'];
    $sNemoc = $_POST['sNemoc'];
    $sZavod = $_POST['sZavod'];
    $casZat = $_POST['casZat'];
    $casReg = $_POST['casReg'];
    $apa_ipk = $_POST['apa_ipk'];
    $id_user = $_SESSION['IDUSER'];
    $idTrener = $_POST['idTrener'];
    $den = date("Y-m-d", strtotime($den));
    $stav = $cRozpis->setPoznamkaAtlet($sPoznamka, $den, $id_user, $idTrener); 
    $stavData = $cRozpis->setDataAtlet($apa_ipk, $iNemoc, $iZavod, $sNemoc, $sZavod, $casZat, $casReg, $den, $id_user, $idTrener); 
    print $stav;
}

if($sAction == 'get-spec-otu') {
    $cRozpis = new cRozpis();
    $spec_iPK = $_POST['spec_iPK'];
    $otu = $cRozpis->getSpecOtu($spec_iPK); 
    //print_r($otu);
    $smarty->assign('otu',$otu);
    $ots = $cRozpis->getOts();
    $smarty->assign('ots',$ots);
    //print_r($otu);
    //print_r($ots);
    $smarty->display('../templates/modal-rtAjax.tpl');
}
if ($sAction == 'modalNasdileni'){
    $id_users = $_POST['id_users'];
    $id_user_origin = $_POST['id_user_origin'];
    $tyden = $_POST['tyden'];
    $cRozpis = new cRozpis();
    $cRozpis->copyTydenRozpis($tyden, $id_user_origin, $id_users);
}

if ($sAction == 'rt-kopie'){
    $iPK = $_POST['iPK'];
    $cRozpis = new cRozpis();
    print $cRozpis->copyRocniRozpis($iPK);
}
if($sAction == 'vlozitExterneStu') {
    
   
    $smarty->display('../templates/vlozitExterneStu.tpl');

    
}
if($sAction == 'oddil-clenove') {
    
   
    $smarty->display('../templates/oddil/oddil-clenove.tpl');

    
}
if($sAction == 'oddil-prispevky') {
    
   
    $smarty->display('../templates/oddil/oddil-prispevky.tpl');

    
}
if($sAction == 'oddil-dochazka') {
    $today = date("Y-m-d");
    //$date = isset($_POST['date']) ? $_POST['date'] : $today;
    if (isset($_POST['date'])){
        $date = $_POST['date'];
        $today = $date;
    } else {
        $date = $today;
    }
    $id_user = $_SESSION['IDUSER'];
	$cOddil = new cOddil();
	$aClenove = $cOddil -> dochazkaSeznamUsers($id_user);
    $aDochazkaHeader = $cOddil -> get_dochazka_table_header($id_user, $date);
    $aDochazkaClenove = $cOddil -> get_dochazka_den_clen($id_user, $date);
    //print_r($aDochazkaHeader);
    $arrIpk = array ();
    foreach ($aDochazkaClenove as $key => $value){
        $arrIpk[$key] = $value['id_clen'];
    }

    //print_r($aDochazkaClenove);
    //print_r($aClenove);
    $smarty->assign('today', $today);
	$smarty->assign('aClenove', $aClenove);
    $smarty->assign('aDochazkaHeader', $aDochazkaHeader);
    $smarty->assign('aDochazkaClenove', $aDochazkaClenove);
    $smarty->assign('arrIpk', $arrIpk);
    $smarty->display('../templates/oddil/oddil-dochazka.tpl');

    
}

if($sAction == 'reportyOddil'){
    $today = date("Y-m-d");
    $last_week = strtotime("-1 week +1 day");
    $last_week = date("Y-m-d",$last_week); 

    $cOddil = new cOddil();

    $aClenove = $cOddil -> oddReportyCelkemClenu();
    $aAktivni = $cOddil -> oddReportyCelkemClenuAktivni();
    $aTreneri = $cOddil -> oddReportyCelkemClenuAktivniTreneri();
    //print_r($aClenove);
    $smarty->assign('aClenove', $aClenove);
    $smarty->assign('aAktivni', $aAktivni);
    $smarty->assign('aTreneri', $aTreneri);
    $smarty->assign('today', $today);
    $smarty->assign('last_week', $last_week);
    $smarty->display('../templates/oddil/reportyOddil.tpl');

}


