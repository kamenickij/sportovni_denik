<?php
require '../autoloader.php';


$sAction = $_POST['sAction'];


if($sAction == 'oddil_new_clen') {
	$jmeno = $_POST['sName'];
	$prijmeni = $_POST['sName2'];
	$rocnik = $_POST['sRok'];
	$poznamka = $_POST['sNote'];
    $cOddil = new cOddil();
	$iReturn = $cOddil -> novyClen($jmeno, $prijmeni, $rocnik, $poznamka);
	//print_r($iReturn);
}
if($sAction == 'oddil_filtr_clen') {
	$jmeno = isset($_POST['filtr_name']) ? $_POST['filtr_name'] : '';
	$prijmeni = isset($_POST['filtr_name2']) ? $_POST['filtr_name2'] : '';
	$rocnik = isset($_POST['filtr_rocnik']) ? $_POST['filtr_rocnik'] : '';
	$chTrener = $_POST['filtr_trener'];
	$chArchive = $_POST['filtr_archive'];
    $cOddil = new cOddil();
	$aClenove = $cOddil -> filtrClen($jmeno, $prijmeni, $rocnik, $chTrener, $chArchive);
	//print_r($aReturn);

	$smarty->assign('aClenove', $aClenove);
    $smarty->display('../templates/oddil/clenoveTable.tpl');
}

if($sAction == 'oddil_filtr_prispevky') {
	$jmeno = isset($_POST['filtr_name']) ? $_POST['filtr_name'] : '';
	$prijmeni = isset($_POST['filtr_name2']) ? $_POST['filtr_name2'] : '';
	$rocnik = isset($_POST['filtr_rocnik']) ? $_POST['filtr_rocnik'] : '';
	$placenoDo = $_POST['filtr_placenoDo'];
	$chPlacenoDo = $_POST['filtr_chPlacenoDo'];
    $cOddil = new cOddil();
	$aClenove = $cOddil -> filtrPrispevek($jmeno, $prijmeni, $rocnik, $placenoDo, $chPlacenoDo);
	//print_r($aReturn);

	$smarty->assign('aClenove', $aClenove);
    $smarty->display('../templates/oddil/prispevkyTable.tpl');
}

if($sAction == 'prispevekClenSave') {
	$dPlacenoDo = $_POST['dPlacenoDo'];
	$iCastka = $_POST['iCastka'];
	$sTypPlatby = $_POST['sTypPlatby'];
	$sPoznamka = $_POST['sPoznamka'];
	$id_user = $_POST['id_user'];
    $cOddil = new cOddil();
	$iReturn = $cOddil -> prispevekClenSave($dPlacenoDo, $iCastka, $sTypPlatby, $sPoznamka, $id_user);

	print $iReturn;
}

if($sAction == 'sprava_skupin') {
	$smarty->display('../templates/oddil/spravaSkupin.tpl');
}
if($sAction == 'vytvorSkupinu'){
	$jmeno = isset($_POST['skupina_name']) ? $_POST['skupina_name'] : '';
	$poznamka = isset($_POST['skupina_poznamka']) ? $_POST['skupina_poznamka'] : '';
	$id_user = $_SESSION['IDUSER'];
	$cOddil = new cOddil();
	$iReturn = $cOddil -> novaSkupina($jmeno, $poznamka, $id_user);

	print $iReturn;

}
if($sAction == 'vypisSkupiny'){
	$id_user = $_SESSION['IDUSER'];
	$cOddil = new cOddil();
	$aSkupiny = $cOddil -> vypisSkupiny($id_user);
	//print_r($aSkupiny);
	$smarty->assign('aSkupiny', $aSkupiny);
    $smarty->display('../templates/oddil/skupinyTable.tpl');

}

if($sAction == 'dochazka_nastavit_k_sobe_clena'){
	
    $smarty->display('../templates/oddil/dochazka_nastavit_k_sobe_clena.tpl');

}

if($sAction == 'oddil_filtr_nastavit_k_sobe_clena') {
	$jmeno = isset($_POST['filtr_name']) ? $_POST['filtr_name'] : '';
	$prijmeni = isset($_POST['filtr_name2']) ? $_POST['filtr_name2'] : '';
	$rocnik = isset($_POST['filtr_rocnik']) ? $_POST['filtr_rocnik'] : '';
	$chTrener = $_POST['filtr_trener'];
	$chArchive = $_POST['filtr_archive'];
	$chShared = $_POST['filtr_shared'];
    $cOddil = new cOddil();
	$aClenove = $cOddil -> filtrClenDochazkaNasdilet($jmeno, $prijmeni, $rocnik, $chTrener, $chArchive, $chShared);
	//print_r($aReturn);

	$smarty->assign('aClenove', $aClenove);
    $smarty->display('../templates/oddil/dochazkaNasdileniTable.tpl');
}

if($sAction == 'oddil_dochazka_nasdilej_clena'){
	$id_user = $_POST['id_user'];
	$checked = $_POST['checked'];
	$id_trener = $_SESSION['IDUSER'];
	if($checked == 'true'){
		$checked = 1;
	} else {
		$checked = 0;
	}
	$cOddil = new cOddil();
	$iReturn = $cOddil -> dochazkaNasdilejClena($id_user, $checked, $id_trener);


}
//zapis jestli uzivatel byl v dany den na treninku
if($sAction == 'set_dochazka_den_clen') {
	$id_user = $_SESSION['IDUSER'];
	$idClen = $_POST['idClen'];
	$date = $_POST['date'];
	$checked = $_POST['checked'];
	if($checked == 'true'){
		$checked = 1;
	} else {
		$checked = 0;
	}
	$cOddil = new cOddil();
	$iReturn = $cOddil -> set_dochazka_den_clen($id_user, $idClen, $date, $checked);
	print $iReturn;
}

//zapis headru treninku
if($sAction == 'dochazkaTableHeader') {
	$id_user = $_SESSION['IDUSER'];
	$date = $_POST['date'];
	$od = $_POST['od'];
	$do = $_POST['do'];
	$misto = $_POST['misto'];
	$poznamka = $_POST['poznamka'];
	
	$cOddil = new cOddil();
	$iReturn = $cOddil -> set_dochazka_table_header($id_user, $date, $od, $do, $misto, $poznamka);
	print $iReturn;
}


//detail clena oddilu z table oddilClenove
if($sAction == 'detailClenoveTable') {
	$iPK = $_POST['iPK'];
	
	$cOddil = new cOddil();
	$aDetail = $cOddil -> get_detailClenoveTable($iPK);
	//print_r($aDetail);
	$smarty->assign('aDetail', $aDetail);
    $smarty->display('../templates/oddil/oddil-detail-clena.tpl');
}

//ulozit detail clena 
if($sAction == 'saveDetailClena') {
	$iPK = $_POST['iPK'];
	//$iTrener = isset($_POST['iTrener']) ? $_POST['iTrener'] : 0 ;
	if (isset($_POST['iTrener'])){
		$iTrener = 1;

	} else {
		$iTrener = 0;
	}
	if (isset($_POST['iArchive'])){
		$iArchive = 1;

	} else {
		$iArchive = 0;
	}
	$sJmeno = $_POST['sJmeno'];
	$sPrijmeni = $_POST['sPrijmeni'];
	$sText = $_POST['sText'];
	$sRocnik = $_POST['sRocnik'];
	
	$cOddil = new cOddil();
	$iReturn = $cOddil -> set_detail_clena($iPK, $iTrener, $iArchive, $sJmeno, $sPrijmeni, $sText, $sRocnik);
	print $iReturn;
}

//reporty filtr dochazka
if($sAction == 'reportFiltrDochazka') {
	$od = isset($_POST['od']) ? $_POST['od'] : '' ;
	$do = isset($_POST['do']) ? $_POST['do'] : '' ;
	$jmeno = isset($_POST['jmeno']) ? $_POST['jmeno'] : '' ;
	$prijmeni = isset($_POST['prijmeni']) ? $_POST['prijmeni'] : '' ;
	$rocnik = isset($_POST['rocnik']) ? $_POST['rocnik'] : '' ;
	$trener = isset($_POST['trener']) ? $_POST['trener'] : '' ;
	
	$cOddil = new cOddil();
	$aTable = $cOddil -> reporty_filtr_dochazka($od, $do, $jmeno, $prijmeni, $rocnik, $trener);
	/* $aDochazka = array ();
	foreach ($aTable as $key => $value) {
		$aDochazka[$value['dDate']] = array();
	} */
	//print_r($aTable);

	
	$smarty->assign('aTable', $aTable);
    $smarty->display('../templates/oddil/report_dochazka_table.tpl');
}

