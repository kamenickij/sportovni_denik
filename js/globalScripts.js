//globalni modal hlaseni vysledku ajaxu
function infoSuccess(message){
  $('.modal-global-info-content').html(message);
  $('.modal-global-info').fadeIn('slow');
  setTimeout(function() {
    $('.modal-global-info-close').click();
  }, 5000);
}
function infoFail(message){
  var message2 = "<i class='fas fa-exclamation-triangle'></i> ";
  message2 =  message2 + message;
  $('.modal-global-info-content').html(message2);
  $('.modal-global-info-content').addClass('red');
  $('.modal-global-info').fadeIn('slow');
}

function showMask() {
  var node = document.getElementById('sidebar');
  if (node.style.visibility=='visible') {
    node.style.visibility = 'hidden';
  }
  else
    node.style.visibility = 'visible'
}

$( document ).ajaxStart(function() {
  $( ".ajaxWorking" ).fadeIn( "fast" );
});
$( document ).ajaxStop(function() {
  $( ".ajaxWorking" ).fadeOut( "fast" );
});

$(function() {
  $(".edit").click(function(){
    $(".day-row-info").toggle();
  });
  $("#filtr-toggle").click(function(){
    $(".filtr").toggle();
  });



  //ovladani hlavniho menu
  $('menu').on('click','#miniLogo', function () {
      $('.sidebarContainer').fadeIn('slow');
  });
  $('#sidebar').on('click','.closeSidebar', function () {
    $('.sidebarContainer').fadeOut('slow');
  });

  
});//ready
