<?php
require 'autoloader.php';

$smarty = new Smarty;

$smarty->cache_lifetime = 120;

$smarty->display('templates/header.tpl');
$id_user = @$_SESSION['IDUSER'];

//$cDB = new cDB();

//$sql_query = $cDB->sql_query('select * from user');

//$cUser = new cUser();
//phpinfo();

if($id_user > 0) {
    $smarty->display('templates/main.tpl');
} else {
    $smarty->display('templates/login.tpl');
}
