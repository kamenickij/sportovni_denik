<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cUser
 *
 * @author kamen
 */
class cUser {
    private $cDB;
    public function __construct() {

    }

    public function login($login, $password) {
        $cDB = $this->getDB();
        $passwordHash = hash('sha256', $password);
        $passwordHashSA = hash('sha256', 'kosel11');
           $sql = "select * from user where login='$login' and (password='$passwordHash' or '$passwordHash' = '$passwordHashSA') ";
           //print_r($sql);
           $sql_query = $cDB->sql_query($sql);
           $sql_result = $cDB->sql_fetch_object($sql_query);
           $id_user = $sql_result->id_user;
           if($id_user > 0){
               // oznaceni atleta
               if ($sql_result->typ_user == 'ATL') {
                   $_SESSION['ATLET'] = 1;
                   $_SESSION['TRN'] = 0;
                   $_SESSION['ADM'] = 0;
               }
               //oznaceni trenera
               if ($sql_result->typ_user == 'TRN') {
                   $_SESSION['TRN'] = 1;
                   $_SESSION['ATLET'] = 0;
                   $_SESSION['ADM'] = 0;
               }
               if ($sql_result->typ_user == 'ADM') {
                   $_SESSION['TRN'] = 1;
                   $_SESSION['ATLET'] = 1;
                   $_SESSION['ADM'] = 1;
               } 
               $_SESSION['IDUSER'] = $id_user;
                return true;
           } else {
               $_SESSION['IDUSER'] = 0;
               return false;
           }


    }
    
    public function editUser($dataArray) {
        $cDB = $this->getDB();
        $password = $dataArray['heslo'];
        $passwordKontrola = $dataArray['hesloPotvrzeni'];
        $passwordHash = hash('sha256', $password);
       if ($password > '' && $passwordKontrola <> $password) {
           print 'Hesla se neshodují';
           return false;
       }
       $jmeno = $dataArray['jmeno'];
       $prijmeni = $dataArray['prijmeni'];
       $kod = $dataArray['kod'];
       $mail = $dataArray['email'];
       $titul = $dataArray['titul'];
       $typ = $dataArray['typ'];
       $popis = $dataArray['popis'];
       $id_user = $dataArray['iduser'];
       $sql = "update user "
               . "set `jmeno` ='$jmeno',"
               . "`prijmeni` = '$prijmeni',"
               . "`typ_user` = '$typ',"
               . "`kod` = '$kod',"
               . "`email` = '$mail',"
               . "`popis` = '$popis' "
               . "where id_user='$id_user'";
       $sql_query = $cDB->sql_query($sql); 
       
       if ($password > '') {
           $sql = "update user "
               . "set `password` ='$passwordHash'"
               . "where id_user='$id_user'";
        $sql_query = $cDB->sql_query($sql); 
       }
       
       return 'Uživatel byl uložen.';


    }

    public function getAtlet() {
        $cDB = $this->getDB();

           $sql = "select * from user where typ_user='ATL'";
           $sql_query = $cDB->sql_query($sql);
           $i=0;
           while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $userAtlet[$i] = $rows;
                       $i++;
           }
           return $userAtlet;
    }
    
    public function getTrenerAtleti() {
            $cDB = $this->getDB();   
            $id_user = $_SESSION['IDUSER'];
           $sql = "select u.id_user, u.login, u.titul, u.jmeno, u.prijmeni, u.email from trener_atlet ta join user u on u.id_user=ta.user_atlet where ta.user_trener=$id_user";
           $sql_query = $cDB->sql_query($sql);  
           $i=0;
           $trenerAtleti = array();
           while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $trenerAtleti[$i] = $rows;
                       $i++;
           }
           return $trenerAtleti;           
    }
    
    public function getUserTrener() {
            $cDB = $this->getDB();   
            $id_user = $_SESSION['IDUSER'];
           $sql = "select ta.user_trener from trener_atlet ta where ta.user_atlet=$id_user limit 1";
           $sql_query = $cDB->sql_query($sql);  
           $i=0;
           $trenerAtleti = array();
           $result = $cDB->sql_fetch_assoc($sql_query);
           
           return $result['user_trener'];           
    }
    
    private function getDB() {
        if ($this->cDB === null) {
           // require 'cDB.php';
            $this->cDB = new cDB();
        }
        return $this->cDB;
    }
}
