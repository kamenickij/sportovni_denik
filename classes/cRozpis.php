<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cRozpis
 *
 * @author kamen
 */
class cRozpis {
   private $cDB;

    public function __construct() {

    }

    

    public function rozpis_save($dataArray) {
        $id_user = $_SESSION['IDUSER'];
        $cDB = $this->getDB();

        $nazevTable = $dataArray['nazevTable'];
        $specializace = $dataArray['specializace'];
        
        $spec = $dataArray['spec'];
                
        foreach ($dataArray as $key => $value) {
            if (strrpos($key,'_') > 0){ 
                $key = explode('_', $key);
                $arg = $key[1];
                $tyden = $key[0];
                $tableData[$arg][$tyden] = $value;
            }
        }
           $sql = "insert into rocni_predpis(nazev, specializace, user_id_user)"
                   . "values('$nazevTable', '$spec', $id_user)";

           $sql_query = $cDB->sql_query($sql);

           $table_value = $cDB->get_last_inserted_id($cDB->get_connection());

           foreach ($tableData as $specializace => $dataArray ) {
           
                for($tyden=1; $tyden<54; $tyden++) {
                    if (isset($dataArray[$tyden])) {
                        $tydenValue = $dataArray[$tyden];
                        $sql = "insert into rocni_predpis_table(nazev, specializace, tyden, hodnota, rocni_predpis_iPK)"
                        . "values('$nazevTable', '$specializace', '$tyden', '$tydenValue', '$table_value')";
                   //  print_r($sql);
                         $sql_query = $cDB->sql_query($sql);
                    }
                }
           }
           print 'Tabulka uložena. Nyní můžete křížkem v pravém horním rohu okno zavřít.';
    }
    
    public function rozpis_update_save($dataArray) {
        $id_user = $_SESSION['IDUSER'];
        $cDB = $this->getDB();

        $nazevTable = $dataArray['nazevTable'];
        $specializace = $dataArray['specializace'];
        $iPKHeader = $dataArray['iPKHeader'];
        foreach ($dataArray as $key => $value) {
            if (strrpos($key,'_') > 0){ 
                $key = explode('_', $key);
                $arg = $key[1];
                $tyden = $key[0];
                $tableData[$arg][$tyden] = $value;
            }
        }
        //print_r($dataArray);
        //die();
        
           $sql = "update rocni_predpis set nazev='$nazevTable' where iPK=$iPKHeader";

           $sql_query = $cDB->sql_query($sql);

           foreach ($tableData as $specializace => $dataArray ) {
           //    print_r('SPEC'.$specializace);
           $sql = "select 1 from rocni_predpis_table rpt where rocni_predpis_iPK='$iPKHeader' and rpt.specializace = '$specializace'";
         //  print_r($sql);
           $sql_query = $cDB->sql_query($sql);
           $result = $cDB->sql_fetch_assoc($sql_query);
           if (@$result[1] == 1) {
               $exists = 1;
           } else {
               $exists = 0;
           }
                for($tyden=1; $tyden<54; $tyden++) {
                    if (isset($dataArray[$tyden])) {
                        $tydenValue = $dataArray[$tyden];
                        if($exists == 1) {
                            $sql = "update rocni_predpis_table set nazev='$nazevTable', hodnota='$tydenValue' where "
                                    . "rocni_predpis_iPK='$iPKHeader' and "
                                    . "tyden='$tyden' and specializace='$specializace'";
                        } elseif($exists == 0) {
                            $sql = "insert into rocni_predpis_table(nazev, specializace, tyden, hodnota, rocni_predpis_iPK)"
                            . "values('$nazevTable', '$specializace', '$tyden', '$tydenValue', '$iPKHeader')";
                        }
                    //    print_r($sql);
                         $sql_query = $cDB->sql_query($sql);
                    }
                }
                $allOTU .= "$specializace,";                
           }
           $allOTU = substr($allOTU, 0, strlen($allOTU)-1);
           
           $sql = "delete from rocni_predpis_table where rocni_predpis_iPK='$iPKHeader' and specializace NOT IN ($allOTU)";
           //print_r($sql);
           $sql_query = $cDB->sql_query($sql);
           
           print 'Tabulka uložena. Nyní můžete křížkem v pravém horním rohu okno zavřít.';
    }

    public function getRozpisTablesHeader($id_user, $sTextFiltr = '', $id_atlet = 0) {
        $cDB = $this->getDB();


         //  $sql = "SELECT rp.* from rocni_predpis rp"
         //          . " where rp.user_id_user=$id_user";
        $id_atlet = (int)$id_atlet;

        $sql = "CALL sp_rocniTabulka('rocniTabulkaFiltruj', '$sTextFiltr', $id_atlet, $id_user)";
           //print_r($sql);
           $sql_query = $cDB->sql_query($sql);
           $result = array();
           $i = 0;
           while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
           }
           $sql_query->free();
           $cDB->sql_next_result();
           return $result;

    }
    public function getRozpisTablesSdileni($id_user) {
        $cDB = $this->getDB();


         $sql = "SELECT rp.* from rocni_predpis rp"
         . " where rp.user_id_user=$id_user";

         //  print_r($sql);
           $sql_query = $cDB->sql_query($sql);
           $result = array();
           $i = 0;
           while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
           }
           $sql_query->free();
           return $result;

    }
    public function getRozpisTable($iPK) {
        $cDB = $this->getDB();


           $sql = "SELECT rp.specializace as specializaceHeader, rp.iPK as iPKHeader, rp.nazev as nazevTabulky, rpt.* from rocni_predpis rp"
                   . " join rocni_predpis_table rpt on rpt.rocni_predpis_iPK=rp.iPK"
                   . " where rp.iPK=$iPK";

        //  print_r($sql);
           $sql_query = $cDB->sql_query($sql);

           $i = 0;
           while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
           }

          return $result;

    }
    public function delRozpisTable($iPK) {
        $cDB = $this->getDB();
        $sql_error = NULL;
            $sql = "DELETE from user_rocni_predpis where rocni_predpis_iPK = $iPK";  
           $sql_query = $cDB->sql_query($sql);
           $sql_error .= @mysqli_error($cDB);
            $sql = "DELETE from rocni_predpis_table where rocni_predpis_iPK = $iPK";
           $sql_query = $cDB->sql_query($sql);
           $sql_error .= @mysqli_error($cDB);
            $sql = "DELETE from rocni_predpis where iPK = $iPK";
           $sql_query = $cDB->sql_query($sql);
           $sql_error = @mysqli_error($cDB);
          // print_r($sql_query);
           
           if ($sql_error === NULL) {
            print 'Tabulka smazána';
           } else {
               print 'ERROR:'. $sql_error;
           }


    }
    public function getOts() {
        $cDB = $this->getDB();


           $sql = "SELECT iPK, sName, sMj, spec FROM ots ";

          $sql_query = $cDB->sql_query($sql);
          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }

    public function getOtsHledat($id, $nazev) {
        $cDB = $this->getDB();
          if ($id == 0){
            $where = " WHERE o.sName LIKE '%$nazev%' ORDER BY o.kod asc";
          } elseif( $nazev == ''){
            $where = " WHERE o.iPK =$id ORDER BY o.kod asc";
          } else{
            $where = " WHERE o.iPK =$id AND o.sName LIKE '%$nazev%' ORDER BY o.kod asc" ;
          }
           $sql = "SELECT o.*, s.iPK as sIPK, s.sName as ssname FROM ots o LEFT JOIN spec s ON o.spec = s.iPK "
           . $where;

          print_r($sql);
          $sql_query = $cDB->sql_query($sql);
          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }
    public function getSpecHledat($nazev) {
        $cDB = $this->getDB();
          if ($nazev == ''){
            $sql = "select * from spec";
          } else {
            $sql = "select * from spec WHERE sName LIKE '%$nazev%'" ;
          }


          $sql_query = $cDB->sql_query($sql);
          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }


    
    public function getOtsPrehled() {
        $cDB = $this->getDB();


           $sql = "SELECT iPK, sName, sMj FROM ots ";

          $sql_query = $cDB->sql_query($sql);
          $result = array();
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$rows['iPK']] = $rows['sName'];
             
          }

          return $result;


    }
    
    public function getUsersSdileni() {
        $cDB = $this->getDB();
        $id_user = $_SESSION['IDUSER'];
           $sql = "SELECT u.id_user, u.jmeno, u.prijmeni, u.email, rp.rocni_predpis_iPK FROM user u"
                   . " join trener_atlet ta on ta.user_atlet = u.id_user"
                   . " left outer join user_rocni_predpis rp on rp.user_id_user = u.id_user"
                   . " WHERE u.typ_user='ATL' and ta.user_trener=$id_user";

          $sql_query = $cDB->sql_query($sql);

          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }
    public function getUsersSdileniRt($tableIPK) {
        $cDB = $this->getDB();
        $id_user = $_SESSION['IDUSER'];
           $sql = "SELECT u.id_user,u.login,u.titul,u.jmeno,u.prijmeni,u.popis,u.typ_user,u.email,u.kod, ur.rocni_predpis_iPK FROM user u"
                   . " left outer join user_rocni_predpis ur on ur.user_id_user=u.id_user"
                   . " WHERE ur.rocni_predpis_iPK=$tableIPK";

          $sql_query = $cDB->sql_query($sql);

          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }
    public function getUsersSdileniCopyModal($idCopy) {
        $cDB = $this->getDB();
        $id_user = $_SESSION['IDUSER'];
           $sql = "SELECT u.id_user, u.jmeno, u.prijmeni, u.email, rp.rocni_predpis_iPK FROM user u"
                   . " join trener_atlet ta on ta.user_atlet = u.id_user"
                   . " left outer join user_rocni_predpis rp on rp.user_id_user = u.id_user"
                   . " WHERE u.typ_user='ATL' and ta.user_trener=$id_user and u.id_user != $idCopy";

          $sql_query = $cDB->sql_query($sql);

          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }
    public function deconnectUser($id_user, $id_user_deconnect) {
        $cDB = $this->getDB();
           $sql = "DELETE FROM trener_atlet WHERE user_trener = $id_user AND user_atlet = $id_user_deconnect";

          $sql_query = $cDB->sql_query($sql);

          $result = $cDB->sql_affected_rows();

          return $result;
    }
   

    public function priraditTabulku($id_user_tabulka, $iPKtabulka ) {
        $cDB = $this->getDB();
           $sql = "SELECT user_id_user"
           . " FROM  user_rocni_predpis"
           . " WHERE user_id_user=$id_user_tabulka  "
           ; 

          $sql_query = $cDB->sql_query($sql);
          $result = $cDB->sql_fetch_assoc($sql_query);
          if ( count($result) > 0 ) {
              if($iPKtabulka == 0){
                $sql = "DELETE FROM user_rocni_predpis"
                . " WHERE user_id_user=$id_user_tabulka "
                ;
                $message='Záznam byl aktualizován.';
              }else {
                $sql = "UPDATE user_rocni_predpis"
                . " SET rocni_predpis_iPK=$iPKtabulka"
                . " WHERE user_id_user=$id_user_tabulka "
                ;
                $message='Záznam byl aktualizován.';
            }
          } else {
            $sql = "INSERT INTO user_rocni_predpis (rocni_predpis_iPK, user_id_user)"
            . " VALUES ($iPKtabulka, $id_user_tabulka) "
            ;       
          }
          $sql_query = $cDB->sql_query($sql);
          $message='Záznam byl vložen.';
          return $message;
    }
    
    public function pridatUser($idAdd, $emailAdd, $id_user ) {
        $cDB = $this->getDB();
        $pridaniUzivatelMsg = "";
        if ($idAdd == null || $idAdd == undefined ){
          $sql = "SELECT id_user "
        . "FROM  user "
        . "WHERE email='$emailAdd'  "
        ; 
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_fetch_assoc($sql_query);
        $idAdd = $result['id_user'];

        }
        // kontrola zdali nemá už někoho přiřazeného
        $sql = "SELECT user_trener, user_atlet "
        . "FROM  trener_atlet "
        . "WHERE user_atlet='$idAdd' AND user_trener <> '$id_user' "
        ; 
        $sql_query = $cDB->sql_query($sql);
          $result = $cDB->sql_fetch_assoc($sql_query);
          if ( count($result) > 0 ) {
            $pridaniUzivatelMsg = "Tento atlet je již k nějakému trenérovi přiřazen.";
            return $pridaniUzivatelMsg;
            
          } 
        //kontrola zda altleta jiz nema prirazeneho
           $sql = "SELECT user_trener, user_atlet "
           . "FROM  trener_atlet "
           . "WHERE user_trener='$id_user' AND user_atlet='$idAdd' "
           ; 

          $sql_query = $cDB->sql_query($sql);
          $result = $cDB->sql_fetch_assoc($sql_query);
          if ( count($result) > 0 ) {
            $pridaniUzivatelMsg = "Tohoto atleta již máte k sobě přiřazeného.";
            return $pridaniUzivatelMsg;
            
          } 
          //kontrola zda uzivatele mame v DB
          else {
            $sql = "SELECT id_user, email "
            . "FROM user "
            . "WHERE id_user='$idAdd' OR email='$emailAdd' "
            ;
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_fetch_assoc($sql_query);    
            // pokud mame v DB -> priradit k uzivateli   
            if ( count($result) > 0 ) {
                $pridaniUzivatelMsg = "";
                $sql = "INSERT INTO trener_atlet (user_trener, user_atlet) VALUES ('$id_user', '$idAdd')";
                $sql_query = $cDB->sql_query($sql);
                $pridaniUzivatelMsg = "Uživatel Vám byl přiřazen.";
                return $pridaniUzivatelMsg;

            } 
            
            else {
                $pridaniUzivatelMsg = "Uživatel s tímto ID či EMAILEM není v naší aplikaci registrován a nebo jste zadali chybné ID či EMAIL.";
                return $pridaniUzivatelMsg;
            }
          }
       
    }
    
     public function getTydenniRozpis($id_user, $tyden) {
        $cDB = $this->getDB();


           $sql = "SELECT rp.specializace, rpt.hodnota FROM user_rocni_predpis urp join rocni_predpis rp on rp.iPK=urp.rocni_predpis_iPK"
                   . " join rocni_predpis_table rpt on rpt.rocni_predpis_iPK = rp.iPK"
                   . " WHERE urp.user_id_user=$id_user and rpt.tyden=$tyden";
           print_r($sql);
           
          $sql_query = $cDB->sql_query($sql);
          $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;


    }
    
    public function getTydenDny($week, $rok) {
             
        $week = $week-1;
        $year = $rok;

        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        $date_for_monday = date( 'j.n.o', $timestamp_for_monday );
        $timestamp = $timestamp_for_monday;
        
        $tyden[1] = $date_for_monday;
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[2] = date('j.n.o', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[3] = date('j.n.o', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[4] = date('j.n.o', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[5] = date('j.n.o', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[6] = date('j.n.o', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[7] = date('j.n.o', $timestamp);
        
        return $tyden;
    }
    
    public function getTydenDnyDB($week, $rok) {
             
        $week = $week-1;
        $year = $rok;

        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
        $date_for_monday = date( 'o-n-j', $timestamp_for_monday );
        
        
        $tyden[1] = $date_for_monday;
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[2] = date('o-n-j', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[3] = date('o-n-j', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[4] = date('o-n-j', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[5] = date('o-n-j', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[6] = date('o-n-j', $timestamp);
        $timestamp = strtotime('+1 days', $timestamp);
        $tyden[7] = date('o-n-j', $timestamp);
        
        return $tyden;
    }
    
    public function getTydenRozpis($id_user_atlet, $tyden) {
        
        $cDB = $this->getDB();
        $trener_id_user = $_SESSION['IDUSER'];
        $jednotkyZatizeni = 0;
        $dnyZatizeni = 0;
        $tydenniRozpis = array();
        foreach($tyden as $key => $den) {
            $denSQL = date("Y-m-d", strtotime($den));  
            $sql = "CALL sp_aktivityPredpis ('getTydenniRozpis', 0, '', '', 0, 0, $id_user_atlet, $trener_id_user, 0, 0, '', '$denSQL', 0)";
            $tydenniRozpis[$key] = array();            
            $sql_query = $cDB->sql_query($sql);
            $i=0;
            $iFazeOld = 0;
            while ($rows = $cDB->sql_fetch_assoc($sql_query)){
                $iFaze = $rows['iFaze'];
                if($iFazeOld <> $iFaze){
                    $jednotkyZatizeni = $jednotkyZatizeni + 1;
                    $iFazeOld = $iFaze;
                }
                if($i == 0) {
                    $dnyZatizeni = $dnyZatizeni + 1;
                }
                $tydenniRozpis[$key][$iFaze]['misto'] = $rows['sFazeMisto'];
                $tydenniRozpis[$key][$iFaze]['poznamka'] = $rows['sPoznamka'];
                $tydenniRozpis[$key][$iFaze]['iNemoc'] = $rows['iNemoc'];
                $tydenniRozpis[$key][$iFaze]['iZavod'] = $rows['iZavod'];
                $tydenniRozpis[$key][$iFaze]['sNemoc'] = $rows['sNemoc'];
                $tydenniRozpis[$key][$iFaze]['sZavod'] = $rows['sZavod'];
                $tydenniRozpis[$key][$iFaze]['casZatizeni'] = $rows['casZatizeni'];
                $tydenniRozpis[$key][$iFaze]['casRegenerace'] = $rows['casRegenerace'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['idOTU'] = $rows['ots_iPK'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['sAktivita'] = $rows['sAktivita'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['iOpakovani'] = $rows['pocetOpakovani'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['vzdalenost'] = $rows['vzdalenost'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['iPK'] = $rows['iPK'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['ots_smj'] = $rows['sMj'];
              $i++;
          }
          $cDB->sql_next_result();
        }     
        $tydenniRozpis['jednotkyZatizeni'] = $jednotkyZatizeni; 
        $tydenniRozpis['dnyZatizeni'] = $dnyZatizeni;
        return $tydenniRozpis;
    }
    
    public function getTydenRozpisPrehled($id_user_atlet, $tyden) {
        
        $cDB = $this->getDB();
        $trener_id_user = $_SESSION['IDUSER'];
        $predpis = array();        
        foreach($tyden as $key => $den) {
            $denSQL = date("Y-m-d", strtotime($den));  
            $sql = "CALL sp_aktivityPredpis ('getTydenniRozpis', 0, '', '', 0, 0, $id_user_atlet, $trener_id_user, 0, 0, '', '$denSQL', 0)";
            //$predpis = array();
            $sql_query = $cDB->sql_query($sql);
            $i=0;
            while ($rows = $cDB->sql_fetch_assoc($sql_query)){                
                
                if(!@$predpis[$rows['ots_iPK']]) {
                    $predpis[$rows['ots_iPK']] = 0;
                }
                
                $predpis[$rows['ots_iPK']] += $rows['pocetOpakovani'] * $rows['vzdalenost'];
              $i++;
          }
          $cDB->sql_next_result();
        }        
        return $predpis;
    }
    
    public function getTydenRozpisAtlet($id_user_atlet, $tyden, $iStav) {
         
        $cDB = $this->getDB();
        $trener_id_user = $_SESSION['IDUSER'];
        $tydenniRozpis = array();
        $jednotkyZatizeni = 0;
        $dnyZatizeni = 0;
        foreach($tyden as $key => $den) {
            $denSQL = date("Y-m-d", strtotime($den));  
            $sql = "CALL sp_aktivityPredpis ('getTydenniRozpisAtlet', 0, '', '', 0, 0, $id_user_atlet, $trener_id_user, 0, 0, '', '$denSQL', $iStav )";            
            $tydenniRozpis[$key] = array();
            $sql_query = $cDB->sql_query($sql);
            $i=0;
            $iFazeOld = 0;
            while ($rows = $cDB->sql_fetch_assoc($sql_query)){
                $iFaze = $rows['iFaze'];
                
                if($iFazeOld <> $iFaze){
                    $jednotkyZatizeni = $jednotkyZatizeni + 1;
                    $iFazeOld = $iFaze;
                }
                if($i == 0) {
                    $dnyZatizeni = $dnyZatizeni + 1;
                }
                
                $tydenniRozpis[$key][$iFaze]['misto'] = $rows['sFazeMisto'];
                $tydenniRozpis[$key][$iFaze]['idTrener'] = $rows['creator_id_user'];
                $tydenniRozpis[$key][$iFaze]['casZatizeni'] = $rows['apa_casZatizeni'];
                $tydenniRozpis[$key][$iFaze]['casRegenerace'] = $rows['apa_casRegenerace'];
                $tydenniRozpis[$key][$iFaze]['iNemoc'] = $rows['apa_iNemoc'];
                $tydenniRozpis[$key][$iFaze]['iZavod'] = $rows['apa_iZavod'];
                $tydenniRozpis[$key][$iFaze]['sNemoc'] = $rows['apa_sNemoc'];
                $tydenniRozpis[$key][$iFaze]['sZavod'] = $rows['apa_sZavod'];
                $tydenniRozpis[$key][$iFaze]['apa_ipk'] = $rows['apa_ipk'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['idOTU'] = $rows['ots_iPK'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['sAktivita'] = $rows['sAktivita'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['iOpakovani'] = $rows['pocetOpakovani'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['vzdalenost'] = $rows['vzdalenost'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['iPK'] = $rows['iPK'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['iStav'] = $rows['iStav'];
                $tydenniRozpis[$key][$iFaze]['aktivity'][$i]['ots_smj'] = $rows['sMj'];
              $i++;
          }
          $cDB->sql_next_result();
        }
        $tydenniRozpis['jednotkyZatizeni'] = $jednotkyZatizeni;  
        $tydenniRozpis['dnyZatizeni'] = $dnyZatizeni;
        return $tydenniRozpis;
    }
    
    public function copyTydenRozpis($tyden, $id_user_origin, $id_users) {
        $cDB = $this->getDB();
        $rok = date('Y');
        $tydenDny = $this->getTydenDnyDB($tyden,$rok);
        $id_users = explode(',', $id_users);
        $id_trener = $_SESSION['IDUSER'];
        foreach($id_users as $key => $value) {
            // odmaz vse co bylo nastaveno
        $sql = "delete from aktivitypredpis where atlet_id_user=$value and datum>='$tydenDny[1]' and datum<='$tydenDny[7]'";
        $sql_query = $cDB->sql_query($sql);
        $sql = "delete from aktivitapredpispoznamka where atlet_id_user=$value and datum>='$tydenDny[1]' and datum<='$tydenDny[7]'";
        $sql_query = $cDB->sql_query($sql);
        // vloz vse, co ma master
            $sql = "insert into aktivitypredpis (ots_iPK, aktivita_iPK, fazeMisto_iPK, iFaze, datum, pocetOpakovani, vzdalenost, atlet_id_user, creator_id_user, iStav)"
                . "select ots_iPK, aktivita_iPK, fazeMisto_iPK, iFaze, datum, pocetOpakovani, vzdalenost, $value, creator_id_user, iStav from aktivitypredpis where atlet_id_user=$id_user_origin and creator_id_user=$id_trener and datum>='$tydenDny[1]' and datum<='$tydenDny[7]'";
        $sql_query = $cDB->sql_query($sql);
        $sql = "insert into aktivitapredpispoznamka (sPoznamka, sPoznamkaAtlet, datum, atlet_id_user, creator_id_user)"
                . "select sPoznamka, sPoznamkaAtlet, datum, $value, creator_id_user from aktivitapredpispoznamka where atlet_id_user=$id_user_origin and creator_id_user=$id_trener and datum>='$tydenDny[1]' and datum<='$tydenDny[7]'";
        $sql_query = $cDB->sql_query($sql);
        }
        
    }
    
    public function copyRocniRozpis($iPK) {
        $cDB = $this->getDB();
        $id_user = $_SESSION['IDUSER'];
        $sql = "CALL sp_rocniTabulka('copyRocniTabulka', $iPK,0,$id_user)";
        $sql_query = $cDB->sql_query($sql);        
        $cDB->sql_next_result();
        return 1;
    }
    
    public function getRocniPredpisTyden($id_user_atlet, $tyden){
        $cDB = $this->getDB();
        $sql = "select rpt.specializace as idOTS, rpt.hodnota from rocni_predpis_table rpt join user_rocni_predpis urp on urp.user_id_user=$id_user_atlet and urp.rocni_predpis_iPK=rpt.rocni_predpis_iPK and rpt.tyden=$tyden";
        $sql_query = $cDB->sql_query($sql);
        $rocniPredpis = array();
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
            $rocniPredpis[$rows['idOTS']] = $rows['hodnota'];
        }
        return $rocniPredpis;
    }

    public function getPoznamkaAtlet($id_user_atlet, $idTrener, $tyden) {
         
        $cDB = $this->getDB();
        foreach($tyden as $key => $den) {
            $denSQL = date("Y-m-d", strtotime($den));  
            $sql = "CALL sp_aktivityPredpis ('getPoznamka', 0, '', '', 0, 0, $id_user_atlet, $idTrener, 0, 0, '', '$denSQL' ,0 )";   
            $poznamka[$key] = array();
            $sql_query = $cDB->sql_query($sql);
            $i=0;
            $rows = $cDB->sql_fetch_assoc($sql_query);             
            $poznamka[$key]['sPoznamka'] = @$rows['sPoznamka'];            
            $poznamka[$key]['sPoznamkaAtlet'] = @$rows['sPoznamkaAtlet'];
                        
          $cDB->sql_next_result();
        }        
        return $poznamka;
    }
    
    public function setRozpisStav($iPK, $iStav) {
        
        $cDB = $this->getDB();
        $trener_id_user = $_SESSION['IDUSER'];
        
        
            $sql = "CALL sp_setAktivityPredpis ('setAktivityStav', $iPK, $iStav, '', 0, 0, '2000-01-01')";
                  //  print_r($sql);    
            $sql_query = $cDB->sql_query($sql);           
            
          $cDB->sql_next_result();
                
        return 1;
    }
    
    public function presunDoFaze($iPK, $doFaze) {
        
        $cDB = $this->getDB();        
        
            $sql = "update aktivitypredpis "
                    . "set iFaze = $doFaze "
                    . "where iPK=$iPK";
                        
            $sql_query = $cDB->sql_query($sql);
            
        return 1;
    }

    /**
     * @param $poznamka
     * @param $den
     * @param $atlet_id_user
     * @param $idTrener
     * @return int
     */
    public function setPoznamkaAtlet($poznamka, $den, $atlet_id_user, $idTrener) {
        
        $cDB = $this->getDB();        
        
            $sql = "CALL sp_setAktivityPredpis ('setPoznamkaAtlet', 0, 0, '$poznamka', $atlet_id_user, $idTrener, '$den')";
                      
            $sql_query = $cDB->sql_query($sql);
                        
          $cDB->sql_next_result();
                
        return $sql;
    }

    /**
     * @param $iNemoc
     * @param $iZavod
     * @param $sNemoc
     * @param $sZavod
     * @param $casZat
     * @param $casReg
     * @param $den
     * @param $atlet_id_user
     * @param $idTrener
     * @return int
     */
    public function setDataAtlet($apa_ipk, $iNemoc, $iZavod, $sNemoc, $sZavod, $casZat, $casReg, $den, $atlet_id_user, $idTrener) {
        
        $cDB = $this->getDB();        
        
            $sql = "update aktivitapredpisatlet "
                    . "set iNemoc=$iNemoc,"
                    . "iZavod=$iZavod,"
                    . "sNemoc='$sNemoc',"
                    . "sZavod='$sZavod',"
                    . "casZatizeni='$casZat',"
                    . "casRegenerace='$casReg'"
                    . "where iPK=$apa_ipk";
                      
            
            $sql_query = $cDB->sql_query($sql);                       
                
        return 1;
    }
    
    public function getSpecOtu($spec_iPK) {
        $cDB = $this->getDB(); 
        
        $sql = "select o.* from otu_spec s inner join ots o on o.iPK = s.ots_iPK where s.spec_iPK = $spec_iPK";        
        $sql_query = $cDB->sql_query($sql);
        $result = array();
          $i = 0;
          while ($rows = $cDB->sql_fetch_assoc($sql_query)){
              $result[$i] = $rows;
              $i++;
          }

          return $result;
    }

        //  public function getOTS($id_user, $tyden) {
    
    private function getDB() {
        if ($this->cDB === null) {
           // require 'cDB.php';
            $this->cDB = new cDB();
        }
        return $this->cDB;
    }
}
