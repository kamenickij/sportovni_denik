<?php
class cAdmin{
    private $cDB;

    public function __construct() {

    }

    private function getDB() {
        if ($this->cDB === null) {
           // require 'cDB.php';
            $this->cDB = new cDB();
        }
        return $this->cDB;
    }
    
    public function overitUser($email){
        $cDB = $this->getDB();
        $sql = "select id_user,login,jmeno,prijmeni, typ_user from user"
        . " where email = $email"; 
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_fetch_assoc($sql_query);
        return $result;
    }
    
    public function delOtu_Spec($iPK) {
        $cDB = $this->getDB();
        $sql = "delete from otu_spec where ots_iPK=$iPK";
        $cDB->sql_query($sql);
        return($sql);
    }

    public function update_Spec_In_Ots($iPK) {
        $cDB = $this->getDB();
        $sql = "UPDATE ots SET spec = null where iPK=$iPK";
        $cDB->sql_query($sql);
        return($sql);
    }
    
    public function setOtu_Spec($ots_ipk, $specArray) {
        $cDB = $this->getDB();
        $spec = '';
       foreach($specArray as $key => $item) {
            $sql = "insert into otu_spec(ots_iPK, spec_iPK) values($ots_ipk, $item)";
            $spec = $spec.$item.',';
            $cDB->sql_query($sql);
       }
        $spec = substr($spec, 0, strlen($spec)-1);
        $sql = "update ots set spec='$spec' where iPK=$ots_ipk";
        $cDB->sql_query($sql);
       
    }
}