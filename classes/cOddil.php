<?php

/*
 databazove dotazy pro sekci oddil
 */

/**

 *
 * @author zahond
 */
class cOddil {

    private $cDB;

    public function __construct() {
          
    }

    private function getDB() {
        if ($this->cDB === null) {
           // require 'cDB.php';
            $this->cDB = new cDB();
        }
        return $this->cDB;
    }
//ulozeni noveho clena do DB
    public function novyClen($jmeno, $prijmeni, $rocnik, $poznamka) {
        $result = 0;
        $sMessage = '';
        $cDB = $this->getDB();
        $sql = "INSERT INTO oddilclenove (`sJmeno`, `sPrijmeni`, `sRocnik`, `sText`, `dCreate`, `dUpdate`) VALUES ('$jmeno', '$prijmeni', '$rocnik', '$poznamka', now(), now());";
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
      
        return $result;
    }
//filtruj clena
    public function filtrClen($jmeno, $prijmeni, $rocnik, $chTrener, $chArchive) {
        $isTrener = "";
        if ($chTrener == 'on'){
            $isTrener = " AND (`iTrener` = 1)";

        }
        $iArchive = "";
        if ($chArchive == 'on'){
            $iArchive = " AND (`iArchive` = 1)";

        } else {
            $iArchive = " AND (`iArchive` = 0)";
        }
        $cDB = $this->getDB();
        $sql = "SELECT * FROM oddilclenove WHERE (`sJmeno` LIKE '%$jmeno%' OR `sJmeno` = '') AND (`sPrijmeni` LIKE '%$prijmeni%' OR `sPrijmeni` = '') AND (`sRocnik` LIKE '%$rocnik%' OR `sRocnik` = '') " . $isTrener . $iArchive . " ;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    }    
    //filtruj clena dochazka nasdileni
    public function filtrClenDochazkaNasdilet($jmeno, $prijmeni, $rocnik, $chTrener, $chArchive, $chShared) {
        $isTrener = "";
        if ($chTrener == 'on'){
            $isTrener = " AND (`iTrener` = 1)";

        }
        $iArchive = "";
        if ($chArchive == 'on'){
            $iArchive = " AND (`iArchive` = 1)";

        }
        $iShared = "";
        if ($chShared == 'on'){
            $iShared = " AND (`iShared` = 1)";

        }
        $cDB = $this->getDB();
        $sql = "SELECT oc.*, (CASE WHEN s.iShared IS NULL THEN 0 ELSE s.iShared END) as iShared FROM oddilclenove oc LEFT OUTER JOIN dochazka_share_clen s on s.id_user = oc.iPK WHERE (`sJmeno` LIKE '%$jmeno%' OR `sJmeno` = '') AND (`sPrijmeni` LIKE '%$prijmeni%' OR `sPrijmeni` = '') AND (`sRocnik` LIKE '%$rocnik%' OR `sRocnik` = '') " . $isTrener . $iArchive . " ORDER BY oc.sPrijmeni;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    }//filtruj celna    
    //filtruj prispevek
     public function filtrPrispevek($jmeno, $prijmeni, $rocnik, $placenoDo, $chPlacenoDo) {
        $sPlatbaDo = "";
        if ($chPlacenoDo == 'on'){
            $sPlatbaDo = " AND  (p.`dPlacenoDo` <= '$placenoDo')";

        }
        $cDB = $this->getDB();
        $sql = "SELECT o.*, p.dPlacenoDo, p.dUpdate as dUpdatePlacenoDo, p.sTypPlatby, p.sPoznamka,p.iCastka FROM oddilclenove o LEFT OUTER JOIN oddil_prispevky p on p.id_user = o.iPK  WHERE (o.`sJmeno` LIKE '%$jmeno%' OR o.`sJmeno` = '') AND (o.`sPrijmeni` LIKE '%$prijmeni%' OR o.`sPrijmeni` = '') AND (o.`sRocnik` LIKE '%$rocnik%' OR o.`sRocnik` = '')" .  $sPlatbaDo . " ;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    }//filtruj prispevek
    //save prispevek
     public function prispevekClenSave($dPlacenoDo, $iCastka, $sTypPlatby, $sPoznamka, $id_user) {
        $cDB = $this->getDB();
        //check jestli uzivatel uz v tabulce existuje
        $sql = "SELECT * FROM oddil_prispevky WHERE  `id_user` = $id_user";
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        //uzivatel existuje
        if ($result > 0){
            $sql = "UPDATE oddil_prispevky SET `dUpdate` = Now(), `dPlacenoDo` = '$dPlacenoDo', `sTypPlatby` = '$sTypPlatby', `sPoznamka` = '$sPoznamka', `iCastka` = $iCastka WHERE `id_user` = $id_user ;";
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        } else {
            $sql = "INSERT INTO oddil_prispevky (`dUpdate`, `dPlacenoDo`, `sTypPlatby`, `sPoznamka`, `iCastka`, `id_user`) VALUES (Now(), '$dPlacenoDo', '$sTypPlatby', '$sPoznamka', $iCastka, $id_user);";
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        }
               
        
        return $result;
    }     
    //novaSkupina
     public function novaSkupina($jmeno, $poznamka, $id_user) {
        $cDB = $this->getDB();
        //check jestli uzivatel uz v tabulce existuje
        $sql = "INSERT INTO skupiny (`sJmeno`, `sPoznamka`, `id_user`) VALUES ('$jmeno','$poznamka', $id_user )";
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
       
        return $result;
    }   
    //vypis skupiny
     public function vypisSkupiny($id_user) {
        $cDB = $this->getDB();
        //check jestli uzivatel uz v tabulce existuje
        $sql = "SELECT * FROM skupiny WHERE `id_user` = $id_user";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    }  
    
     //nasdilej si clena na seznam pro dochazku
     public function dochazkaNasdilejClena($id_user, $checked, $id_trener) {
        $cDB = $this->getDB();
        //pokud true insert pokud false delete
        if($checked == 1){
            $sql = "INSERT INTO dochazka_share_clen (`id_user`, `id_trener`, `iShared`) VALUES ($id_user,$id_trener, 1 )";
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        } else{
            $sql = "DELETE FROM dochazka_share_clen WHERE `id_user` = $id_user AND  `id_trener` = $id_trener AND `iShared` = 1";
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        }
        return $result;
    }
    //vypis seznam nasdilenych clenu pro vyplneni dochazky
    public function dochazkaSeznamUsers($id_user) {
        $cDB = $this->getDB();
        $sql = "SELECT od.*, p.dPlacenoDo, (case when p.dPlacenoDo > now() then 1 else 0 end) as zaplaceno 
        FROM dochazka_share_clen sh 
        JOIN oddilclenove od on od.iPK = sh.id_user 
        LEFT OUTER JOIN oddil_prispevky p on p.id_user = sh.id_user
        WHERE `id_trener` = $id_user ORDER BY od.sPrijmeni ASC";
       /*  $sql = "SELECT od.* FROM dochazka_share_clen sh JOIN oddilclenove od on od.iPK = sh.id_user WHERE `id_trener` = $id_user"; */
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
       
    }
    //zapis jestli uzivatel byl v dany den na treninku
    public function set_dochazka_den_clen($id_user, $idClen, $date, $checked){
        $cDB = $this->getDB();
        //check jestli uzivatel uz v tabulce existuje
        $sql = "SELECT * FROM dochazka_clenove WHERE  `id_clen` = $idClen and `dDate` = '$date'";
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        //uzivatel existuje
        if ($result > 0){
            $sql = "UPDATE dochazka_clenove SET `dUpdate` = Now(), `id_trener`= $id_user, `iPritomen` = $checked WHERE `dDate` = '$date' and `id_clen` = $idClen ;";
            //print_r($sql);
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        } else {
            $sql = "INSERT INTO dochazka_clenove (`dUpdate`, `id_trener`, `iPritomen`, `dDate`, `id_clen`) VALUES (Now(), $id_user, $checked, '$date', $idClen);";
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        }
               
        
        return $result;
    }

    //zapis headru treninku
    public function set_dochazka_table_header($id_user, $date, $od, $do, $misto, $poznamka){
        $cDB = $this->getDB();
        //check jestli uzivatel uz v tabulce existuje
        $sql = "SELECT * FROM dochazka_table_header WHERE  `id_trener` = $id_user and `datum` = '$date'";
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        //uzivatel existuje
        if ($result > 0){
            $sql = "UPDATE dochazka_table_header SET `od` = '$od', `do` = '$do', `misto` = '$misto', `poznamka` = '$poznamka' WHERE `datum` = '$date' and `id_trener`= $id_user;";
            //print_r($sql);
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        } else {
            $sql = "INSERT INTO dochazka_table_header (`id_trener`, `od`, `do`, `datum`, `misto`, `poznamka`) VALUES ($id_user, '$od', '$do', '$date', '$misto', '$poznamka');";
            $sql_query = $cDB->sql_query($sql);
            $result = $cDB->sql_affected_rows();
        }
               
        
        return $result;
    }

    //nacti dochazka_table_header pro dany den a id trenera
    public function get_dochazka_table_header($id_user, $date){
        $cDB = $this->getDB();
        $sql = "SELECT * FROM dochazka_table_header WHERE  `id_trener` = $id_user and `datum` = '$date'";
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;

    }
    //nacti dochazka_clenove pro dany den a id trenera
    public function get_dochazka_den_clen($id_user, $date){
        $cDB = $this->getDB();
        $sql = "SELECT `id_clen` FROM dochazka_clenove WHERE  `id_trener` = $id_user and `dDate` = '$date' and `iPritomen` = 1";
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    }   

    //detail clena oddilu z table oddilClenove
    public function get_detailClenoveTable($iPK){
        $cDB = $this->getDB();
        $sql = "SELECT * FROM oddilclenove WHERE  `iPK` = $iPK ";
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    } 

     //ulozit detail clena 
    public function set_detail_clena($iPK, $iTrener, $iArchive, $sJmeno, $sPrijmeni, $sText, $sRocnik){
        $cDB = $this->getDB();
        $sql = "UPDATE oddilclenove SET `iTrener` = $iTrener, `iArchive` = $iArchive,  `sJmeno` = '$sJmeno', `sPrijmeni` = '$sPrijmeni', `sText` = '$sText', `sRocnik` = '$sRocnik', `dUpdate` = Now()  WHERE  `iPK` = $iPK ;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        return $result;
    }

    //------------------reporty ------dotazy SQL sekce reporty ---------------------


    // oddil reportz celkem clenu v db
    public function oddReportyCelkemClenu(){
        $cDB = $this->getDB();
        $sql = "SELECT * FROM oddilclenove ;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        return $result;

    }
    // oddil reportz celkem clenu v db aktivni
    public function oddReportyCelkemClenuAktivni(){
        $cDB = $this->getDB();
        $sql = "SELECT * FROM oddilclenove WHERE iArchive = 0 ;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        return $result;

    }

    // oddil reportz celkem clenu v db aktivni treneri
    public function oddReportyCelkemClenuAktivniTreneri(){
        $cDB = $this->getDB();
        $sql = "SELECT * FROM oddilclenove WHERE iArchive = 0 and iTrener = 1 ;";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = $cDB->sql_affected_rows();
        return $result;

    }

    //oddil reporty filtr
    public function reporty_filtr_dochazka($od, $do, $jmeno, $prijmeni, $rocnik, $trener){
        $db1 = "";
        $db2 = "";
        $db3 = "";
        $db4 = "";
        if ( $trener != '' ){
            $db1 = "AND id_trener = $trener";
        }
        if ( $prijmeni != '' ){
            $db2 = "AND sPrijmeni like '%" . $prijmeni . "%'";
        }
        if ( $jmeno != '' ){
            $db3 = "AND sJmeno like '%" . $jmeno . "%'";
        }
        if ( $rocnik != '' ){
            $db4 = "AND sRocnik like '%" . $rocnik . "%'";
        }
        $cDB = $this->getDB();
        $sql = "SELECT dc.*, oc.sJmeno, oc.sPrijmeni, oc.sRocnik from dochazka_clenove dc join oddilclenove oc on oc.iPK = dc.id_clen where dDate > '$od' and dDate < '$do'" . "$db1" . "$db2" . "$db3" . "$db4 " . " ORDER BY dc.dDate ";
        //print_r($sql);
        $sql_query = $cDB->sql_query($sql);
        $result = array();
        $i = 0;
        while ($rows = $cDB->sql_fetch_assoc($sql_query)){
               $result[$i] = $rows;
               $i++;
        }
        $sql_query->free();
        return $result;
    }


           
}//cOddil
